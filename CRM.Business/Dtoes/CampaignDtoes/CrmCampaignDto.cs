﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Business.Dtoes.ProductDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.CampaignDtoes
{
    public class CrmCampaignDto
    {
        public CrmCampaignDto()
        {
            AllMembers= new List<CrmMemberDto>();
            AllProducts = new List<CrmProductDto>();
            AllCampaignTypes = ((IEnumerable<CampaignTypes>)Enum.GetValues(typeof(CampaignTypes))).Select(c => new CrmCampaignTypeDto() { TypeNameId = (int)c, TypeName = c.ToString() }).ToList();
            
            AllExpectedResponses = ((IEnumerable<ExpectedResponseType>)Enum.GetValues(typeof(ExpectedResponseType))).Select(c => new CrmCampaignExpectedResponseTypeDto() { ExpectedResponseTypeId = (int)c, ExpectedResponseTypeName = c.ToString() }).ToList();
            AssignTo = 1;
            TypeName = "1";
            ExpectedResponseTypeName = "1";
            NumberSent = 1;
            StatusNameId = "1";
        }

        public int CampaignId { get; set; }

        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        //[StringLength(100, MinimumLength = 10)]
        public string CampaignName { get; set; } // CampaignName

        public int AssignTo { get; set; } // AssignTo. Atanan Kişi
        [Required(ErrorMessage = "Lütfen geçerli bir tarihi giriniz")]
        public DateTime ExpectedCloseDate { get; set; } // ExpectedCloseDate. Bitiş Tarihi
        public string TargetAudience { get; set; } // TargetAudience. Hedef İlgili
        public string Sponsor { get; set; } // Sponsor
        public int TargetSize { get; set; } // TargetSize
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        [Range(1, Int32.MaxValue)]
        public int NumberSent { get; set; } // NumberSent. Gönderilen Adet
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public double BudgetCost { get; set; } // BudgetCost. Bütçelenen Maliyet
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public double ExpectedRevenue { get; set; } // ExpectedRevenue. Beklenen Ciro
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public int ExpectedSalesCount { get; set; } // ExpectedSalesCount. Beklenen Satış Adeti
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public int ExpectedResponseCount { get; set; } // ExpectedResponseCount. Beklenen Cevap Adeti
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public double ExpectedROI { get; set; } // ExpectedROI. Beklenen ROI
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public double ActualCost { get; set; } // ActualCost. Geçekleşen Maliyet
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public int ExpectedResponse { get; set; } // ExpectedResponse. Beklenen Sonuç:  Excellent  Good  Average  Poor
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public int ActualSalesCount { get; set; } // ActualSalesCount
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public int ActualResponseCount { get; set; } // ActualResponseCount. Gerçekleşen Cevap Adeti
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public double ActualROI { get; set; } // ActualROI. Gerçekleşen ROI
        public string Description { get; set; } // Description
        public CampaignTypes TypeId { get; set; } // TypeNameId
        public StatusTypes StatusId { get; set; } // StatusId
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy
        public int ProductId { get; set; }
        public bool IsDelete { get; set; }
        public int CampaignStatusId { get; set; }
        public int ExpectedResponseTypeId { get; set; }
        public string ExpectedResponseTypeName { get; set; }
        public int MemberNameId { get; set; }
        public int TypeNameId { get; set; }
        public int ProductNameId { get; set; }
        public DateTime ExpectedCloseCampaignDate { get; set; }
        public string CurrentRole { get; set; }
        public string StatusNameId { get; set; } // 
        public string TypeName { get; set; }
        public string CurrentOperation { get; set; }
        public string AssignPerson { get; set; }
        public string Status { get; set; }

        public CrmMemberDto AssignMember { get; set; }
        public CrmMemberDto UpdateMember { get; set; }
        public CrmMemberDto CreateMember { get; set; }
        public CrmProductDto Prodcut { get; set; }

        public List<CrmProductDto> AllProducts { get; set; }
        public List<CrmMemberDto> AllMembers { get; set; }
        public List<CrmCampaignTypeDto> AllCampaignTypes { get; set; }
        public List<Crm_CampaignStatu> AllCampaignStatus { get; set; }
        public List<CrmCampaignExpectedResponseTypeDto> AllExpectedResponses { get; set; }

        public static Crm_Campaign ToCrmCampaign(CrmCampaignDto campaign)
        {
            return new Crm_Campaign
            {
             CampaignId   = campaign.CampaignId,
            CampaignName = campaign.CampaignName,
            AssignTo =campaign.MemberNameId,
            ExpectedCloseDate = campaign.ExpectedCloseDate,
            CreatedTime = DateTime.Now.Date,
            UpdatedTime = DateTime.Now.Date,
            TargetAudience  = campaign.TargetAudience,
            Sponsor  = campaign.Sponsor,
            TargetSize  = campaign.TargetSize,
            NumberSent  = campaign.NumberSent,
            BudgetCost  = campaign.BudgetCost,
            ExpectedRevenue  = campaign.ExpectedRevenue,
            ExpectedSalesCount  = campaign.ExpectedSalesCount,
            ExpectedResponseCount  = campaign.ExpectedResponseCount,
            ExpectedROI  = campaign.ExpectedROI,
            ActualCost  = campaign.ActualCost,
             ExpectedResponse = int.Parse(campaign.ExpectedResponseTypeName),
            ActualSalesCount  = campaign.ActualSalesCount,
            ActualResponseCount  = campaign.ActualResponseCount,
            ActualROI  = campaign.ActualROI,
            Description  = campaign.Description,
            TypeId  =(int)campaign.TypeNameId,
            StatusId = (int)campaign.CampaignStatusId,
            ProductId = campaign.ProductNameId,
            IsDeleted = false
            };
        }

        public static CrmCampaignDto ToCrmCampaignDto(Crm_Campaign campaign)
        {
            return new CrmCampaignDto
            {

                CampaignName = campaign.CampaignName,
                AssignTo = campaign.AssignTo,
                ExpectedCloseDate = campaign.ExpectedCloseDate,
                TargetAudience = campaign.TargetAudience,
                Sponsor = campaign.Sponsor,
                TargetSize = campaign.TargetSize,
                NumberSent = campaign.NumberSent,
                BudgetCost = campaign.BudgetCost,
                ExpectedRevenue = campaign.ExpectedRevenue,
                ExpectedSalesCount = campaign.ExpectedSalesCount,
                ExpectedResponseCount = campaign.ExpectedResponseCount,
                ExpectedROI = campaign.ExpectedROI,
                ActualCost = campaign.ActualCost,
                ExpectedResponse = campaign.ExpectedResponse,
                ActualSalesCount = campaign.ActualSalesCount,
                ActualResponseCount = campaign.ActualResponseCount,
                ActualROI = campaign.ActualROI,
                Description = campaign.Description,
                TypeId = (CampaignTypes)campaign.TypeId,
                StatusId = (StatusTypes)campaign.StatusId
            };
        }
    }

}