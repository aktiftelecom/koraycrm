﻿namespace CRM.Business.Dtoes.CampaignDtoes
{
    /// <summary>
    /// CrmCampaignExpectedResponseTypeDto
    /// </summary>
    public class CrmCampaignExpectedResponseTypeDto
    {

        /// <summary>
        /// Gets or sets the type identifier.
        /// </summary>
        /// <value>
        /// The type identifier.
        /// </value>
        public int ExpectedResponseTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string ExpectedResponseTypeName { get; set; }
    }
}
