﻿namespace CRM.Business.Dtoes.CampaignDtoes
{
    /// <summary>
    /// CrmCampaignTypeDto
    /// </summary>
    public class CrmCampaignTypeDto
    {

        /// <summary>
        /// Gets or sets the type identifier.
        /// </summary>
        /// <value>
        /// The type identifier.
        /// </value>
        public int TypeNameId { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; set; }
    }
}
