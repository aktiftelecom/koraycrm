﻿using System.Collections.Generic;
using System.Linq;
using CRM.Core.Entities.City;

namespace CRM.Business.Dtoes.City
{
    public class CrmCityDto
    {

        /// <summary>
        /// Gets or sets the city identifier.
        /// </summary>
        /// <value>
        /// The city identifier.
        /// </value>
        public int CityId { get; set; }

        /// <summary>
        /// Gets or sets the cityame.
        /// </summary>
        /// <value>
        /// The cityame.
        /// </value>
        public string Cityame { get; set; }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>
        /// The country code.
        /// </value>
        public int CountryCode { get; set; }

        /// <summary>
        /// To the CRM campaign dto.
        /// </summary>
        /// <param name="cities">The cities.</param>
        /// <returns></returns>
        public static List<CrmCityDto> ToCrmCampaignDto(List<This_City> cities)
        {
            return cities.Select(city => new CrmCityDto
            {
                CityId = city.Code, Cityame = city.Name, CountryCode = city.Contry
            }).ToList();
        }
    }
}
