﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.ShippingDtoes;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.GLAccountDtoes
{
    /// <summary>
    /// CrmRoleDto
    /// </summary>
    public class GLAccountDto
    {

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public int GLAccountId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets the shipping dtos.
        /// </summary>
        /// <returns></returns>
        public static List<GLAccountDto> GetGLAccountDtos()
        {
            return ((IEnumerable<GLAccountTypes>)Enum.GetValues(typeof(ShippingType))).Select(
                c => new GLAccountDto() { GLAccountId = (int)c, Name = c.ToString() }).ToList();
        } 
    }
}
