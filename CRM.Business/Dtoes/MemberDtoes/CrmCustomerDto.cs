﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.City;
using CRM.Business.Dtoes.ProductDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Member;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.MemberDtoes
{
    /// <summary>
    /// CrmCustomerDto
    /// </summary>
    public class CrmCustomerDto
    {
        public CrmCustomerDto()
        {
            CreationTime = DateTime.Now;
            Types = ((IEnumerable<CustomerTypes>)Enum.GetValues(typeof(CustomerTypes))).Select(c => new CustomerTypeDto() { TypeId= (int)c, TypeName = c.ToString() }).ToList();
            RegisterTypes = ((IEnumerable<CustomerRegisterTypes>)Enum.GetValues(typeof(CustomerRegisterTypes))).Select(c => new CustomerRegisterTypeDto() { RegisterTypeId = (int)c, RegisterTypeName = c.ToString() }).ToList();
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId { get; set; } // MemberId (Primary key)

        /// <summary>
        /// Gets or sets the creation time.
        /// </summary>
        /// <value>
        /// The creation time.
        /// </value>
        public DateTime CreationTime { get; set; } // CreationTime
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        [Required(ErrorMessage = "Lütfen geçerli bir şirket ismi giriniz")]
        public string CompanyName { get; set; } // CompanyName

        /// <summary>
        /// Gets or sets the commercial title.
        /// </summary>
        /// <value>
        /// The commercial title.
        /// </value>
        public string CommercialTitle { get; set; } // CommercialTitle

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public string Address { get; set; } // Address

        /// <summary>
        /// Gets or sets the city identifier.
        /// </summary>
        /// <value>
        /// The city identifier.
        /// </value>
        public int? CityId { get; set; } // CityId

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        public string Phone { get; set; } // Phone

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        /// <value>
        /// The fax.
        /// </value>
        public string Fax { get; set; } // Fax

        /// <summary>
        /// Gets or sets the tax office.
        /// </summary>
        /// <value>
        /// The tax office.
        /// </value>
        public string TaxOffice { get; set; } // TaxOffice

        /// <summary>
        /// Gets or sets the tax number.
        /// </summary>
        /// <value>
        /// The tax number.
        /// </value>
        public string TaxNumber { get; set; } // TaxNumber

        /// <summary>
        /// Gets or sets the contry.
        /// </summary>
        /// <value>
        /// The contry.
        /// </value>
        public string Contry { get; set; } // Contry

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [Required(ErrorMessage = "Lütfen geçerli bir müşteri ismi giriniz")]
        public string FirstName { get; set; } // FirstName

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName { get; set; } // LastName

        /// <summary>
        /// Gets or sets the e mail.
        /// </summary>
        /// <value>
        /// The e mail.
        /// </value>
        [Required(ErrorMessage = "Lütfen geçerli bir email giriniz")]
        public string EMail { get; set; } // EMail

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [Required(ErrorMessage = "Lütfen geçerli bir şifre giriniz")]
        public string Password { get; set; } // Password

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the first name of the contact1.
        /// </summary>
        /// <value>
        /// The first name of the contact1.
        /// </value>
        public string Contact1FirstName { get; set; } // Contact1FirstName

        /// <summary>
        /// Gets or sets the last name of the contact1.
        /// </summary>
        /// <value>
        /// The last name of the contact1.
        /// </value>
        public string Contact1LastName { get; set; } // Contact1LastName

        /// <summary>
        /// Gets or sets the contact1 e mail.
        /// </summary>
        /// <value>
        /// The contact1 e mail.
        /// </value>
        public string Contact1EMail { get; set; } // Contact1EMail

        /// <summary>
        /// Gets or sets the contact1 phone.
        /// </summary>
        /// <value>
        /// The contact1 phone.
        /// </value>
        public string Contact1Phone { get; set; } // Contact1Phone

        /// <summary>
        /// Gets or sets the first name of the contact2.
        /// </summary>
        /// <value>
        /// The first name of the contact2.
        /// </value>
        public string Contact2FirstName { get; set; } // Contact2FirstName

        /// <summary>
        /// Gets or sets the last name of the contact2.
        /// </summary>
        /// <value>
        /// The last name of the contact2.
        /// </value>
        public string Contact2LastName { get; set; } // Contact2LastName

        /// <summary>
        /// Gets or sets the contack2 e mail.
        /// </summary>
        /// <value>
        /// The contack2 e mail.
        /// </value>
        public string Contack2EMail { get; set; } // Contack2EMail

        /// <summary>
        /// Gets or sets the contact2 phone.
        /// </summary>
        /// <value>
        /// The contact2 phone.
        /// </value>
        public string Contact2Phone { get; set; } // Contact2Phone

        /// <summary>
        /// Gets or sets the phone2.
        /// </summary>
        /// <value>
        /// The phone2.
        /// </value>
        public string Phone2 { get; set; } // Phone2

        /// <summary>
        /// Gets or sets the phone3.
        /// </summary>
        /// <value>
        /// The phone3.
        /// </value>
        public string Phone3 { get; set; } // Phone3

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        public string Address2 { get; set; } // Address2

        /// <summary>
        /// Gets or sets the name of the city.
        /// </summary>
        /// <value>
        /// The name of the city.
        /// </value>
        public string City { get; set; } // CityName

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [Required(ErrorMessage = "Lütfen geçerli bir tip giriniz")]
        public int TypeId { get; set; } // Type. 0:Undefined, 1:Corporate, 2:Individual

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; set; } // Type. 0:Undefined, 1:Corporate, 2:Individual

        /// <summary>
        /// Gets or sets the type of the register.
        /// </summary>
        /// <value>
        /// The type of the register.
        /// </value>
        [Required(ErrorMessage = "Lütfen geçerli bir kayıt tipi giriniz")]
        public int RegisterTypeId { get; set; } // RegisterType. 0:Web_Register, 1:Web_Ticket, 2:App_SingleInsert, 3:App_ViaImport

        /// <summary>
        /// Gets or sets the name of the register type.
        /// </summary>
        /// <value>
        /// The name of the register type.
        /// </value>
        public string RegisterTypeName { get; set; } // RegisterType. 0:Web_Register, 1:Web_Ticket, 2:App_SingleInsert, 3:App_ViaImport

        /// <summary>
        /// Gets or sets the current role.
        /// </summary>
        /// <value>
        /// The current role.
        /// </value>
        public string CurrentRole { get; set; }

        /// <summary>
        /// Gets or sets the types.
        /// </summary>
        /// <value>
        /// The types.
        /// </value>
        public List<CustomerTypeDto> Types { get; set; }

        /// <summary>
        /// Gets or sets the types.
        /// </summary>
        /// <value>
        /// The types.
        /// </value>
        public List<CustomerRegisterTypeDto> RegisterTypes { get; set; }

        /// <summary>
        /// To the members customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns></returns>
        public static Customer ToMembersCustomer(CrmCustomerDto customer)
        {
            return new Customer
            {
                Address = customer.Address,
                CommercialTitle = customer.CommercialTitle,
                CompanyName = customer.CompanyName,
                Contack2EMail = customer.Contack2EMail,
                Contact1EMail = customer.Contact1EMail,
                Contact1FirstName = customer.Contact1FirstName,
                Contact1LastName = customer.Contact1LastName,
                Contact1Phone = customer.Contact1Phone,
                Contact2FirstName = customer.Contact2FirstName,
                Contact2LastName = customer.Contact2LastName,
                Contact2Phone = customer.Contact2Phone,
                Contry = customer.Contry,
                CreationTime = customer.CreationTime,
                CustomerId = customer.CustomerId,
                EMail = customer.EMail,
                Fax = customer.Fax,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Phone = customer.Phone,
                RegisterType = customer.RegisterTypeId,
                Status = true,
                TaxNumber = customer.TaxNumber,
                TaxOffice = customer.TaxOffice,
                Type = customer.TypeId
            };
        }

        /// <summary>
        /// To the CRM campaign dto.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="cities">The cities.</param>
        /// <param name="role">The role.</param>
        /// <returns></returns>
        public static CrmCustomerDto ToCrmCampaignDto(Members_Customer customer, string role)
        {

            return new CrmCustomerDto
            {

                Address = customer.Address??string.Empty,
                City = customer.CityId.ToString(),
                CommercialTitle = customer.CommercialTitle ?? string.Empty,
                CompanyName = customer.CompanyName ?? string.Empty,
                Contack2EMail = customer.Contack2EMail ?? string.Empty,
                Contact1EMail = customer.Contact1EMail ?? string.Empty,
                Contact1FirstName = customer.Contact1FirstName ?? string.Empty,
                Contact1LastName = customer.Contact1LastName ?? string.Empty,
                Contact1Phone = customer.Contact1Phone ?? string.Empty,
                Contact2FirstName = customer.Contact2FirstName ?? string.Empty,
                Contact2LastName = customer.Contact2LastName ?? string.Empty,
                Contact2Phone = customer.Contact2Phone ?? string.Empty,
                Contry = customer.Contry ?? string.Empty,
                CreationTime = customer.CreationTime,
                CustomerId = customer.CustomerId,
                EMail = customer.EMail ?? string.Empty,
                Fax = customer.Fax ?? string.Empty,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Phone = customer.Phone ?? string.Empty,
                RegisterTypeId = customer.RegisterType,
                RegisterTypeName = Enum.GetName(typeof(CustomerRegisterTypes), customer.RegisterType),
                Status = 1,
                TaxNumber = customer.TaxNumber ?? string.Empty,
                TaxOffice = customer.TaxOffice ?? string.Empty,
                TypeId = customer.Type,
                TypeName = Enum.GetName(typeof(CustomerTypes), customer.Type),
                CurrentRole = role
            };
        }
    }

}