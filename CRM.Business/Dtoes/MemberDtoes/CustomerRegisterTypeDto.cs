﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.ProductDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Member;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.MemberDtoes
{

    /// <summary>
    /// CrmCustomerDto
    /// </summary>
    public class CustomerRegisterTypeDto
    {
        /// <summary>
        /// Gets or sets the type identifier.
        /// </summary>
        /// <value>
        /// The type identifier.
        /// </value>
        public int RegisterTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string RegisterTypeName { get; set; }
    }

}