﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.OpportunityDtoes
{
    /// <summary>
    /// CrmRoleDto
    /// </summary>
    public class OpportunityDto
    {

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public int OpportunityTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets the shipping dtos.
        /// </summary>
        /// <returns></returns>
        public static List<OpportunityDto> GetSOpportunityDtoes()
        {
            return ((IEnumerable<OpportunityType>)Enum.GetValues(typeof(OpportunityType))).Select(
                c => new OpportunityDto() { OpportunityTypeId = (int)c, Name = c.ToString() }).ToList();
        } 
    }
}
