﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.ShippingDtoes;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.OrganizationDtoes
{
    /// <summary>
    /// CrmRoleDto
    /// </summary>
    public class OrganizationDto
    {

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public int OrganizationTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets the shipping dtos.
        /// </summary>
        /// <returns></returns>
        public static List<OrganizationDto> GetOrganizationDtos()
        {
            return ((IEnumerable<OrganizationType>)Enum.GetValues(typeof(OrganizationType))).Select(
                c => new OrganizationDto() { OrganizationTypeId = (int)c, Name = c.ToString() }).ToList();
        } 
    }
}
