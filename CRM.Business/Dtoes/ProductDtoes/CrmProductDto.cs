﻿namespace CRM.Business.Dtoes.ProductDtoes
{
    public class CrmProductDto
    {
        /// <summary>
        /// Gets or sets the member identifier.
        /// </summary>
        /// <value>
        /// The member identifier.
        /// </value>
        public int ProductNameId { get; set; }

        /// <summary>
        /// Gets or sets the name of the Product.
        /// </summary>
        /// <value>
        /// The name of the Product.
        /// </value>
        public string ProductName { get; set; }
    }
}
