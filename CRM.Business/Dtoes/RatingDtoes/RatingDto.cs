﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.SaleOrderDto;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.RatingDtoes
{
    /// <summary>
    /// CrmRoleDto
    /// </summary>
    public class RatingDto
    {

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public int RatingId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets the shipping dtos.
        /// </summary>
        /// <returns></returns>
        public static List<RatingDto> GetRatingDtos()
        {
            return ((IEnumerable<RatingType>)Enum.GetValues(typeof(RatingType))).Select(
                c => new RatingDto() { RatingId = (int)c, Name = c.ToString() }).ToList();
        } 
    }
}
