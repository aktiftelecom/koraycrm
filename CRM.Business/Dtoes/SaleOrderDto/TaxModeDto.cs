﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.ShippingDtoes;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.SaleOrderDto
{
    /// <summary>
    /// CrmRoleDto
    /// </summary>
    public class TaxModeDto
    {

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public int TaxModeId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets the shipping dtos.
        /// </summary>
        /// <returns></returns>
        public static List<TaxModeDto> GetTaxModeDtos()
        {
            return ((IEnumerable<TaxModeType>)Enum.GetValues(typeof(TaxModeType))).Select(
                c => new TaxModeDto() { TaxModeId = (int)c, Name = c.ToString() }).ToList();
        } 
    }
}
