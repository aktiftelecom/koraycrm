﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Core.Enums;

namespace CRM.Business.Dtoes.ShippingDtoes
{
    /// <summary>
    /// CrmRoleDto
    /// </summary>
    public class ShippingDto
    {

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public int ShippingId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets the shipping dtos.
        /// </summary>
        /// <returns></returns>
        public static List<ShippingDto> GetShippingDtos()
        {
            return ((IEnumerable<ShippingType>)Enum.GetValues(typeof(ShippingType))).Select(
                c => new ShippingDto() { ShippingId = (int)c, Name = c.ToString() }).ToList();
        } 
    }
}
