﻿namespace CRM.Business.Dtoes.UserDtoes
{
    /// <summary>
    /// CrmRoleDto
    /// </summary>
    public class CrmRoleDto
    {

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public string RoleId { get; set; }


        /// <summary>
        /// Gets or sets the name of the role.
        /// </summary>
        /// <value>
        /// The name of the role.
        /// </value>
        public string RoleName { get; set; }
    }
}
