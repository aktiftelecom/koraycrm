﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Campaign

{
    [Table("[Crm].[Campaigns]")]
    public partial class Crm_Campaign
    {
        [Key]
        public int CampaignId { get; set; } // CampaignId (Primary key)
        public bool IsDeleted { get; set; } // IsDeleted
        [Required(ErrorMessage = "Lütfen geçerli bir kampanya ismi giriniz")]
        public string CampaignName { get; set; } // CampaignName
        public int AssignTo { get; set; } // AssignTo. Atanan Kişi
        public int TypeId { get; set; } // TypeId
        public int StatusId { get; set; } // StatusId
        [Required(ErrorMessage = "Lütfen geçerli bir tarihi giriniz")]
        public DateTime ExpectedCloseDate { get; set; } // ExpectedCloseDate. Bitiş Tarihi
        public string TargetAudience { get; set; } // TargetAudience. Hedef İlgili
        public string Sponsor { get; set; } // Sponsor
        public int TargetSize { get; set; } // TargetSize
        [Required(ErrorMessage = "Lütfen geçerli bir Gönderilen Adet giriniz")]
        [Range(1, Int32.MaxValue)]
        public int NumberSent { get; set; } // NumberSent. Gönderilen Adet
        [Required(ErrorMessage = "Lütfen geçerli bir Bütçelenen Maliyet giriniz")]
        public double BudgetCost { get; set; } // BudgetCost. Bütçelenen Maliyet
        [Required(ErrorMessage = "Lütfen geçerli bir Beklenen Ciro giriniz")]
        public double ExpectedRevenue { get; set; } // ExpectedRevenue. Beklenen Ciro
        [Required(ErrorMessage = "Lütfen geçerli bir Beklenen Satış Adeti giriniz")]
        public int ExpectedSalesCount { get; set; } // ExpectedSalesCount. Beklenen Satış Adeti
        [Required(ErrorMessage = "Lütfen geçerli bir Beklenen Cevap Adeti giriniz")]
        public int ExpectedResponseCount { get; set; } // ExpectedResponseCount. Beklenen Cevap Adeti
        [Required(ErrorMessage = "Lütfen geçerli bir Beklenen ROI giriniz")]
        public double ExpectedROI { get; set; } // ExpectedROI. Beklenen ROI
        [Required(ErrorMessage = "Lütfen geçerli bir Geçekleşen Maliyet giriniz")]
        public double ActualCost { get; set; } // ActualCost. Geçekleşen Maliyet
        [Required(ErrorMessage = "Lütfen geçerli bir Beklenen cevap giriniz")]
        public int ExpectedResponse { get; set; } // ExpectedResponse. Beklenen Sonuç:  Excellent  Good  Average  Poor
        [Required(ErrorMessage = "Lütfen geçerli bir  gerçekleşen satış sayısı giriniz")]
        public int ActualSalesCount { get; set; } // ActualSalesCount
        [Required(ErrorMessage = "Lütfen geçerli bir Gerçekleşen Cevap Adeti giriniz")]
        public int ActualResponseCount { get; set; } // ActualResponseCount. Gerçekleşen Cevap Adeti
        [Required(ErrorMessage = "Lütfen geçerli bir Gerçekleşen ROI giriniz")]
        public double ActualROI { get; set; } // ActualROI. Gerçekleşen ROI
        public string Description { get; set; } // Description
        public int ProductId { get; set; } // ProductId
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy

        public Crm_Campaign()
        {
            IsDeleted = false;
            AssignTo = 0;
            TypeId = 0;
            StatusId = 0;
            ExpectedCloseDate = System.DateTime.Now;
            TargetSize = 0;
            NumberSent = 0;
            BudgetCost = 0;
            ExpectedRevenue = 0;
            ExpectedSalesCount = 0;
            ExpectedResponseCount = 0;
            ExpectedROI = 0;
            ActualCost = 0;
            ExpectedResponse = 0;
            ActualSalesCount = 0;
            ActualResponseCount = 0;
            ActualROI = 0;
            ProductId = 0;
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 0;
            CreatedTime = System.DateTime.Now;
            UpdatedBy = 0;
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
