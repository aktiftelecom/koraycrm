﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Campaign
{
    [Table("[Crm].[CampaignStatus]")]
    public partial class Crm_CampaignStatu
    {
        [Key]
        public int CampaignStatusId { get; set; } // CampaignStatusId (Primary key)
        public string Description { get; set; } // Description. Planning  Active   Inactive   Completed   Cancelled   InProgress   Queued   Draft   Sent   Failed
        partial void InitializePartial();
    }
}
