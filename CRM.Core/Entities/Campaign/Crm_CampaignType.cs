﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Campaign
{
     [Table("[Crm].[CampaignType]")]
    public partial class Crm_CampaignType
    {
        [Key]
        public int CampaignTypeId { get; set; } // CampaignTypeId (Primary key)
        public string Description { get; set; } // Description. Conference  Telepazarlama  E-Posta  Doğrudan Posta  Bant Reklam  Reklam  Referans Programı  Partners  Public Relations  Trade Show  Net sunumu  Diğer

        // Reverse navigation
        public virtual ICollection<Crm_Campaign> Crm_Campaign { get; set; } // Campaigns.FK_Campaign_CampaignType

        public Crm_CampaignType()
        {
            Crm_Campaign = new List<Crm_Campaign>();
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
