﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.City
{
    [Table("[This].[City]")]
    public partial class This_City
    {
        [Key]
        public int Code { get; set; } // Code (Primary key)
        public string Name { get; set; } // Name
        public int Contry { get; set; } // Contry

        public This_City()
        {
            Contry = 222;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
