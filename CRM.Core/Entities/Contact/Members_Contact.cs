﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Contact
{
    [Table("[Members].[Contact]")]
    public partial class Members_Contact
    {
        [Key]
        public int ContactId { get; set; } // ContactId
        public bool IsDeleted { get; set; } // IsDeleted
        [Required(ErrorMessage = "Lütfen geçerli bir isim giriniz")]
        public string FirstName { get; set; } // FirstName
        [Required(ErrorMessage = "Lütfen geçerli bir soyadı giriniz")]
        public string LastName { get; set; } // LastName
        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string PrimaryEmail { get; set; } // PrimaryEmail
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Phone is invalid")]
        public string OfficePhone { get; set; } // OfficePhone
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Phone is invalid")]
        public string MobilePhone { get; set; } // MobilePhone
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Phone is invalid")]
        public string HomePhone { get; set; } // OfficePhone
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Phone is invalid")]
        public string OtherPhone { get; set; } // OfficePhone
        public DateTime? DateOfBirth { get; set; } // DateOfBirth
        public string Fax { get; set; } // Fax
        public int CustomerId { get; set; } // CustomerId
        public string Title { get; set; } // Title
        public string Department { get; set; } // Department
        public bool Referance { get; set; } // Referance
        public int? ReportsTo { get; set; } // ReportsTo
        public int? LeadSourceId { get; set; } // LeadSourceId
        public int AssignedTo { get; set; } // AssignedTo
        public bool DoNotCall { get; set; } // DoNotCall
        public bool NotifyOwner { get; set; } // NotifyOwner
        public bool EmailOptionOut { get; set; } // EmailOptionOut
        public bool PortalUser { get; set; } // PortalUser
        public DateTime? SupportStartDate { get; set; } // SupportStartDate
        public DateTime? SupportEndDate { get; set; } // SupportEndDate
        public string MailingStreet { get; set; } // MailingStreet
        public string MailingPOBox { get; set; } // MailingPOBox
        public string MailingCity { get; set; } // MailingCity
        public string MailingState { get; set; } // MailingState
        public string MailingZip { get; set; } // MailingZip
        public string MailingCountry { get; set; } // MailingCountry
        public string OtherStreet { get; set; } // OtherStreet
        public string OtherPOBox { get; set; } // OtherPOBox
        public string OtherCity { get; set; } // OtherCity
        public string OtherState { get; set; } // OtherState
        public string OtherZip { get; set; } // OtherZip
        public string OtherCountry { get; set; } // OtherCountry
        public string Description { get; set; } // Description
        public byte[] ContactImage { get; set; } // ContactImage
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy
        public string Assistant { get; set; } // OtherState
        public int? DialogReference { get; set; }
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Phone is invalid")]
        public string AssistantPhone { get; set; } // OfficePhone
        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string SecondaryEmail { get; set; } // PrimaryEmail

        [NotMapped]
        public string CurrentRole { get; set; }

        [NotMapped]
        public string CurrentOperation { get; set; }

        [NotMapped]
        public string CustomerName { get; set; }

        [NotMapped]
        public string AssignPerson { get; set; }


        public Members_Contact()
        {
            AssignedTo = 1;
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 0;
            CreatedTime = System.DateTime.Now;
            UpdatedBy = 0;
            CustomerId = 226;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
