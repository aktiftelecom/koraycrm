﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Core.Entities.Currency
{
    [Table("[This].[Currency]")]
    public partial class This_Currency
    {
        [Key]
        public int CurrencyId { get; set; } // CurrencyId (Primary key)
        public bool IsActive { get; set; } // IsActive
        public string Name { get; set; } // Name
        public string Code { get; set; } // Code
        public double ConversionRate { get; set; } // ConversionRate
        public bool IsBase { get; set; } // IsBase

        public This_Currency()
        {
            IsActive = false;
            ConversionRate = 1;
            IsBase = false;
        }
        partial void InitializePartial();
    }
}
