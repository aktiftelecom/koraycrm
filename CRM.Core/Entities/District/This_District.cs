﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Core.Entities.District
{
    [Table("[This].[District]")]
    public partial class This_District
    {
        [Key]
        public int DistrictId { get; set; } // DistrictId (Primary key)
        public int? CityId { get; set; } // CityId
        public string DistrictName { get; set; } // DistrictName
    }
}
