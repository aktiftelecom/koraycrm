﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CRM.Core.Entities.Lead;

namespace CRM.Core.Entities.Industry
{
    [Table("[This].[Industry]")]
    public partial class This_Industry
    {
        [Key]
        public int IndustryId { get; set; } // IndustryId (Primary key)
        public string Description { get; set; } // Description


        public This_Industry()
        {
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
