﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CRM.Core.Entities.City;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Industry;

namespace CRM.Core.Entities.Lead
{
     [Table("[Crm].[Lead]")]
    public partial class Crm_Lead
    {
        [Key]
        public int LeadId { get; set; } // LeadId (Primary key)
        [Required(ErrorMessage = "Lütfen geçerli bir isim giriniz")]
        public string FirstName { get; set; } // FirstName
        [Required(ErrorMessage = "Lütfen geçerli bir soyisim giriniz")]
        public string LastName { get; set; } // LastName
        [Required(ErrorMessage = "Lütfen geçerli bir şirket ismi giriniz")]
        public string Company { get; set; } // Company
        [Required(ErrorMessage = "Lütfen geçerli bir email giriniz")]
        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string PrimaryEmail { get; set; } // PrimaryEmail
        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string SecondaryEmail { get; set; } // PrimaryEmail
        [Required(ErrorMessage = "Lütfen geçerli bir telefon giriniz")]
        public string PrimaryPhone { get; set; } // PrimaryPhone
        public string Designation { get; set; } // Designation. Ünvan
        public string MobilePhone { get; set; } // MobilePhone
        public int IndustryId { get; set; } // IndustryId. Sektör
        public string Website { get; set; } // Website
        [Required(ErrorMessage = "Lütfen geçerli yıllık kazanç giriniz")]
        public double AnnualRevenue { get; set; } // AnnualRevenue. Yıllık Ciro
        public int LeadSourceId { get; set; } // LeadSourceId. Kaynak Kaynağı (Nereden)
        public int LeadStatusId { get; set; } // LeadStatusId. Kaynak Statüsü
        public int AssignedTo { get; set; } // AssignedTo
        public string Fax { get; set; } // Fax
        [Required(ErrorMessage = "Lütfen geçerli bir çalışan sayısı giriniz")]
        public int NumberEmployees { get; set; } // NumberEmployees
        public int Rating { get; set; } // Rating. LeadRateTypes:  1-Acquired  2-Active  3-Market Failed  4-Project Cancelled  5-Shutdown
        public string Street { get; set; } // Street
        public string POBox { get; set; } // POBox
        public string PostalCode { get; set; } // PostalCode
        [Required(ErrorMessage = "Lütfen geçerli bir aday kaynağı giriniz")]
        public int CityId { get; set; } // CityId
        [Required(ErrorMessage = "Lütfen geçerli bir aday kaynağı giriniz")]
        public int DistrictId { get; set; } // DistrictId
        public bool IsActive { get; set; } // IsActive
        public bool EmailOptOut { get; set; } // EmailOptOut
        public string Description { get; set; } // Description
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy

        // Foreign keys
        [NotMapped]
        public List<Crm_LeadSource>  Crm_LeadSource { get; set; } // FK_Lead_LeadSource
        [Required(ErrorMessage = "Lütfen geçerli bir aday kaynağı giriniz")]
        [NotMapped]
        public string LeadSourceNameId { get; set; }

        [NotMapped]
        public List<Crm_LeadStatu> Crm_LeadStatu { get; set; } // FK_Lead_LeadStatus
        [Required(ErrorMessage = "Lütfen geçerli bir aday durumu giriniz")]
        [NotMapped]
        public string LeadStatuNameId { get; set; }

        [NotMapped]
        public List<This_Industry> This_Industry { get; set; } // FK_Lead_Industry
        [Required(ErrorMessage = "Lütfen geçerli bir endüstri giriniz")]
        [NotMapped]
        public string IndustryNameId { get; set; }

        [NotMapped]
        public string CurrentRole { get; set; }

        [NotMapped]
        public string CurrentOperation { get; set; }

        [NotMapped]
        public string AssignPerson { get; set; }

        public Crm_Lead()
        {
            IndustryId = 1;
            AnnualRevenue = 0;
            LeadSourceId = 1;
            LeadStatusId = 1;
            AssignedTo = 1;
            NumberEmployees = 0;
            Rating = 1;
            CityId = 34;
            DistrictId = 1103;
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 0;
            CreatedTime = System.DateTime.Now;
            UpdatedBy = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
