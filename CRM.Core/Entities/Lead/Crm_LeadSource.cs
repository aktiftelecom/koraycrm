﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CRM.Core.Entities.Opportunity;

namespace CRM.Core.Entities.Lead
{
    [Table("[Crm].[LeadSource]")]
    public partial class Crm_LeadSource
    {
        [Key]
        public int LeadSourceId { get; set; } // LeadSourceId (Primary key)
        public string Description { get; set; } // Description

        public Crm_LeadSource()
        {
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
