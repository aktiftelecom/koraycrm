﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Lead
{
    [Table("[Crm].[LeadStatus]")]
    public partial class Crm_LeadStatu
    {
        [Key]
        public int LeadStatusId { get; set; } // LeadStatusId (Primary key)
        public string Description { get; set; } // Description


        public Crm_LeadStatu()
        {
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
