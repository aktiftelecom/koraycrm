﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Core.Entities.Logs
{
     [Table("[dbo].[ErrorLog]")]
    public partial class ErrorLog
    {
        public int Id { get; set; } // Id (Primary key)
        public string ReferenceNumber { get; set; } // ReferenceNumber
        public string ExceptionType { get; set; } // ExceptionType
        public string ExceptionMessage { get; set; } // ExceptionMessage
        public string TargetSiteModule { get; set; } // TargetSiteModule
        public DateTime CreateDate { get; set; } // CreateDate
    }
}
