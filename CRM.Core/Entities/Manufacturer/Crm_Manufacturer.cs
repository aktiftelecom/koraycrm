﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Core.Entities.Manufacturer
{
    [Table("[Crm].[Manufacturer]")]
    public partial class Crm_Manufacturer
    {
        [Key]
        public int ManufacturerId { get; set; } // ManufacturerId (Primary key)
        public string Description { get; set; } // Description

        public Crm_Manufacturer()
        {
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
