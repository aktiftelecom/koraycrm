﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Member
{
    [Table("[dbo].[Customers]")]
    public partial class Customer
    {
        [Key]
        public int CustomerId { get; set; } // CustomerId (Primary key)
        public string FirstName { get; set; } // FirstName
        public string LastName { get; set; } // LastName
        public string EMail { get; set; } // EMail
        public string Address { get; set; } // Address
        public string Phone { get; set; } // Phone
        public string Fax { get; set; } // Fax
        public string TaxOffice { get; set; } // TaxOffice
        public string TaxNumber { get; set; } // TaxNumber
        public string City { get; set; } // City
        public bool? Status { get; set; } // Status
        public DateTime? CreationTime { get; set; } // CreationTime
        public string CompanyName { get; set; } // CompanyName
        public string CommercialTitle { get; set; } // CommercialTitle
        public string Contry { get; set; } // Contry
        public string Contact1FirstName { get; set; } // Contact1FirstName
        public string Contact1LastName { get; set; } // Contact1LastName
        public string Contact1EMail { get; set; } // Contact1EMail
        public string Contact1Phone { get; set; } // Contact1Phone
        public string Contact2FirstName { get; set; } // Contact2FirstName
        public string Contact2LastName { get; set; } // Contact2LastName
        public string Contack2EMail { get; set; } // Contack2EMail
        public string Contact2Phone { get; set; } // Contact2Phone
        public int? Type { get; set; } // Type
        public int? RegisterType { get; set; } // RegisterType

        public Customer()
        {
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
