﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using CRM.Core.Entities.City;
using CRM.Core.Enums;

namespace CRM.Core.Entities.Member
{
    [Table("[Members].[Customer]")]
    public partial class Members_Customer
    {
        [Key]
        public int CustomerId { get; set; } // MemberId (Primary key)
        public DateTime CreationTime { get; set; } // CreationTime
        [Required(ErrorMessage = "Lütfen geçerli bir şirket ismi giriniz")]
        public string CompanyName { get; set; } // CompanyName
        public string CommercialTitle { get; set; } // CommercialTitle
        public string Address { get; set; } // Address
        public int? CityId { get; set; } // CityId
        public string Phone { get; set; } // Phone
        public string Fax { get; set; } // Fax
        public string TaxOffice { get; set; } // TaxOffice
        public string TaxNumber { get; set; } // TaxNumber
        public string Contry { get; set; } // Contry
        [Required(ErrorMessage = "Lütfen geçerli bir müşteri ismi giriniz")]
        public string FirstName { get; set; } // FirstName
        public string LastName { get; set; } // LastName
         [Required(ErrorMessage = "Lütfen geçerli bir email giriniz")]
         [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string EMail { get; set; } // EMail
        public string Password { get; set; } // Password
        public int Status { get; set; } // Status. 1 bit: Active,  , 128 bit:Deleted
        public string Contact1FirstName { get; set; } // Contact1FirstName
        public string Contact1LastName { get; set; } // Contact1LastName
        public string Contact1EMail { get; set; } // Contact1EMail
        public string Contact1Phone { get; set; } // Contact1Phone
        public string Contact2FirstName { get; set; } // Contact2FirstName
        public string Contact2LastName { get; set; } // Contact2LastName
        public string Contack2EMail { get; set; } // Contack2EMail
        public string Contact2Phone { get; set; } // Contact2Phone
        public string Phone2 { get; set; } // Phone2
        public string Phone3 { get; set; } // Phone3
        public string Address2 { get; set; } // Address2
        public int Type { get; set; } // Type. 0:Undefined, 1:Corporate, 2:Individual
        public int RegisterType { get; set; } // RegisterType. 0:Web_Register, 1:Web_Ticket, 2:App_SingleInsert, 3:App_ViaImport
         [NotMapped]
        public string TypeName { get; set; }
         [NotMapped]
        public string RegisterTypeName { get; set; }
         [NotMapped]
         public string CityName { get; set; }
         [NotMapped]
        public string CurrentRole { get; set; }
         [NotMapped]
        public List<Tuple<int,string>> Types { get; set; }
         [NotMapped]
        public List<Tuple<int,string>> RegisterTypes { get; set; }
         [NotMapped]
         public List<This_City> Cities { get; set; }

        public Members_Customer()
        {
            CreationTime = System.DateTime.Now;
            Password = "password";
            Status = 1;
            CreationTime = DateTime.Now;
            Types = ((IEnumerable<CustomerTypes>)Enum.GetValues(typeof(CustomerTypes))).Select(c => new Tuple<int, string> ((int)c, c.ToString())).ToList();
            RegisterTypes = ((IEnumerable<CustomerRegisterTypes>)Enum.GetValues(typeof(CustomerRegisterTypes))).Select(c => new Tuple<int, string> ((int)c, c.ToString())).ToList();
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
