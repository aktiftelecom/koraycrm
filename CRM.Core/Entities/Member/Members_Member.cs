﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Member
{
    [Table("[Members].[Member]")]
    public partial class Members_Member
    {
        [Key]
        public int MemberId { get; set; } // MemberId (Primary key)
        public DateTime CreationTime { get; set; } // CreationTime
        public bool IsGroup { get; set; } // IsGroup
        public int ParentId { get; set; } // ParentId. 0 if is group
        public int L { get; set; } // L
        public int R { get; set; } // R
        public int CustomerId { get; set; } // CustomerId
        public string LoginName { get; set; } // LoginName
        public string FirstName { get; set; } // FirstName
        public string LastName { get; set; } // LastName
        public string Email { get; set; } // Email
        public string Phone { get; set; } // Phone
        public string Password { get; set; } // Password
        public byte[] PassEncrypted { get; set; } // PassEncrypted
        public byte[] HashedPass { get; set; } // HashedPass
        public int PassRetries { get; set; } // PassRetries
        public DateTime? LastLoginTime { get; set; } // LastLoginTime
        public int VisitsCount { get; set; } // VisitsCount
        public int AllowedSections { get; set; } // AllowedSections. from Members.From sections
        public Guid? LastAccessKey { get; set; } // LastAccessKey
        public int? LanguageId { get; set; } // LanguageId. id 1: tr, 2: en, 3: de, 4: ru
        public int Status { get; set; } // Status. 1 bit: Active/Operator,     8 bit:,   16 bit: Deletion prohibited,   32: IP Locked,   64 bit: Disabled,   128 bit : Deleted
        public int Type { get; set; } // Type. 1: Admin operator, 2:InternalMember, 4:Customer, 8:UnregisteredCustomer, 16:Customers Root, 32: ?
        public int UserGender { get; set; } // UserGender. 0: Tanımsız, 1: Bayan, 2: Erkek
        public int VisualParams { get; set; } // VisualParams. 1 bit: Layout left/center, 2 bit: Menu navigation to right/left / Side bar to left/right, 4 bit: Menu navigation to top / Side bar to left,
        public bool UseParentPermission { get; set; } // UseParentPermission
        public long Permissions { get; set; } // Permissions
        public int MailEvents { get; set; } // MailEvents. 1: Müşteri talebi açıldığında, 2:, 4:
        public byte[] UpdateCheck { get; internal set; } // UpdateCheck
        public byte[] ICalender { get; set; } // ICalender

        public Members_Member()
        {
            CreationTime = System.DateTime.Now;
            IsGroup = false;
            ParentId = 0;
            CustomerId = 0;
            PassRetries = 0;
            VisitsCount = 0;
            AllowedSections = 0;
            LanguageId = 1;
            Status = 0;
            Type = 0;
            UserGender = 0;
            VisualParams = 5;
            UseParentPermission = true;
            Permissions = 0;
            MailEvents = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
