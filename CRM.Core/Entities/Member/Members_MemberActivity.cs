﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CRM.Core.Entities.Member
{
    public partial class Members_MemberActivity
    {
        [Key]
        public int ActivityId { get; set; } // ActivityId (Primary key)
        public DateTime CreationTime { get; set; } // CreationTime
        public int ByWhomId { get; set; } // ByWhomId
        public int ToId { get; set; } // ToId
        public int Module { get; set; } // Module. Module
        public int Type { get; set; } // Type. DbProcessType
        public string Result { get; set; } // Result

        public Members_MemberActivity()
        {
            CreationTime = System.DateTime.Now;
            ByWhomId = 0;
            Module = 0;
            Type = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
