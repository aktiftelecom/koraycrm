﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Member
{
    [Table("[Members].[MemberRole]")]
    public partial class Members_MemberRole
    {
        [Key]
        public int Id { get; set; } // Id (Primary key)
        public int MemberId { get; set; } // MemberId
        public int RoleId { get; set; } // RoleId

        public Members_MemberRole()
        {   
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
