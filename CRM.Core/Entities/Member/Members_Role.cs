﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Member
{
    [Table("[Members].[Role]")]
    public partial class Members_Role
    {
        [Key]
        public int RoleId { get; set; } // RoleId (Primary key)
        public string Name { get; set; } // Name
        public int Status { get; set; } // Status

        public Members_Role()
        {
            Status = 1;
            
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
