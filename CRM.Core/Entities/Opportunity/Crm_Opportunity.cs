﻿using System;
using System.ComponentModel.DataAnnotations;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Sale;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Opportunity
{
    // Opportunity
    [Table("[Crm].[Opportunity]")]
    public partial class Crm_Opportunity
    {
        [Key]
        public int OpportunityId { get; set; } // OpportunityId (Primary key)
        [Required(ErrorMessage = "Lütfen geçerli bir isim giriniz")]
        public string Name { get; set; } // Name
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public double Amount { get; set; } // Amount. Tutar
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public int CustomerId { get; set; } // CustomerId. İlgili/Müşteri
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public int ContactId { get; set; } // ContactId. İlgili kişi
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public DateTime ExpectedCloseDate { get; set; } // ExpectedCloseDate
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public int SaleStageId { get; set; } // SaleStageId
        public int AssignedTo { get; set; } // AssignedTo
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public int LeadSourceId { get; set; } // LeadSourceId
        public string NextStep { get; set; } // NextStep
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public int Type { get; set; } // Type. 1-Ek iş / Existing bussines  2-Yeni iş / New bussines
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public int Probability { get; set; } // Probability
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public int CampaignId { get; set; } // CampaignId
        [Required(ErrorMessage = "Lütfen geçerli bir tutar giriniz")]
        public double ForecastAmount { get; set; } // ForecastAmount
        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string PrimaryEmail { get; set; } // PrimaryEmail
        public string Description { get; set; } // Description
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted
        [NotMapped]
        public string CurrentRole { get; set; }

        [NotMapped]
        public string CurrentOperation { get; set; }

        [NotMapped]
        public string ContactName { get; set; }

        [NotMapped]
        public string SaleStageName { get; set; }

        [NotMapped]
        public string LeadSourceName { get; set; }

        [NotMapped]
        public string CustomerName { get; set; }

        [NotMapped]
        public string AssignedMember { get; set; }

        public Crm_Opportunity()
        {
            Amount = 0;
            CustomerId = 226;
            ContactId = 13;
            ExpectedCloseDate = System.DateTime.Now;
            AssignedTo = 1;
            LeadSourceId = 1;
            SaleStageId = 1;
            Type = 1;
            Probability = 50;
            CampaignId = 2;
            ForecastAmount = 0;
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 0;
            CreatedTime = System.DateTime.Now;
            UpdatedBy = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
