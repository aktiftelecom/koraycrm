﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Organization
{
    // Opportunity
    [Table("[Crm].[Organizations]")]
    public partial class Crm_Organization
    {
        [Key]
        public int Id { get; set; } // Id (Primary key)
        [Required(ErrorMessage = "Lütfen geçerli bir organizasyon ismi giriniz")]
        public string OrganizationName { get; set; } // OrganizationName
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Phone is invalid")]
        public string PrimaryPhone { get; set; } // PrimaryPhone
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Fax is invalid")]
        public string Fax { get; set; } // Fax
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Phone is invalid")]
        public string SecondaryPhone { get; set; } // SecondaryPhone
        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string PrimaryEmail { get; set; } // PrimaryEmail
        public string Ownership { get; set; } // Ownership
        public int? Rating { get; set; } // Rating
        public string SICCode { get; set; } // SICCode
        public decimal? AnnualRevenue { get; set; } // AnnualRevenue
        public bool? NotifyOwner { get; set; } // NotifyOwner
        public string Website { get; set; } // Website
        public string TickerSymbol { get; set; } // TickerSymbol
        public int? MemberOf { get; set; } // MemberOf
        public int? Employees { get; set; } // Employees
        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string SecondaryEmail { get; set; } // SecondaryEmail
        public int? Industry { get; set; } // Industry
        public int? Type { get; set; } // Type
        public bool? EmailOptOut { get; set; } // EmailOptOut
        public bool IsDeleted { get; set; }
        public string BillingAddress { get; set; } // BillingAddress
        public string BillingPOBox { get; set; } // BillingPOBox
        public int BillingCity { get; set; } // BillingCity
        public int BillingState { get; set; } // BillingState
        public string BillingPostalCode { get; set; } // BillingPostalCode
        public string BillingCountry { get; set; } // BillingCountry
        public string ShippingAddress { get; set; } // ShippingAddress
        public string ShippingPOBox { get; set; } // ShippingPOBox
        public int ShippingCity { get; set; } // ShippingCity
        public int ShippingState { get; set; } // ShippingState
        public string ShippingPostalCode { get; set; } // ShippingPostalCode
        public string ShippingCountry { get; set; } // ShippingCountry
        public string Description { get; set; } // Description
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy
        public int MainOrganizationId { get; set; }
        public int AssignedTo { get; set; } // AssignedTo
        [NotMapped]
        public string CurrentRole { get; set; }
        [NotMapped]
        public string CurrentOperation { get; set; }
        [NotMapped]
        public string AssignedMember { get; set; }
        [NotMapped]
        public string CityName { get; set; }

        public Crm_Organization()
        {
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 1;
            CreatedTime = System.DateTime.Now;
            UpdatedBy = 1;
            BillingCity = 34;
            BillingState = 1103;
            ShippingCity = 34;
            ShippingState = 1103;
            AssignedTo = 1;
            MainOrganizationId = 1;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
