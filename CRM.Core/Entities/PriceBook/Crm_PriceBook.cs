﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.PriceBook
{
    [Table("[Crm].[PriceBook]")]
    public partial class Crm_PriceBook
    {
        [Key]
        public int PriceBookId { get; set; } // PriceBookId (Primary key)
        [Required(ErrorMessage = "Lütfen geçerli bir isim giriniz")]
        public string PriceBookName { get; set; } // PriceBookName
        public int CurrencyId { get; set; } // CurrencyId
        public string Description { get; set; } // Description
        public bool Status { get; set; } // Status
        public DateTime CreatedTime { get; set; } // CreatedTime
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy

        [NotMapped]
        public string CurrentRole { get; set; }
        [NotMapped]
        public string CurrentOperation { get; set; }
        [NotMapped]
        public string StatusName { get; set; }
        [NotMapped]
        public string CurrencyName { get; set; }

        public Crm_PriceBook()
        {
            
            CurrencyId = 1;
            Status = true;
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 0;
            CreatedTime = System.DateTime.Now;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
