﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.Manufacturer;
using CRM.Core.Entities.Unit;
using CRM.Core.Entities.Vendor;
using CRM.Core.Enums;

namespace CRM.Core.Entities.Product
{
    [Table("[Crm].[Product]")]
    public partial class Crm_Product
    {
        [Key]
        public int ProductId { get; set; } // ProductId (Primary key)
        [Required(ErrorMessage = "Lütfen geçerli bir ürün ismi giriniz")]
        public string Name { get; set; } // Name
        public bool IsActive { get; set; } // IsActive
        public int Category { get; set; } // Category. 1-Hardware  2-Software  3-CRM Applications  4-...
        public int VendorId { get; set; } // VendorId
        public int ManufacturerId { get; set; } // ManufacturerId
        public DateTime? SalesStartDate { get; set; } // SalesStartDate
        public DateTime? SalesEndDate { get; set; } // SalesEndDate
        public DateTime? SupportStartDate { get; set; } // SupportStartDate
        public DateTime? SupportExpiryDate { get; set; } // SupportExpiryDate
        public string SerialNo { get; set; } // SerialNo
        public string VendorPartNo { get; set; } // VendorPartNo
        public string ManufacturPartNo { get; set; } // ManufacturPartNo
        public string Website { get; set; } // Website
        public string GLAccount { get; set; } // GLAccount
        [Required(ErrorMessage = "Lütfen geçerli bir ürün fiyatı giriniz")]
        [Range(1, 10, ErrorMessage = "The unit price must be between 1 and 10")]
        public double UnitPrice { get; set; } // UnitPrice
        [Required(ErrorMessage = "Lütfen geçerli bir vergi oranı giriniz")]
        [Range(1, 100, ErrorMessage = "The Percent VAT must be between 1 and 10")]
        public double PercentVAT { get; set; } // PercentVAT
        [Required(ErrorMessage = "Lütfen geçerli bir satış miktarı giriniz")]
        [Range(1, 100, ErrorMessage = "The Percent Sales must be between 1 and 10")]
        public double PercentSales { get; set; } // PercentSales
        [Required(ErrorMessage = "Lütfen geçerli bir servis miktarı giriniz")]
        [Range(1, 100, ErrorMessage = "The Percent Service must be between 1 and 10")]
        public double PercentService { get; set; } // PercentService
        [Required(ErrorMessage = "Lütfen geçerli bir ürün maliyeti giriniz")]
        [Range(1, 100, ErrorMessage = "The Purchase Cost must be between 1 and 10")]
        public double PurchaseCost { get; set; } // PurchaseCost
        public int UsageUnitId { get; set; } // UsageUnitId
        public int CurrencyId { get; set; } // CurrencyId
        [Required(ErrorMessage = "Lütfen geçerli bir komisyon oranı giriniz")]
        [Range(1, 100, ErrorMessage = "The Purchase Cost must be between 1 and 10")]
        public int CommissionRate { get; set; } // CommissionRate
        [Required(ErrorMessage = "Lütfen geçerli bir ürün sayısı giriniz")]
        [Range(1, 100, ErrorMessage = "The Purchase Cost must be between 1 and 10")]
        public int QuantityPerUnit { get; set; } // QuantityPerUnit
        [Required(ErrorMessage = "Lütfen geçerli bir stok miktarı giriniz")]
        [Range(1, 100, ErrorMessage = "The Purchase Cost must be between 1 and 10")]
        public int QuantityInStock { get; set; } // QuantityInStock
        [Required(ErrorMessage = "Lütfen geçerli bir sipariş seviyesi giriniz")]
        [Range(1, 100, ErrorMessage = "The Purchase Cost must be between 1 and 10")]
        public int ReorderLevel { get; set; } // ReorderLevel
        public int Handler { get; set; } // Handler
        [Required(ErrorMessage = "Lütfen geçerli bir talep miktarı giriniz")]
        [Range(1, 100, ErrorMessage = "The Purchase Cost must be between 1 and 10")]
        public int QuantityInDemand { get; set; } // QuantityInDemand
        public byte[] ProductImage { get; set; } // ProductImage
        public string PartNo { get; set; } // PartNo
        public string ProductPage { get; set; } // ProductPage
        public string Description { get; set; } // Description
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy

        [NotMapped]
        public List<Crm_Manufacturer> Manufacturers { get; set; }
        [Required(ErrorMessage = "Lütfen geçerli bir üretici giriniz")]
        [NotMapped]
        public string ManufacturerNameId { get; set; }
        [NotMapped]
        public List<Tuple<int, string>> Categories { get; set; }
        [Required(ErrorMessage = "Lütfen geçerli bir kategori giriniz")]
        [NotMapped]
        public string CategoryNameId { get; set; }
        [NotMapped]
        public List<Crm_Vendor> Vendors { get; set; }
        [Required(ErrorMessage = "Lütfen geçerli bir satıcı giriniz")]
        [NotMapped]
        public string VendorNameId { get; set; }
        [NotMapped]
        public List<This_Unit> Units { get; set; }
        [Required(ErrorMessage = "Lütfen geçerli bir ürün adeti tipi giriniz")]
        [NotMapped]
        public string UnitNameId { get; set; }
        [NotMapped]
        public List<This_Currency> Currencies { get; set; }
        [Required(ErrorMessage = "Lütfen geçerli bir döviz tipi giriniz")]
        [NotMapped]
        public string CurrencyNameId { get; set; }
        [NotMapped]
        public string CurrentRole { get; set; }
        [NotMapped]
        public string CurrentOperation { get; set; }

        public Crm_Product()
        {
            Handler = 1;
            IsActive = true;
            Category = 0;
            VendorId = 0;
            ManufacturerId = 0;
            UnitPrice = 0;
            PercentVAT = 0;
            PercentSales = 0;
            PercentService = 0;
            PurchaseCost = 0;
            UsageUnitId = 1;
            CurrencyId = 1;
            CommissionRate = 0;
            QuantityPerUnit = 0;
            QuantityInStock = 0;
            ReorderLevel = 0;
            Handler = 0;
            QuantityInDemand = 0;
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 0;
            CreatedTime = System.DateTime.Now;
            UpdatedBy = 0;
            Categories = ((IEnumerable<ProductCategoryTypes>)Enum.GetValues(typeof(ProductCategoryTypes))).Select(c => new Tuple<int, string>((int)c, c.ToString())).ToList();
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
