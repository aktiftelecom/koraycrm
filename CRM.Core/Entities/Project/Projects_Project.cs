﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Project
{
    [Table("[Projects].[Project]")]
    public partial class Projects_Project
    {
        [Key]
        public int ProjectId { get; set; } // ProjectId (Primary key)
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime CreationTime { get; set; } // CreationTime
        public int CreatedBy { get; set; } // CreatedBy
        public string Definition { get; set; } // Definition
        public int? CustomerId { get; set; } // CustomerId
        public int DepartmentId { get; set; } // DepartmentId. Müşteri  Satış  İş Geliştirme  Integrity  Yazılım  İş Geliştirme&Satış  3rd Party  Integrity&Yazılım  Teknik Destek
        public int StageId { get; set; } // StageId
        public int CollectionStatus { get; set; } // CollectionStatus. Belirtilmedi=0  Tahsil Edilmedi = 1  Tahsil Edildi = 2  Kısmi Tahsilat = 3
        public DateTime? ReceiptFromSales { get; set; } // ReceiptFromSales
        public DateTime? DueTime { get; set; } // DueTime
        public int RemainingDays { get; set; } // RemainingDays
        public int? QuoteId { get; set; } // QuoteId
        public DateTime? DeliveryTime { get; set; } // DeliveryTime
        public string LastStatusDescription { get; set; } // LastStatusDescription
        public string DelayExplanation { get; set; } // DelayExplanation

       

        public Projects_Project()
        {
            IsDeleted = false;
            CreationTime = System.DateTime.Now;
            CreatedBy = 0;
            StageId = 0;
            CollectionStatus = 0;
            RemainingDays = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
