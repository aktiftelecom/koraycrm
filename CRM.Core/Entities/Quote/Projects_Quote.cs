﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Core.Entities.Quote
{
    [Table("[Projects].[Quote]")]
    public partial class Projects_Quote
    {
        [Key]
        public int QuoteId { get; set; } // QuoteId (Primary key)
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime CreationTime { get; set; } // CreationTime
        public int ProjectId { get; set; } // ProjectId
        [Required(ErrorMessage = "Lütfen geçerli bir tarih giriniz")]
        public DateTime QuoteTime { get; set; } // QuoteTime
        public string LastStatusDescription { get; set; } // LastStatusDescription
        public int CreatedBy { get; set; } // CreatedBy
        public int ContactId { get; set; } // ContactId
        public int? LeadId { get; set; } // LeadId
        public int AssingedTo { get; set; } // AssingedTo
        public int StageId { get; set; } // StageId. Created  Delivered  Reviewed  Accepted  Rejected
        public int Shipping { get; set; } // Shipping. Taşıyıcı/Nakliye:  1 Yurtiçi  2 Aras  3 MNG  4 FedEx  5 UPS  6 USPS  7 DHL  8 BlueDart
        public string BillingAddress { get; set; } // BillingAddress
        public string ShippingAddress { get; set; } // ShippingAddress
        [Required(ErrorMessage = "Lütfen geçerli bir miktar giriniz")]
        public int ItemsTotal { get; set; } // ItemsTotal
        [Required(ErrorMessage = "Lütfen geçerli bir miktar giriniz")]
        public double Discount { get; set; } // Discount
        [Required(ErrorMessage = "Lütfen geçerli bir miktar giriniz")]
        public double ShippingCharges { get; set; } // ShippingCharges. Nakliye masrafı
        [Required(ErrorMessage = "Lütfen geçerli bir miktar giriniz")]
        public double PreTaxTotal { get; set; } // PreTaxTotal
        [Required(ErrorMessage = "Lütfen geçerli bir miktar giriniz")]
        public double Tax { get; set; } // Tax
        [Required(ErrorMessage = "Lütfen geçerli bir miktar giriniz")]
        public double Adjustment { get; set; } // Adjustment
        [Required(ErrorMessage = "Lütfen geçerli bir miktar giriniz")]
        public double TaxesForShipping { get; set; } // TaxesForShipping
        [Required(ErrorMessage = "Lütfen geçerli bir miktar giriniz")]
        public double GrandTotal { get; set; } // GrandTotal
        [NotMapped]
        public string CurrentRole { get; set; }

       

        public Projects_Quote()
        {
            IsDeleted = false;
            CreationTime = System.DateTime.Now;
            CreatedBy = 1;  
            ProjectId = 3;
            ContactId = 13;
            LeadId = 2;
            AssingedTo = 0;
            StageId = 1;
            Shipping = 1;
            ItemsTotal = 0;
            Discount = 0;
            ShippingCharges = 0;
            PreTaxTotal = 0;
            Tax = 0;
            Adjustment = 0;
            TaxesForShipping = 0;
            GrandTotal = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
