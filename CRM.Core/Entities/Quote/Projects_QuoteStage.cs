﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Quote
{
     [Table("[Projects].[QuoteStage]")]
    public partial class Projects_QuoteStage
    {
         [Key]
        public int QuoteStageId { get; set; } // QuoteStageId (Primary key)
        public string Description { get; set; } // Description
    }
}
