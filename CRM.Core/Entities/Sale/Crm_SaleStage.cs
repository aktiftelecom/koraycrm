﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CRM.Core.Entities.Opportunity;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Sale
{
    // SaleStage
    [Table("[Crm].[SaleStage]")]
    public partial class Crm_SaleStage
    {
        [Key]
        public int SaleStageId { get; set; } // SaleStageId (Primary key)
        public string Description { get; set; } // Description

        public Crm_SaleStage()
        {
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
