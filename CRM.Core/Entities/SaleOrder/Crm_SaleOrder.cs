﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.SaleOrder
{
    [Table("[Crm].[SaleOrder]")]
    public partial class Crm_SaleOrder
    {
        [Key]
        public int SaleOrderId { get; set; } // SaleOrderId (Primary key)
        [Required(ErrorMessage = "Lütfen geçerli bir subject ismi giriniz")]
        public string Subject { get; set; } // Subject
        public int? CustomerId { get; set; } // CustomerId
        public string PurchaseOrder { get; set; } // PurchaseOrder
        public DateTime? DueDate { get; set; } // DueDate
        public string Pending { get; set; } // Pending
        public decimal? SalesCommission { get; set; } // SalesCommission
        public int? OrganizationId { get; set; } // OrganizationId
        public int? OpportunityId { get; set; } // OpportunityId
        public int? ContactId { get; set; } // ContactId
        public int? CarrierShippingType { get; set; } // CarrierShippingType
        public int Status { get; set; } // Status
        public string ExciseDuty { get; set; } // ExciseDuty
        public bool? EnableRecurring { get; set; } // EnableRecurring
        public DateTime? StartPeriod { get; set; } // StartPeriod
        public int? PaymentDuration { get; set; } // PaymentDuration
        public int? CopyBillingAddressFrom { get; set; } // CopyBillingAddressFrom
        [Required(ErrorMessage = "Lütfen geçerli bir adres giriniz")]
        public string BillingAddress { get; set; } // BillingAddress
        public string BillingPOBox { get; set; } // BillingPOBox
        public int? BillingCity { get; set; } // BillingCity
        public int? BillingState { get; set; } // BillingState
        public string BillingPostalCode { get; set; } // BillingPostalCode
        public string BillingCountry { get; set; } // BillingCountry
        [Required(ErrorMessage = "Lütfen geçerli bir adres giriniz")]
        public string ShippingAddress { get; set; } // ShippingAddress
        public string ShippingPOBox { get; set; } // ShippingPOBox
        public int? ShippingCity { get; set; } // ShippingCity
        public int? ShippingState { get; set; } // ShippingState
        public string ShippingPostalCode { get; set; } // ShippingPostalCode
        public string ShippingCountry { get; set; } // ShippingCountry
        public string TermsConditions { get; set; } // TermsConditions
        public string Description { get; set; } // Description
        public int? Currency { get; set; } // Currency
        public int? TaxMode { get; set; } // TaxMode
        public int? Quantity { get; set; } // Quantity
        public decimal? ListPrice { get; set; } // ListPrice
        public decimal? ItemsTotal { get; set; } // ItemsTotal
        public decimal? ShippingHandlingCharges { get; set; } // ShippingHandlingCharges
        public decimal? PreTaxTotal { get; set; } // PreTaxTotal
        public decimal? Tax { get; set; } // Tax
        public decimal? TaxesForShippingAndHandling { get; set; } // TaxesForShippingAndHandling
        public decimal? Adjustment { get; set; } // Adjustment
        public decimal? GrandTotal { get; set; } // GrandTotal
        public DateTime? UpdatedTime { get; set; } // UpdatedTime
        public int? CreatedBy { get; set; } // CreatedBy
        public DateTime? CreatedTime { get; set; } // CreatedTime
        public int? UpdatedBy { get; set; } // UpdatedBy
        public bool? IsActive { get; set; } // IsActive
        [NotMapped]
        public string CurrentRole { get; set; }

        public Crm_SaleOrder()
        {
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 1;
            CreatedTime = System.DateTime.Now;
            UpdatedBy = 1;
            BillingCity = 34;
            BillingState = 1103;
            ShippingCity = 34;
            ShippingState = 1103;
            Quantity = 0;
            ListPrice = 0;
            ItemsTotal = 0;
            ShippingHandlingCharges = 0;
            PreTaxTotal = 0;
            Tax = 0;
            TaxesForShippingAndHandling = 0;
            Adjustment = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
