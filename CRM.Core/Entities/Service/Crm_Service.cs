﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Core.Entities.Service
{
    [Table("[Crm].[Service]")]
    public partial class Crm_Service
    {
        [Key]
        public int ServiceId { get; set; } // ServiceId (Primary key)
        [Required(ErrorMessage = "Lütfen geçerli bir servis ismi giriniz")]
        public string Name { get; set; } // Name
        public int CategoryId { get; set; } // CategoryId
        public int OwnerId { get; set; } // OwnerId
        public int UsageUnitId { get; set; } // UsageUnitId
        [Required(ErrorMessage = "Lütfen geçerli bir ünite sayısı giriniz")]
        public int NumberOfUnits { get; set; } // NumberOfUnits
        public DateTime? SalesStartDate { get; set; } // SalesStartDate
        public DateTime? SalesEndDate { get; set; } // SalesEndDate
        public DateTime? SupportStartDate { get; set; } // SupportStartDate
        public DateTime? SupportExpiryDate { get; set; } // SupportExpiryDate
        public string Website { get; set; } // Website
        [Required(ErrorMessage = "Lütfen geçerli bir fiyat giriniz")]
        public double Price { get; set; } // Price
        public int CurrencyId { get; set; } // CurrencyId
        [Required(ErrorMessage = "Lütfen geçerli bir komisyon oranı giriniz")]
        public double CommissionRate { get; set; } // CommissionRate
        [Required(ErrorMessage = "Lütfen geçerli bir PercentVAT giriniz")]
        public double PercentVAT { get; set; } // PercentVAT
        [Required(ErrorMessage = "Lütfen geçerli bir satış yüzdesi giriniz")]
        public double PercentSales { get; set; } // PercentSales
        [Required(ErrorMessage = "Lütfen geçerli bir servis yüzdesi giriniz")]
        public double PercentService { get; set; } // PercentService
        [Required(ErrorMessage = "Lütfen geçerli bir satın alma maliyeti giriniz")]
        public double PurchaseCost { get; set; } // PurchaseCost
        public string Description { get; set; } // Description
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy
        public DateTime UpdatedTime { get; set; } // UpdatedTime

        [NotMapped]
        public string CurrentRole { get; set; }
        [NotMapped]
        public string CurrentOperation { get; set; }

        [NotMapped]
        public string UsageUnitName { get; set; }

        [NotMapped]
        public string OwnerName { get; set; }

        [NotMapped]
        public string CategoryName { get; set; }

        public Crm_Service()
        {
            CategoryId = 1;
            OwnerId = 0;
            UsageUnitId = 1;
            NumberOfUnits = 0;
            Price = 0;
            CurrencyId = 1;
            CommissionRate = 0;
            PercentVAT = 0;
            PercentSales = 0;
            PercentService = 0;
            PurchaseCost = 0;
            CreatedBy = 0;
            CreatedTime = DateTime.Now;
            UpdatedBy = 0;
            UpdatedTime = DateTime.Now;
            IsActive = true;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
