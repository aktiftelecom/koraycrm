﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.Service
{
    [Table("[Crm].[ServiceCategory]")]
    public partial class Crm_ServiceCategory
    {
         [Key]
        public int CategoryId { get; set; } // CategoryId (Primary key)
        public string Description { get; set; } // Description
    }
}
