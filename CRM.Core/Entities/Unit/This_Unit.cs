﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Core.Entities.Unit
{
    [Table("[This].[Unit]")]
    public partial class This_Unit
    {
        [Key]
        public int UnitId { get; set; } // UnitId (Primary key)
        public string Abbreviation { get; set; } // Abbreviation
        public string Text { get; set; } // Text
        public int Category { get; set; } // Category. 1 bit: Services

        public This_Unit()
        {
            Category = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
