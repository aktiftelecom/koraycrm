﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.User
{
     [Table("[dbo].[AspNetRoles]")]
    public partial class AspNetRole
    {
        [Key]
        public string Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name

        public AspNetRole()
        {
            Name = string.Empty;
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
