﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.User
{
     [Table("[dbo].[AspNetUsers]")]
    public partial class AspNetUser
    {
        [Key]
        public string Id { get; set; } // Id (Primary key)
        public string Email { get; set; } // Email
        public bool EmailConfirmed { get; set; } // EmailConfirmed
        public string PasswordHash { get; set; } // PasswordHash
        public string SecurityStamp { get; set; } // SecurityStamp
        public string PhoneNumber { get; set; } // PhoneNumber
        public bool PhoneNumberConfirmed { get; set; } // PhoneNumberConfirmed
        public bool TwoFactorEnabled { get; set; } // TwoFactorEnabled
        public DateTime? LockoutEndDateUtc { get; set; } // LockoutEndDateUtc
        public bool LockoutEnabled { get; set; } // LockoutEnabled
        public int AccessFailedCount { get; set; } // AccessFailedCount
        public string UserName { get; set; } // UserName
        public int UserId { get; set; } // UserId

        // Reverse navigation
        public virtual ICollection<AspNetRole> AspNetRoles { get; set; } // Many to many mapping
        

        public AspNetUser()
        {
            
            AspNetRoles = new List<AspNetRole>();
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
