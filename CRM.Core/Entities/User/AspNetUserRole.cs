﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.User
{
    [Table("[dbo].[AspNetUserRoles]")]
    public partial class AspNetUserRole
    {
        [Key]
        public string UserId { get; set; } // UserId (Primary key)
        public string RoleId { get; set; } // RoleId (Primary key)


        public AspNetUserRole()
        {
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
