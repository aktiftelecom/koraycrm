﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Core.Entities.User
{
     [Table("[dbo].[UserInfo]")]
    public partial class UserInfo
    {
        [Key]
        public int Id { get; set; } // Id (Primary key)
        public string UserId { get; set; } // UserId
        public string FirstName { get; set; } // FirstName
        public string LastName { get; set; } // LastName
    }
}
