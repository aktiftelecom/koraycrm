﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Core.Entities.Vendor
{
    [Table("[Crm].[Vendor]")]
    public partial class Crm_Vendor
    {
        [Key]
        public int VendorId { get; set; } // VendorId (Primary key)
        [Required(ErrorMessage = "Lütfen geçerli bir isim giriniz")]
        public string Name { get; set; } // Name
        [RegularExpression("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$", ErrorMessage = "Phone is invalid")]
        public string Phone { get; set; } // Phone
        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid")]
        public string Email { get; set; } // Email
        public string WebSite { get; set; } // WebSite
        public int GLAccount { get; set; } // GLAccount
        public string Category { get; set; } // Category
        public int AssignedTo { get; set; } // AssignedTo
        public int CityId { get; set; }
        public int State { get; set; }
        public string POBox { get; set; }
        public string Street { get; set; }
        public int? PostalCode { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public DateTime UpdatedTime { get; set; } // UpdatedTime
        public int CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedTime { get; set; } // CreatedTime
        public int UpdatedBy { get; set; } // UpdatedBy
        public string Country { get; set; }

        [NotMapped]
        public string CurrentRole { get; set; }
        [NotMapped]
        public string CurrentOperation { get; set; }
        [NotMapped]
        public string AssingMemberName { get; set; }

         public Crm_Vendor()
        {
            AssignedTo = 0;
            CityId = 34;
             GLAccount = 1;
             State = 1103;
            UpdatedTime = System.DateTime.Now;
            CreatedBy = 0;
            CreatedTime = System.DateTime.Now;
            UpdatedBy = 0;
            InitializePartial();
        }
        partial void InitializePartial();
    }
}
