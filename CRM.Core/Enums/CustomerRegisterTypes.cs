﻿namespace CRM.Core.Enums
{
    public enum CustomerRegisterTypes
    {

       WebRegister = 1,

       WebTicket = 2,

       AppSingleInsert = 3,

       AppViaImport = 4

    }
}
