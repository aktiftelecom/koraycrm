﻿namespace CRM.Core.Enums
{
    public enum GLAccountTypes
    {

        SalesSoftware = 1,

        SalesHardware = 2,

        RentalIncome = 3,

        InterestIncome = 4,

        SalesSoftwareSupport = 5,

        SalesOther = 6,

        InternetSales = 7,

        ServiceHardwareLabor = 8,

        SalesBooks = 7,

    }
}
