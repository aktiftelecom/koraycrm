﻿namespace CRM.Core.Enums
{
    /// <summary>
    /// ExistingBussines
    /// </summary>
    public enum OpportunityType
    {

        /// <summary>
        /// The existing bussines
        /// </summary>
       ExistingBussines = 1,

       /// <summary>
       /// The new bussines
       /// </summary>
        NewBussines = 2

    }
}
