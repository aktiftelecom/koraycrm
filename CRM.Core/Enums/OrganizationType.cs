﻿namespace CRM.Core.Enums
{
    public enum OrganizationType
    {

        Analyst =1,

        Competitor = 2,

        Customer = 3,

        Integrator = 4,

        Investor = 5,
        
        Partner = 6,

        Press = 7,

        Prospect = 8,

        Reseller = 9,

        Other = 10

    }
}
