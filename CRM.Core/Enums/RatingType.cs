﻿namespace CRM.Core.Enums
{
    public enum RatingType
    {

       Acquired = 1,

       Active = 2,

       MarketFailed = 3,

       ProjectCancelled = 4,

       Shutdown = 5


    }
}
