﻿namespace CRM.Core.Enums
{
    public enum ShippingType
    {

        Yurtici = 1,

        Aras = 2,

        MNG = 3,

        FedEx = 4,

        UPS = 5,

        USPS = 6,

        DHL = 7,

        BlueDart = 8

    }
}
