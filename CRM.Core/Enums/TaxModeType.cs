﻿namespace CRM.Core.Enums
{
    public enum PaymentDurationType
    {

        Net30Days = 1,

        Net45Days = 2,

        Net60Days = 3,

    }
}
