﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using CRM.Core.Entities.Logs;
using Newtonsoft.Json;

namespace CRM.Core.Utility
{

    public static class WebApiUtility
    {

        public static string WebApiGet(string url, bool isNeedAuthorization)
        {
            using (var client = new WebClient())
            {
                if (isNeedAuthorization)
                {
                    return AddAuthorization(client) ? client.DownloadString(url) : null;                    
                }
                return client.DownloadString(url);
            }
        }

        public static string WebApiPost(string url, NameValueCollection values)
        {
            using (var client = new WebClient())
            {
                if (!AddAuthorization(client)) return null;
                var response = client.UploadValues(url, values);
                return Encoding.Default.GetString(response);
            }
        }

        private static bool AddAuthorization(WebClient client)
        {
            if (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)) return false;
            client.Headers["Authorization"] = "bearer " + HttpContext.Current.User.Identity.Name;
            return true;
        }

        public static string CallWebApiForPost<T>(T value, string url)
        {
            // Create a request using a URL that can receive a post. 
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "text/json";
            request.Method = "POST";
            if (HttpContext.Current.User.Identity.Name != null)
            {
                request.Headers["Authorization"] = "bearer " + HttpContext.Current.User.Identity.Name;
            }
           
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    var json = GetJsonFromObject(value);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                string result;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
        }

        public static string GetJsonFromObject<T>(T value)
        {
            return new JavaScriptSerializer().Serialize(value);
        }

        public static IQueryable<T> GetQueryableObjectFromJson<T>(string value)
        {
            byte[] data = Encoding.Default.GetBytes(value);
            string output = Encoding.UTF8.GetString(data);
            return JsonConvert.DeserializeObject<T[]>(output).AsQueryable();
        }

        public static T GetObjectFromJson<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }

        public static String StringEncodingConvert(String strText, String strSrcEncoding, String strDestEncoding)
        {
            System.Text.Encoding srcEnc = System.Text.Encoding.GetEncoding(strSrcEncoding);
            System.Text.Encoding destEnc = System.Text.Encoding.GetEncoding(strDestEncoding);
            byte[] bData = srcEnc.GetBytes(strText);
            byte[] bResult = System.Text.Encoding.Convert(srcEnc, destEnc, bData);
            return destEnc.GetString(bResult);
        }
    }
}