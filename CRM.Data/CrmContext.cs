﻿using System.Data.Entity;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Logs;
using CRM.Core.Entities.Manufacturer;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Entities.Product;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Core.Entities.SaleOrder;
using CRM.Core.Entities.Security;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;
using CRM.Core.Entities.User;
using CRM.Core.Entities.Vendor;
using CRM.Core.Entities.Opportunity;
using CRM.Core.Entities.Sale;

namespace CRM.Data
{
    public class CrmContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public CrmContext() : base("DefaultConnection")
        {
        }

        public System.Data.Entity.DbSet<Crm_Campaign> Crm_Campaign { get; set; }
        public System.Data.Entity.DbSet<Members_Member> Members_Member { get; set; }
        public System.Data.Entity.DbSet<Members_Role> Members_Role { get; set; }
        public System.Data.Entity.DbSet<Members_MemberRole> Members_MemberRole { get; set; }
        public System.Data.Entity.DbSet<Crm_Product> Crm_Product { get; set; }
        public System.Data.Entity.DbSet<Crm_CampaignStatu> Crm_CampaignStatu { get; set; }
        public System.Data.Entity.DbSet<ErrorLog> ErrorLog { get; set; }
        public System.Data.Entity.DbSet<AspNetRole> AspNetRole { get; set; }
        public System.Data.Entity.DbSet<AspNetUser> AspNetUser { get; set; }
        public System.Data.Entity.DbSet<AspNetUserRole> AspNetUserRole { get; set; }
        public System.Data.Entity.DbSet<Members_Customer> Members_Customer { get; set; }
        public System.Data.Entity.DbSet<This_City> This_City { get; set; }
        public System.Data.Entity.DbSet<Customer> Customers { get; set; }
        public System.Data.Entity.DbSet<Crm_Manufacturer> Crm_Manufacturer { get; set; }
        public System.Data.Entity.DbSet<Crm_Vendor> Crm_Vendor { get; set; }
        public System.Data.Entity.DbSet<This_Unit> This_Unit { get; set; }
        public System.Data.Entity.DbSet<This_Currency> This_Currency { get; set; }
        public System.Data.Entity.DbSet<Crm_Lead> Crm_Lead { get; set; }
        public System.Data.Entity.DbSet<Crm_LeadSource> Crm_LeadSource { get; set; }
        public System.Data.Entity.DbSet<Crm_LeadStatu> Crm_LeadStatu { get; set; }
        public System.Data.Entity.DbSet<This_Industry> This_Industry { get; set; }
        public System.Data.Entity.DbSet<This_District> This_District { get; set; }
        public System.Data.Entity.DbSet<UserInfo> UserInfo { get; set; }
        public System.Data.Entity.DbSet<Projects_Quote> Projects_Quote { get; set; }
        public System.Data.Entity.DbSet<Projects_Project> Projects_Project { get; set; }
        public System.Data.Entity.DbSet<Members_Contact> Members_Contact { get; set; }
        public System.Data.Entity.DbSet<Projects_QuoteStage> Projects_QuoteStage { get; set; }
        public System.Data.Entity.DbSet<Crm_Opportunity> Crm_Opportunity { get; set; }
        public System.Data.Entity.DbSet<Crm_SaleStage> Crm_SaleStage { get; set; }
        public System.Data.Entity.DbSet<Crm_Organization> Crm_Organization { get; set; }
        public System.Data.Entity.DbSet<Crm_SaleOrder> Crm_SaleOrder { get; set; }
        public System.Data.Entity.DbSet<Crm_Service> Crm_Service { get; set; }
        public System.Data.Entity.DbSet<Crm_ServiceCategory> Crm_ServiceCategory { get; set; }
        public System.Data.Entity.DbSet<Crm_PriceBook> Crm_PriceBook { get; set; }
    
    }
}
