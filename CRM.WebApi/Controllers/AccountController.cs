﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Security;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;
using CRM.WebApi.Repository.AuthRepository;
using Microsoft.AspNet.Identity;

namespace CRM.WebApi.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        /// <summary>
        /// The _repo
        /// </summary>
        private AuthRepository _repo = null;
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public AccountController(ICRMRepository repository)
        {
            _repo = new AuthRepository();
            this._repository = repository;
        }

        /// <summary>
        /// Registers the specified user model.
        /// </summary>
        /// <param name="userModel">The user model.</param>
        /// <returns></returns>
        [AllowAnonymous] 
        [Route("Register")]
        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _repo.RegisterUser(userModel);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        /// <summary>
        /// Adds the role.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("AddRole")]
        public async Task<IHttpActionResult> AddRole(string roleName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _repo.AddRoles(roleName);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        /// <summary>
        /// Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
         {
            if (disposing)
            {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Gets the error result.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        /// <summary>
        /// Gets the CRM users.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("GetCrmUsers")]
        [HttpGet]
        public IQueryable<CrmUserDto> GetCrmUsers()
        {
            return _repository.GetAllUsers();
        }

        /// <summary>
        /// Updates the CRM users.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("UpdateCrmUsers")]
        [HttpPost]
        public bool UpdateCrmUsers(CrmUserDto user)
        {
            _repository.UpdateUser(user);
            return true;
        }

        /// <summary>
        /// Deletes the CRM users.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("DeleteCrmUser")]
        [HttpPost]
        public bool DeleteCrmUser([FromBody]string userId)
        {
            _repository.DeleteUser(userId);
            return true;
        }

        /// <summary>
        /// Gets the CRM users.
        /// </summary>
        /// <returns></returns>
        [Route("GetRoles")]
        [HttpGet]
        public IQueryable<CrmRoleDto> GetRoles()
        {
            return _repository.GetAllRoles();
        }
    }
}
