﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Core.Entities.Campaign;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class CampaignController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CampaignController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public CampaignController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Campaign/GetCrmCampaign")]
        [HttpGet]
        public IQueryable<CrmCampaignDto> GetCrmCampaign()
        {
            var role = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
            return _repository.GetCrmCampaignDto();
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Campaign/InsertCrmCampaign")]
        [HttpPost]
        public bool InsertCrmCampaign([FromBody]CrmCampaignDto campaign)
        {
            _repository.SaveCampaign(campaign);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Campaign/DeleteCrmCampaign")]
        [HttpPost]
        public bool DeleteCrmCampaign([FromBody]int id)
        {
            _repository.DeleteCampaign(id);
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        [Route("api/Campaign/UpdateCrmCampaign")]
        [HttpPost]
        public bool UpdateCrmCampaign([FromBody]CrmCampaignDto campaign)
        {
            _repository.UpdateCampaign(campaign);
            return true;
        }
    }
}
