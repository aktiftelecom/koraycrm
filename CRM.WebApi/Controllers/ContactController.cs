﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ContactController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public ContactController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Contact/InsertContact")]
        [HttpPost]
        public bool InsertContact([FromBody]Members_Contact contact)
        {
            _repository.SaveContact(contact);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Contact/DeleteContact")]
        [HttpPost]
        public bool DeleteContact([FromBody]int id)
        {
            _repository.DeleteContact(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        [Route("api/Contact/UpdateContact")]
        [HttpPost]
        public bool UpdateContact([FromBody]Members_Contact contact)
        {
            _repository.UpdateContact(contact);
            return true;
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Contact/GetContact")]
        [HttpGet]
        public IQueryable<Members_Contact> GetContact()
        {
            return _repository.GetContact();
        }
    }
}
