﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.City;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class LeadController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public LeadController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Lead/GetCrmLead")]
        [HttpGet]
        public IQueryable<Crm_Lead> GetCrmLead()
        {
            return _repository.GetAllCrmLeads();
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="lead">The lead.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Lead/InsertCrmLead")]
        [HttpPost]
        public bool InsertCrmLead([FromBody]Crm_Lead lead)
        {
            _repository.SaveCrmLead(lead);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Lead/DeleteCrmLead")]
        [HttpPost]
        public bool DeleteCrmLead([FromBody]int id)
        {
            _repository.DeleteCrmLead(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="lead">The lead.</param>
        /// <returns></returns>
        [Route("api/Lead/UpdateCrmLead")]
        [HttpPost]
        public bool UpdateCrmLead([FromBody]Crm_Lead lead)
        {
            _repository.UpdateCrmLeads(lead);
            return true;
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Lead/GetDistrict")]
        [HttpGet]
        public IQueryable<This_District> GetDistrict()
        {
            return _repository.GetAllDistricts();
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Lead/GetCity")]
        [HttpGet]
        public IQueryable<This_City> GetCity()
        {
            return _repository.GetAllCities();
        }

        /// <summary>
        /// Gets the CRM lead source.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Lead/GetCrmLeadSource")]
        [HttpGet]
        public IQueryable<Crm_LeadSource> GetCrmLeadSource()
        {
            return _repository.GetAllLeadSource();
        }
    }
}
