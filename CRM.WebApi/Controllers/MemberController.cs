﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Core.Entities.Member;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class MemberController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CampaignController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public MemberController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Member/GetCrmCustomer")]
        [HttpGet]
        public IQueryable<Members_Customer> GetCrmCustomer()
        {
            var role = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
            return _repository.GetAllCustomers();
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Member/InsertCrmCustomer")]
        [HttpPost]
        public bool InsertCrmCustomer([FromBody]Members_Customer customer)
        {
            _repository.SaveCrmCustomer(customer);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Member/DeleteCrmCustomer")]
        [HttpPost]
        public bool DeleteCrmCustomer([FromBody]int id)
        {
            _repository.DeleteCrmCustomer(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        [Route("api/Member/UpdateCrmCustomer")]
        [HttpPost]
        public bool UpdateCrmCustomer([FromBody]Members_Customer customer)
        {
            _repository.UpdateCrmCustomer(customer);
            return true;
        }
    }
}
