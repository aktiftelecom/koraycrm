﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;
using CRM.Core.Entities.Opportunity;
using CRM.Core.Entities.Sale;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class OpportunityController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public OpportunityController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Opportunity/GetOpportunity")]
        [HttpGet]
        public IQueryable<Crm_Opportunity> GetOpportunity()
        {
            return _repository.GetOpportunities();
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="quote">The quote.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Opportunity/InsertOpportunity")]
        [HttpPost]
        public bool InsertOpportunity([FromBody]Crm_Opportunity opportunity)
        {
            _repository.SaveOpportunity(opportunity);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Opportunity/DeleteOpportunity")]
        [HttpPost]
        public bool DeleteOpportunity([FromBody]int id)
        {
            _repository.DeleteOpportunity(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="quote">The quote.</param>
        /// <returns></returns>
        [Route("api/Opportunity/UpdateOpportunity")]
        [HttpPost]
        public bool UpdateOpportunity([FromBody]Crm_Opportunity opportunity)
        {
            _repository.UpdateOpportunity(opportunity);
            return true;
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Opportunity/GetSaleStage")]
        [HttpGet]
        public IQueryable<Crm_SaleStage> GetSaleStage()
        {
            return _repository.GetSaleStage();
        }
    }
}
