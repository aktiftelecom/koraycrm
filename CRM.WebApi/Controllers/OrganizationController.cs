﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class OrganizationController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public OrganizationController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="organization">The organization.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Organization/InsertOrganization")]
        [HttpPost]
        public bool InsertOrganization([FromBody]Crm_Organization organization)
        {
            _repository.SaveOrganization(organization);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Organization/DeleteOrganization")]
        [HttpPost]
        public bool DeleteOrganization([FromBody]int id)
        {
            _repository.DeleteOrganization(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="organization">The organization.</param>
        /// <returns></returns>
        [Route("api/Organization/UpdateOrganization")]
        [HttpPost]
        public bool UpdateOrganization([FromBody]Crm_Organization organization)
        {
            _repository.UpdateOrganization(organization);
            return true;
        }

        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Organization/GetOrganization")]
        [HttpGet]
        public IQueryable<Crm_Organization> GetOrganization()
        {
            return _repository.GetOrganization();
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Organization/GetIndustry")]
        [HttpGet]
        public IQueryable<This_Industry> GetIndustry()
        {
            return _repository.GetIndustry();
        }
    }
}
