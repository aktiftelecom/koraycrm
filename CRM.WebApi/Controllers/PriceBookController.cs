﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.PriceBook;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class PriceBookController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public PriceBookController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="priceBook">The price book.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/PriceBook/SavePriceBook")]
        [HttpPost]
        public bool SavePriceBook([FromBody]Crm_PriceBook priceBook)
        {
            _repository.SavePriceBook(priceBook);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/PriceBook/DeletePriceBook")]
        [HttpPost]
        public bool DeletePriceBook([FromBody]int id)
        {
            _repository.DeletePriceBook(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        [Route("api/PriceBook/UpdatePriceBook")]
        [HttpPost]
        public bool UpdatePriceBook([FromBody]Crm_PriceBook priceBook)
        {
            _repository.UpdatePriceBook(priceBook);
            return true;
        }

        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/PriceBook/GetPriceBook")]
        [HttpGet]
        public IQueryable<Crm_PriceBook> GetPriceBook()
        {
            return _repository.GetPriceBook();
        }
    }
}
