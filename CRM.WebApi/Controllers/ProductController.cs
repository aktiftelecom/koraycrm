﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Product;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ProductController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CampaignController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public ProductController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Product/GetCrmProduct")]
        [HttpGet]
        public IQueryable<Crm_Product> GetCrmProduct()
        {
            return _repository.GetAllCrmProducts();
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Product/InsertCrmProduct")]
        [HttpPost]
        public bool InsertCrmProduct([FromBody]Crm_Product product)
        {
            _repository.SaveCrmProduct(product);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Product/DeleteCrmProduct")]
        [HttpPost]
        public bool DeleteCrmProduct([FromBody]int id)
        {
            _repository.DeleteCrmProduct(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns></returns>
        [Route("api/Product/UpdateCrmProduct")]
        [HttpPost]
        public bool UpdateCrmProduct([FromBody]Crm_Product product)
        {
            _repository.UpdateCrmProducts(product);
            return true;
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Product/GetAllCurrencies")]
        [HttpGet]
        public IQueryable<This_Currency> GetAllCurrencies()
        {
            return _repository.GetAllCurrencies();
        }
    }
}
