﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class QuoteController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public QuoteController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Quote/GetQuote")]
        [HttpGet]
        public IQueryable<Projects_Quote> GetQuote()
        {
            return _repository.GetQuotes();
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="quote">The quote.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Quote/InsertQuote")]
        [HttpPost]
        public bool InsertQuote([FromBody]Projects_Quote quote)
        {
            _repository.SaveQuote(quote);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Quote/DeleteQuote")]
        [HttpPost]
        public bool DeleteQuote([FromBody]int id)
        {
            _repository.DeleteQuote(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="quote">The quote.</param>
        /// <returns></returns>
        [Route("api/Quote/UpdateQuote")]
        [HttpPost]
        public bool UpdateQuote([FromBody]Projects_Quote quote)
        {
            _repository.UpdateQuote(quote);
            return true;
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Quote/GetContact")]
        [HttpGet]
        public IQueryable<Members_Contact> GetContact()
        {
            return _repository.GetContact();
        }

        /// <summary>
        /// Gets the project.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Quote/GetProject")]
        [HttpGet]
        public IQueryable<Projects_Project> GetProject()
        {
            return _repository.GetProject();
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Quote/GetQuoteStage")]
        [HttpGet]
        public IQueryable<Projects_QuoteStage> GetQuoteStage()
        {
            return _repository.GetQuoteStage();
        }
    }
}
