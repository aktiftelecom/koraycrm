﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.SaleOrder;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class SaleOrderController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public SaleOrderController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/SaleOrder/InsertSaleOrder")]
        [HttpPost]
        public bool InsertSaleOrder([FromBody]Crm_SaleOrder order)
        {
            _repository.SaveSaleOrder(order);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/SaleOrder/DeleteSaleOrder")]
        [HttpPost]
        public bool DeleteSaleOrder([FromBody]int id)
        {
            _repository.DeleteSaleOrder(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        [Route("api/SaleOrder/UpdateSaleOrder")]
        [HttpPost]
        public bool UpdateSaleOrder([FromBody]Crm_SaleOrder order)
        {
            _repository.UpdateSaleOrder(order);
            return true;
        }

        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/SaleOrder/GetSaleOrder")]
        [HttpGet]
        public IQueryable<Crm_SaleOrder> GetSaleOrder()
        {
            return _repository.GetSaleOrder();
        }
    }
}
