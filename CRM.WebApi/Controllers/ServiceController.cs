﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.SaleOrder;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ServiceController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public ServiceController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Service/InsertService")]
        [HttpPost]
        public bool InsertService([FromBody]Crm_Service service)
        {
            _repository.SaveService(service);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Service/DeleteService")]
        [HttpPost]
        public bool DeleteService([FromBody]int id)
        {
            _repository.DeleteService(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        [Route("api/Service/UpdateService")]
        [HttpPost]
        public bool UpdateService([FromBody]Crm_Service service)
        {
            _repository.UpdateService(service);
            return true;
        }

        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Service/GetService")]
        [HttpGet]
        public IQueryable<Crm_Service> GetService()
        {
            return _repository.GetService();
        }

        /// <summary>
        /// Gets the service category.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Service/GetServiceCategory")]
        [HttpGet]
        public IQueryable<Crm_ServiceCategory> GetServiceCategory()
        {
            return _repository.GetServiceCategory();
        }

        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Service/GetUnit")]
        [HttpGet]
        public IQueryable<This_Unit> GetUnit()
        {
            return _repository.GetAllUnits();
        }
    }
}
