﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.SaleOrder;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;
using CRM.Core.Entities.Vendor;
using CRM.WebApi.CustomAttributes;
using CRM.WebApi.Repository;

namespace CRM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class VendorController : ApiController
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly ICRMRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public VendorController(ICRMRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Inserts the CRM campaign.
        /// </summary>
        /// <param name="vendor">The vendor.</param>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "Admin")]
        [Route("api/Vendor/InsertVendors")]
        [HttpPost]
        public bool InsertVendors([FromBody]Crm_Vendor vendor)
        {
            _repository.SaveVendor(vendor);
            return true;
        }

        /// <summary>
        /// Deletes the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("api/Vendor/DeleteVendor")]
        [HttpPost]
        public bool DeleteVendor([FromBody]int id)
        {
            _repository.DeleteVendor(id.ToString(CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Updates the CRM campaign.
        /// </summary>
        /// <param name="vendor">The order.</param>
        /// <returns></returns>
        [Route("api/Vendor/UpdateVendor")]
        [HttpPost]
        public bool UpdateVendor([FromBody]Crm_Vendor vendor)
        {
            _repository.UpdateVendor(vendor);
            return true;
        }

        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClaimsAuthorize(ClaimTypes.Role, "User,Admin")]
        [Route("api/Vendor/GetVendors")]
        [HttpGet]
        public IQueryable<Crm_Vendor> GetVendors()
        {
            return _repository.GetVendors();
        }

    }
}
