﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CRM.WebApi.CustomAttributes
{
    public class ClaimsAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string _claimType;
        private readonly string[] _claimValue;

        public ClaimsAuthorizeAttribute(string type, params string[] roles)
        {
            this._claimType = type;
            this._claimValue = roles;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
            var claim = identity.Claims.FirstOrDefault(c => c.Type == _claimType);

            if (claim != null && _claimValue[0].Contains(claim.Value))
            {
                base.OnAuthorization(actionContext);
            }
            else
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
        }

    } 
}
