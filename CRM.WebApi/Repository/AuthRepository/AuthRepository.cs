﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Security;
using CRM.Core.Entities.User;
using CRM.Data;
using CRM.WebApi.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CRM.WebApi.Repository.AuthRepository
{
    public class AuthRepository : IDisposable
    {
        private AuthContext _ctx;

        private UserManager<IdentityUser> _userManager;

        private RoleManager<IdentityRole> _roleManager;

        public AuthRepository()
        {
            _ctx = new AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.UserName,
                Email = userModel.EMail,
                PhoneNumber = userModel.PhoneNumber
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            if (result.Succeeded)
            {
                var savedUser = await this.FindUser(user.UserName, userModel.Password);
                var roles = this.GetRoles();
                var firstOrDefault = roles.FirstOrDefault();
                if (firstOrDefault != null)
                {
                    this.SaveUserInfo(savedUser.Id, userModel);
                    var roleResult = await this.AssignRole(firstOrDefault.Name, savedUser.Id);
                    return roleResult;
                }
                else
                {
                    return result;
                }
            }
            else
            {
                return result;
            }
           
        }

        public async Task<IdentityResult> AddRoles(string roleName)
        {
            IdentityRole role = new IdentityRole
            {
               Name = roleName,
            };

            var result = await _roleManager.CreateAsync(role);

            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            var user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public async Task<IList<string>> GetUserRole(string userId)
        {
            var roles = await _userManager.GetRolesAsync(userId);

            return roles;
        }

        public  IQueryable<IdentityRole> GetRoles()
        {
            var roles =  _roleManager.Roles;

            return roles;
        }

        public async Task<IdentityResult> AssignRole(string roleName, string userId)
        {
            var result = await _userManager.AddToRoleAsync(userId, roleName);

            return result;
        }

        /// <summary>
        /// Saves the user information.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="user">The user.</param>
        private void SaveUserInfo(string userId, UserModel user)
        {
            var context = new CrmContext();
            context.UserInfo.Add(new UserInfo
            {
                UserId = userId,
                FirstName = user.FirstName,
                LastName = user.LastName
            });

            context.SaveChanges();
        }

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }
}
