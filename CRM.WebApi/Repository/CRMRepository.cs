﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Business.Dtoes.ProductDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Logs;
using CRM.Core.Entities.Manufacturer;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Entities.Product;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Core.Entities.SaleOrder;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;
using CRM.Core.Entities.User;
using CRM.Core.Entities.Vendor;
using CRM.Core.Enums;
using CRM.Data;
using CRM.Core.Entities.Opportunity;
using CRM.Core.Entities.Sale;

namespace CRM.WebApi.Repository
{
    public class CRMRepository : ICRMRepository
    {
        /// <summary>
        /// The _campaign context
        /// </summary>
        private CrmContext _crmContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="CRMRepository"/> class.
        /// </summary>
        public CRMRepository(CrmContext context)
        {
            _crmContext = context;
        }


        /// <summary>
        /// Gets the member role b member identifier.
        /// </summary>
        /// <param name="memberId">The member identifier.</param>
        /// <returns>Members_Role</returns>
        public Members_Role GetMemberRoleBMemberId(int memberId)
        {
            var memberRole = _crmContext.Members_MemberRole.FirstOrDefault(x => x.MemberId == memberId);
            return _crmContext.Members_Role.FirstOrDefault(x => x.RoleId == memberRole.RoleId);

        }

        /// <summary>
        /// Gers the campaign statuses by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Crm_Campaign GetCampaignById(int id)
        {
            return _crmContext.Crm_Campaign.FirstOrDefault(x => x.CampaignId == id);
        }

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_Campaign> GetCrmCampaigns()
        {
            return _crmContext.Crm_Campaign;
        }

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Crm_Campaign GetCrmCampaign(int id)
        {
            return _crmContext.Crm_Campaign.FirstOrDefault(x => x.CampaignId == id);
        }

        /// <summary>
        /// Gets the member.
        /// </summary>
        /// <param name="memberId">The member identifier.</param>
        /// <returns></returns>
        public Members_Member GetMember(int memberId)
        {
            return _crmContext.Members_Member.FirstOrDefault(x => x.MemberId == memberId);
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        public Crm_Product GetProduct(int productId)
        {
            return _crmContext.Crm_Product.FirstOrDefault(x => x.ProductId == productId);
        }

        /// <summary>
        /// Gets the CRM campaign dto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CrmCampaignDto GetCrmCampaignDto(int? id)
        {
            CrmCampaignDto crmCampaignDto = null;
            if (id != null)
            {
                var campaign = this.GetCrmCampaign(id.Value);

                if (campaign != null)
                {

                    crmCampaignDto = new CrmCampaignDto
                    {
                        TypeId = (CampaignTypes)campaign.TypeId,
                        StatusId = (StatusTypes)campaign.StatusId,
                        AssignMember = new CrmMemberDto
                        {
                            MemberNameId = campaign.AssignTo,
                            MemberName =
                                this.GetMember(campaign.AssignTo) == null
                                    ? string.Empty
                                    : this.GetMember(campaign.AssignTo).FirstName
                        },
                        CreateMember = new CrmMemberDto
                        {
                            MemberNameId = campaign.CreatedBy,
                            MemberName =
                                this.GetMember(campaign.CreatedBy) == null
                                    ? string.Empty
                                    : this.GetMember(campaign.CreatedBy).FirstName
                        },
                        Prodcut = new CrmProductDto
                        {
                            ProductNameId = campaign.ProductId,
                            ProductName =
                                this.GetProduct(campaign.ProductId) == null
                                    ? string.Empty
                                    : GetProduct(campaign.ProductId).Name
                        },
                        UpdateMember = new CrmMemberDto
                        {
                            MemberNameId = campaign.UpdatedBy,
                            MemberName =
                                this.GetMember(campaign.UpdatedBy) == null
                                    ? string.Empty
                                    : this.GetMember(campaign.UpdatedBy).FirstName
                        }
                    };


                }

            }
            return crmCampaignDto;
        }

        /// <summary>
        /// Gets the CRM campaign dto.
        /// </summary>
        /// <returns></returns>
        public IQueryable<CrmCampaignDto> GetCrmCampaignDto()
        {
            var campaigns = this.GetCrmCampaigns().Where(c => c.IsDeleted == false);
            
            var crmCampaigns = new List<CrmCampaignDto>();
            var status = this.GetAllCampaignStatus().ToList();
            var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
            var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            if (campaigns != null)
            {
                foreach (var campaign in campaigns)
                {
                    var user = this.GetAllUsers().FirstOrDefault(u => u.UserId == campaign.AssignTo);
                    if (claim != null)
                        crmCampaigns.Add(new CrmCampaignDto
                        {
                            CampaignId = campaign.CampaignId,
                            CampaignStatusId = campaign.StatusId,
                            ExpectedResponseTypeId = campaign.ExpectedResponse,
                            TypeNameId = campaign.TypeId,
                            MemberNameId = campaign.AssignTo,
                            ProductNameId = campaign.ProductId,
                            CampaignName = campaign.CampaignName,
                            AssignTo = campaign.AssignTo,
                            ExpectedCloseDate = campaign.ExpectedCloseDate,
                            TargetAudience = campaign.TargetAudience,
                            Sponsor = campaign.Sponsor,
                            TargetSize = 1,
                            NumberSent = campaign.NumberSent,
                            BudgetCost = campaign.BudgetCost,
                            ExpectedRevenue = campaign.ExpectedRevenue,
                            ExpectedSalesCount = campaign.ExpectedSalesCount,
                            ExpectedResponseCount = campaign.ExpectedResponseCount,
                            ExpectedROI = campaign.ExpectedROI,
                            ActualCost = campaign.ActualCost,
                            ExpectedResponse = campaign.ExpectedResponse,
                            ActualSalesCount = campaign.ActualSalesCount,
                            ActualResponseCount = campaign.ActualResponseCount,
                            ActualROI = campaign.ActualROI,
                            Description = campaign.Description,
                            IsDelete = campaign.IsDeleted,
                            TypeId = (CampaignTypes)campaign.TypeId,
                            StatusId = (StatusTypes)campaign.StatusId,
                            ProductId = campaign.ProductId,
                            AllCampaignStatus = status,
                            CreatedTime = DateTime.Now.Date,
                            CurrentRole = claim.Value,
                            AssignPerson = user != null ? user.FirstName : string.Empty,
                            Status = "Active",
                        
                                AssignMember = new CrmMemberDto
                            {
                                MemberNameId = campaign.AssignTo,
                                MemberName = "Test"//this.GetMember(campaign.AssignTo) == null ? string.Empty : this.GetMember(campaign.AssignTo).FirstName
                            },
                            CreateMember = new CrmMemberDto
                            {
                                MemberNameId = campaign.CreatedBy,
                                MemberName = "Test"//this.GetMember(campaign.CreatedBy) == null ? string.Empty : this.GetMember(campaign.CreatedBy).FirstName
                            },
                            Prodcut = new CrmProductDto
                            {
                                ProductNameId = 5,
                                ProductName = "Test"//this.GetProduct(campaign.ProductId) == null ? string.Empty : GetProduct(campaign.ProductId).Name
                            },
                            UpdateMember = new CrmMemberDto
                            {
                                MemberNameId = campaign.UpdatedBy,
                                MemberName = "Test"//this.GetMember(campaign.UpdatedBy) == null ? string.Empty : this.GetMember(campaign.UpdatedBy).FirstName
                            }
                });
                }
            }
            return crmCampaigns.AsQueryable();
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns></returns>
        public CrmCampaignDto GetAllProducts()
        {
            var crmCampaignDto = new CrmCampaignDto();
            foreach (var product in _crmContext.Crm_Product)
            {
                crmCampaignDto.AllProducts.Add(new CrmProductDto
                {
                    ProductNameId = product.ProductId,
                    ProductName = product.Name
                });
            }
            return crmCampaignDto;
        }

        /// <summary>
        /// Gets all campaign status.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_CampaignStatu> GetAllCampaignStatus()
        {
            return _crmContext.Crm_CampaignStatu;
        }

        /// <summary>
        /// Gets all members.
        /// </summary>
        /// <returns></returns>
        public CrmCampaignDto GetAllMembers()
        {
            var crmCampaignDto = new CrmCampaignDto();
            foreach (var member in _crmContext.Members_Member)
            {
                crmCampaignDto.AllMembers.Add(new CrmMemberDto
                {
                   MemberNameId = member.MemberId,
                   MemberName = member.LoginName
                   
                });
            }
            return crmCampaignDto;
        }

        /// <summary>
        /// Saves the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        public void SaveCampaign(CrmCampaignDto campaign)
        {
            try
            {
                _crmContext.Crm_Campaign.AddOrUpdate(CrmCampaignDto.ToCrmCampaign(campaign));
                _crmContext.SaveChanges();
            }
            catch (Exception ex)
            {
                    
                this.SaveErrorLog(new ErrorLog
                {
                    CreateDate = DateTime.Now,
                    ExceptionMessage = ex.Message,
                    ExceptionType = ex.GetType().ToString(),
                    ReferenceNumber = "Test",
                    TargetSiteModule = ex.StackTrace.Trim()
                });
            }
           

        }

        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        public void UpdateCampaign(CrmCampaignDto campaign)
        {
            try
            {
                var campaigns = new Crm_Campaign[1];
                campaigns[0] = CrmCampaignDto.ToCrmCampaign(campaign);
                _crmContext.Crm_Campaign.AddOrUpdate(campaigns);
                _crmContext.SaveChanges();
            }
            catch (Exception ex)
            {

                this.SaveErrorLog(new ErrorLog
                {
                    CreateDate = DateTime.Now,
                    ExceptionMessage = ex.Message,
                    ExceptionType = ex.GetType().ToString(),
                    ReferenceNumber = "Test",
                    TargetSiteModule = ex.StackTrace.Trim()
                });
            }
           

        }

        /// <summary>
        /// Delete the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <param name="id">Campaign Id</param>
        public void DeleteCampaign(int id)
        {
            var customer = _crmContext.Crm_Campaign.Find(id);
            customer.IsDeleted = true;
            _crmContext.Crm_Campaign.AddOrUpdate(customer);
            _crmContext.SaveChanges();
        }

        /// <summary>
        /// Saves the error log.
        /// </summary>
        /// <param name="error">The error.</param>
        public void SaveErrorLog(ErrorLog error)
        {
            _crmContext.ErrorLog.Add(error);
            _crmContext.SaveChanges();
        }

        /// <summary>
        /// Gets all roles.
        /// </summary>
        /// <returns></returns>
        public IQueryable<CrmRoleDto> GetAllRoles()
        {
            var qry = from b in _crmContext.AspNetRole
                      select new CrmRoleDto
                      {
                          RoleId = b.Id,
                          RoleName = b.Name
                      };

            return qry.AsQueryable();
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        public IQueryable<CrmUserDto> GetAllUsers()
        {
            var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
            var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var qry = from b in _crmContext.AspNetUser
                      select new CrmUserDto()
                      {
                          Id = b.Id,
                          UserId = b.UserId,
                          Email = b.Email,
                          PhoneNumber = b.PhoneNumber,
                          UserName = b.UserName,
                          CurrentRole = claim.Value,
                          RoleId = _crmContext.AspNetUserRole.FirstOrDefault(x => x.UserId == b.Id).RoleId,
                          RoleName = _crmContext.AspNetRole.FirstOrDefault(r => r.Id == (_crmContext.AspNetUserRole.FirstOrDefault(x => x.UserId == b.Id).RoleId)).Name,
                          FirstName = _crmContext.UserInfo.FirstOrDefault(u => u.UserId == b.Id).FirstName,
                          LastName= _crmContext.UserInfo.FirstOrDefault(u => u.UserId == b.Id).LastName
                      };

            return qry.AsQueryable();
        }

        /// <summary>
         /// Updates the campaign.
         /// </summary>
         /// <param name="user">The user.</param>
        public void UpdateUser(CrmUserDto user)
        {
            try
            {
                
                var users = new AspNetUser[1];
                users[0] = _crmContext.AspNetUser.Find(user.Id);//
                users[0].Email = user.Email;
                users[0].PhoneNumber = user.PhoneNumber;
                users[0].UserName = user.UserName;
                _crmContext.AspNetUser.AddOrUpdate(users);
                _crmContext.SaveChanges();

                var roles = new AspNetUserRole[1];
                roles[0] = new AspNetUserRole
                {
                    RoleId = user.RoleId,
                    UserId = user.Id
                };
                _crmContext.AspNetUserRole.AddOrUpdate(roles);
                _crmContext.SaveChanges();

                _crmContext.UserInfo.AddOrUpdate(new UserInfo
                {
                    UserId = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName
                });

                _crmContext.SaveChanges();
            }
            catch (Exception ex)
            {

                this.SaveErrorLog(new ErrorLog
                {
                    CreateDate = DateTime.Now,
                    ExceptionMessage = ex.Message,
                    ExceptionType = ex.GetType().ToString(),
                    ReferenceNumber = "Test",
                    TargetSiteModule = ex.StackTrace.Trim()
                });
            }
           

        }

       /// <summary>
         /// Saves the campaign.
         /// </summary>
         /// <param name="user">The user.</param>
       public void SaveUser(CrmUserDto user)
        {
            try
            {
                _crmContext.AspNetUser.Add(CrmUserDto.ToAspNetUser(user));
                _crmContext.SaveChanges();
            }
            catch (Exception ex)
            {
                    
                this.SaveErrorLog(new ErrorLog
                {
                    CreateDate = DateTime.Now,
                    ExceptionMessage = ex.Message,
                    ExceptionType = ex.GetType().ToString(),
                    ReferenceNumber = "Test",
                    TargetSiteModule = ex.StackTrace.Trim()
                });
            }
           

        }

       /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
       public void DeleteUser(string userId)
        {
            var user = _crmContext.AspNetUser.Find(userId);
            _crmContext.AspNetUser.Remove(user);
            _crmContext.SaveChanges();
        }

       /// <summary>
       /// Gets all cities.
       /// </summary>
       /// <returns></returns>
       public IQueryable<This_City> GetAllCities()
       {
           var qry = from b in _crmContext.This_City select b;

           return qry.AsQueryable();
       }

       #region Customer Operations
       /// <summary>
       /// Gets the CRM customers.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Members_Customer> GetCrmCustomers()
       {
           return _crmContext.Members_Customer;
       }

       /// <summary>
       /// Gets all customers.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Members_Customer> GetAllCustomers()
       {
         
           var customers = this.GetCrmCustomers().Where(c => c.Status == 1);

           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           customers.FirstOrDefault().CurrentRole = claim.Value;
           customers.FirstOrDefault().Cities = this.GetAllCities().ToList();

           return customers;
       }

       /// <summary>
       /// Updates the CRM customer.
       /// </summary>
       /// <param name="customer">The customer.</param>
       public void UpdateCrmCustomer(Members_Customer customer)
       {
           try
           {

               _crmContext.Members_Customer.AddOrUpdate(customer);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Saves the CRM customer.
       /// </summary>
       /// <param name="customer">The customer.</param>
       public void SaveCrmCustomer(Members_Customer customer)
       {
           try
           {
               _crmContext.Members_Customer.Add(customer);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Deletes the CRM customer.
       /// </summary>
       /// <param name="customerId">The customer identifier.</param>
       public void DeleteCrmCustomer(string customerId)
       {
           var customer = _crmContext.Members_Customer.Find(int.Parse(customerId));
           customer.Status = 0;
           _crmContext.Members_Customer.AddOrUpdate(customer);
           _crmContext.SaveChanges();
       }
       #endregion

       #region Product Operations
       /// <summary>
       /// Gets the products.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Product> GetProducts()
       {
           return _crmContext.Crm_Product;
       }

       /// <summary>
       /// Gets all manufacturers.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Manufacturer> GetAllManufacturers()
       {
           var qry = from b in _crmContext.Crm_Manufacturer select b;

           return qry.AsQueryable();
       }

       /// <summary>
       /// Gets all vendors.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Vendor> GetAllVendors()
       {
           var qry = from b in _crmContext.Crm_Vendor select b;

           return qry.AsQueryable();
       }

       /// <summary>
       /// Gets all units.
       /// </summary>
       /// <returns></returns>
       public IQueryable<This_Unit> GetAllUnits()
       {
           var qry = from b in _crmContext.This_Unit select b;

           return qry.AsQueryable();
       }

       /// <summary>
       /// Gets all currencies.
       /// </summary>
       /// <returns></returns>
       public IQueryable<This_Currency> GetAllCurrencies()
       {
           var qry = from b in _crmContext.This_Currency select b;

           return qry.AsQueryable();
       }

       /// <summary>
       /// Gets all CRM products.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Product> GetAllCrmProducts()
       {

           var products = this.GetProducts().Where(c => c.IsActive == true);

           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           products.FirstOrDefault().CurrentRole = claim.Value;
           products.FirstOrDefault().Manufacturers = this.GetAllManufacturers().ToList();
           products.FirstOrDefault().Vendors = this.GetAllVendors().ToList();
           products.FirstOrDefault().Units = this.GetAllUnits().ToList();
           products.FirstOrDefault().Currencies = this.GetAllCurrencies().ToList();

           return products;
       }

       /// <summary>
       /// Updates the CRM products.
       /// </summary>
       /// <param name="product">The product.</param>
       public void UpdateCrmProducts(Crm_Product product)
       {
           try
           {
               product.CategoryNameId = product.Category.ToString();
               product.ManufacturerNameId = product.ManufacturerId.ToString();
               product.VendorNameId = product.VendorId.ToString();
               product.CurrencyNameId = product.CurrencyId.ToString();
               product.UnitNameId = product.UsageUnitId.ToString();
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_Product.AddOrUpdate(product);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Saves the CRM product.
       /// </summary>
       /// <param name="product">The product.</param>
       public void SaveCrmProduct(Crm_Product product)
       {
           try
           {
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_Product.AddOrUpdate(product);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Deletes the CRM product.
       /// </summary>
       /// <param name="productId">The product identifier.</param>
       public void DeleteCrmProduct(string productId)
       {
           var product = _crmContext.Crm_Product.Find(int.Parse(productId));
           product.CategoryNameId = product.Category.ToString();
           product.ManufacturerNameId = product.ManufacturerId.ToString();
           product.VendorNameId = product.VendorId.ToString();
           product.CurrencyNameId = product.CurrencyId.ToString();
           product.UnitNameId = product.UsageUnitId.ToString();
           product.IsActive = false;
           _crmContext.Configuration.ValidateOnSaveEnabled = false;
           _crmContext.Crm_Product.AddOrUpdate(product);
           _crmContext.SaveChanges();
       }
        #endregion

       #region Lead Operations
       /// <summary>
       /// Gets the products.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Lead> GetLeads()
       {
           return _crmContext.Crm_Lead;
       }

       /// <summary>
       /// Gets all manufacturers.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_LeadSource> GetAllLeadSource()
       {
           var qry = from b in _crmContext.Crm_LeadSource select b;

           return qry.AsQueryable();
       }

       /// <summary>
       /// Gets all vendors.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_LeadStatu> GetAllLeadStatu()
       {
           var qry = from b in _crmContext.Crm_LeadStatu select b;

           return qry.AsQueryable();
       }

       /// <summary>
       /// Gets all units.
       /// </summary>
       /// <returns></returns>
       public IQueryable<This_Industry> GetAllIndustries()
       {
           var qry = from b in _crmContext.This_Industry select b;

           return qry.AsQueryable();
       }

       /// <summary>
       /// Gets all currencies.
       /// </summary>
       /// <returns></returns>
       public IQueryable<This_District> GetAllDistricts()
       {
           var qry = from b in _crmContext.This_District select b;

           return qry.AsQueryable();
       }

       /// <summary>
       /// Gets all CRM products.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Lead> GetAllCrmLeads()
       {

           var leads = this.GetLeads().Where(c => c.IsActive == true);

           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = leads.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
               firstOrDefault.Crm_LeadSource = this.GetAllLeadSource().ToList();
               firstOrDefault.Crm_LeadStatu = this.GetAllLeadStatu().ToList();
               firstOrDefault.This_Industry = this.GetAllIndustries().ToList();
           }

           foreach (var crmLead in leads)
           {
               var crmUserDto = this.GetAllUsers().FirstOrDefault(u => u.UserId == crmLead.AssignedTo);
               if (crmUserDto != null)
                   crmLead.AssignPerson = crmUserDto.FirstName;
           }


           return leads;
       }

       /// <summary>
       /// Updates the CRM products.
       /// </summary>
       /// <param name="lead">The lead.</param>
       public void UpdateCrmLeads(Crm_Lead lead)
       {
           try
           {
               lead.LeadSourceNameId = lead.LeadSourceId.ToString();
               lead.LeadStatuNameId= lead.LeadStatusId.ToString();
               lead.IndustryNameId = lead.IndustryId.ToString();
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_Lead.AddOrUpdate(lead);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Saves the CRM product.
       /// </summary>
       /// <param name="lead">The lead.</param>
       public void SaveCrmLead(Crm_Lead lead)
       {
           try
           {
               _crmContext.Crm_Lead.Add(lead);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Deletes the CRM product.
       /// </summary>
       /// <param name="leadId">The lead identifier.</param>
       public void DeleteCrmLead(string leadId)
       {
           var lead = _crmContext.Crm_Lead.Find(int.Parse(leadId));
           lead.LeadSourceNameId = lead.LeadSourceId.ToString();
           lead.LeadStatuNameId = lead.LeadStatusId.ToString();
           lead.IndustryNameId = lead.IndustryId.ToString();
           lead.IsActive = false;
           _crmContext.Crm_Lead.AddOrUpdate(lead);
           _crmContext.SaveChanges();
       }
        #endregion

        #region Quote Operations
       /// <summary>
       /// Gets the products.
       /// </summary>
       /// <returns>Projects_Quote</returns>
       public IQueryable<Projects_Quote> GetQuotes()
       {
           var quotes = _crmContext.Projects_Quote.Where(q => q.IsDeleted == false);
            var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = quotes.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
           }

           return quotes;
       }

       /// <summary>
       /// Updates the CRM products.
       /// </summary>
       /// <param name="quote">The quote.</param>
       public void UpdateQuote(Projects_Quote quote)
       {
           try
           {
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Projects_Quote.AddOrUpdate(quote);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Saves the CRM product.
       /// </summary>
       /// <param name="quote">The quote.</param>
       public void SaveQuote(Projects_Quote quote)
       {
           try
           {
               _crmContext.Projects_Quote.Add(quote);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Deletes the quote.
       /// </summary>
       /// <param name="quoteId">The quote identifier.</param>
       public void DeleteQuote(string quoteId)
       {
           var quote = _crmContext.Projects_Quote.Find(int.Parse(quoteId));

           quote.IsDeleted = true;
           _crmContext.Projects_Quote.AddOrUpdate(quote);
           _crmContext.SaveChanges();
       }

       /// <summary>
       /// Gets the project.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Projects_Project> GetProject()
       {
           return _crmContext.Projects_Project.Where(q => q.IsDeleted == false);
       }

       /// <summary>
       /// Gets the quote stage.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Projects_QuoteStage> GetQuoteStage()
       {
           return _crmContext.Projects_QuoteStage;
       }
        #endregion

        #region Opportunity Operations

       /// <summary>
       /// Gets the quotes.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Opportunity> GetOpportunities()
       {
           var opportunities = _crmContext.Crm_Opportunity.Where(q => q.IsDeleted == false);
           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = opportunities.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
           }
           var contacts = this.GetContact();
           var saleStages = this.GetSaleStage();
           var leadSources = this.GetAllLeadSource();
           var customers = this.GetAllCustomers();
           var users = this.GetAllUsers();

           foreach (var crmOpportunity in opportunities)
           {
               var membersContact = contacts.FirstOrDefault(c => c.ContactId == crmOpportunity.ContactId);
               if (membersContact != null)
                   crmOpportunity.ContactName =
                       membersContact.FirstName;
               var crmSaleStage = saleStages.FirstOrDefault(c => c.SaleStageId == crmOpportunity.SaleStageId);
               if (crmSaleStage != null)
                   crmOpportunity.SaleStageName =
                       crmSaleStage.Description;
               var crmLeadSource = leadSources.FirstOrDefault(c => c.LeadSourceId == crmOpportunity.LeadSourceId);
               if (crmLeadSource != null)
                   crmOpportunity.LeadSourceName =
                       crmLeadSource.Description;
               var membersCustomer = customers.FirstOrDefault(c => c.CustomerId == crmOpportunity.CustomerId);
               if (membersCustomer != null)
                   crmOpportunity.CustomerName =
                       membersCustomer.FirstName;
               var crmUserDto = users.FirstOrDefault(c => c.UserId == crmOpportunity.AssignedTo);
               if (crmUserDto != null)
                   crmOpportunity.AssignedMember =
                       crmUserDto.UserName;
           }

           return opportunities;
       }

       /// <summary>
       /// Updates the opportunity.
       /// </summary>
       /// <param name="opportunity">The opportunity.</param>
       public void UpdateOpportunity(Crm_Opportunity opportunity)
       {
           try
           {
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_Opportunity.AddOrUpdate(opportunity);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Savees the opportunity.
       /// </summary>
       /// <param name="opportunity">The opportunity.</param>
       public void SaveOpportunity(Crm_Opportunity opportunity)
       {
           try
           {
               _crmContext.Crm_Opportunity.Add(opportunity);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Deletes the opportunity.
       /// </summary>
       /// <param name="opportunityId">The opportunity identifier.</param>
       public void DeleteOpportunity(string opportunityId)
       {
           var quote = _crmContext.Crm_Opportunity.Find(int.Parse(opportunityId));

           quote.IsDeleted = true;
           _crmContext.Crm_Opportunity.AddOrUpdate(quote);
           _crmContext.SaveChanges();
       }

       /// <summary>
       /// Gets the sale stage.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_SaleStage> GetSaleStage()
       {
           return _crmContext.Crm_SaleStage;
       }
        #endregion

        #region Contact Operations
       /// <summary>
       /// Gets the contract.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Members_Contact> GetContact()
       {
           var contacts = _crmContext.Members_Contact.Where(q => q.IsDeleted == false);
           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = contacts.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
           }

           foreach (var membersContact in contacts)
           {
               var crmUserDto = this.GetAllUsers().FirstOrDefault(u => u.UserId == membersContact.AssignedTo);
               if (crmUserDto != null)
                   membersContact.AssignPerson =
                       crmUserDto.FirstName;
               var membersCustomer = GetAllCustomers().FirstOrDefault(c => c.CustomerId == membersContact.CustomerId);
               if (membersCustomer != null)
                   membersContact.CustomerName =
                       membersCustomer.FirstName;
           }

           return contacts;
       }
       /// <summary>
       /// Saves the contact.
       /// </summary>
       /// <param name="contact">The contact.</param>
       public void SaveContact(Members_Contact contact)
       {
           try
           {
               _crmContext.Members_Contact.AddOrUpdate(contact);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Updates the contact.
       /// </summary>
       /// <param name="contact">The contact.</param>
        public void UpdateContact(Members_Contact contact)
        {
            try
            {
                _crmContext.Configuration.ValidateOnSaveEnabled = false;
                _crmContext.Members_Contact.AddOrUpdate(contact);
                _crmContext.SaveChanges();
            }
            catch (Exception ex)
            {

                this.SaveErrorLog(new ErrorLog
                {
                    CreateDate = DateTime.Now,
                    ExceptionMessage = ex.Message,
                    ExceptionType = ex.GetType().ToString(),
                    ReferenceNumber = "Test",
                    TargetSiteModule = ex.StackTrace.Trim()
                });
            }
            
        }

        /// <summary>
        /// Deletes the contact.
        /// </summary>
        /// <param name="contactId">The contact identifier.</param>
       public void DeleteContact(string contactId)
       {
           var contact = _crmContext.Members_Contact.Find(int.Parse(contactId));

           contact.IsDeleted = true;
           _crmContext.Members_Contact.AddOrUpdate(contact);
           _crmContext.SaveChanges();
       }
        #endregion

       #region Organization Operations
       /// <summary>
       /// Gets the contract.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Organization> GetOrganization()
       {
           var contacts = _crmContext.Crm_Organization.Where(q => q.IsDeleted == false);
           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = contacts.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
           }

           var cities = this.GetAllCities();
           var users = this.GetAllUsers();

           foreach (var crmOrganization in contacts)
           {
               var thisCity = cities.FirstOrDefault(c => c.Code == crmOrganization.BillingCity);
               if (thisCity != null)
                   crmOrganization.CityName = thisCity.Name;
               var crmUserDto = users.FirstOrDefault(c => c.UserId == crmOrganization.AssignedTo);
               if (crmUserDto != null)
                   crmOrganization.AssignedMember =
                       crmUserDto.UserName;
           }

           return contacts;
       }
       /// <summary>
       /// Saves the contact.
       /// </summary>
       /// <param name="organization">The organization.</param>
       public void SaveOrganization(Crm_Organization organization)
       {
           try
           {
               _crmContext.Crm_Organization.AddOrUpdate(organization);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Updates the contact.
       /// </summary>
       /// <param name="organization">The organization.</param>
       public void UpdateOrganization(Crm_Organization organization)
       {
           try
           {
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_Organization.AddOrUpdate(organization);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }

       }

       /// <summary>
       /// Deletes the contact.
       /// </summary>
       /// <param name="organizationId">The organization identifier.</param>
       public void DeleteOrganization(string organizationId)
       {
           var organization = _crmContext.Crm_Organization.Find(int.Parse(organizationId));

           organization.IsDeleted = true;
           _crmContext.Crm_Organization.AddOrUpdate(organization);
           _crmContext.SaveChanges();
       }

       /// <summary>
       /// Gets the industry.
       /// </summary>
       /// <returns></returns>
       public IQueryable<This_Industry> GetIndustry()
       {
           var industries = _crmContext.This_Industry;

           return industries;
       }
       #endregion

       #region Sale Order Operations
       /// <summary>
       /// Gets the contract.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_SaleOrder> GetSaleOrder()
       {
           var contacts = _crmContext.Crm_SaleOrder.Where(q => q.IsActive == true);
           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = contacts.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
           }

           return contacts;
       }
       /// <summary>
       /// Saves the contact.
       /// </summary>
       /// <param name="order">The order.</param>
       public void SaveSaleOrder(Crm_SaleOrder order)
       {
           try
           {
               _crmContext.Crm_SaleOrder.Add(order);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Updates the contact.
       /// </summary>
       /// <param name="order">The order.</param>
       public void UpdateSaleOrder(Crm_SaleOrder order)
       {
           try
           {
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_SaleOrder.AddOrUpdate(order);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }

       }

       /// <summary>
       /// Deletes the contact.
       /// </summary>
       /// <param name="orderId">The order identifier.</param>
       public void DeleteSaleOrder(string orderId)
       {
           var order = _crmContext.Crm_SaleOrder.Find(int.Parse(orderId));

           order.IsActive = false;
           _crmContext.Crm_SaleOrder.AddOrUpdate(order);
           _crmContext.SaveChanges();
       }
       #endregion

        #region Service Operations
       /// <summary>
       /// Gets the contract.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Service> GetService()
       {
           var contacts = _crmContext.Crm_Service.Where(q => q.IsActive == true);
           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = contacts.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
           }

           var units = this.GetAllUnits();
           var users = this.GetAllUsers();
           var serviceCategories = this.GetServiceCategory();

           foreach (var crmService in contacts)
           {
               var thisUnit = units.FirstOrDefault(u => u.UnitId == crmService.UsageUnitId);
               if (thisUnit != null)
                   crmService.UsageUnitName = thisUnit.Text;

               var owner = users.FirstOrDefault(u => u.UserId == crmService.OwnerId);
               if (owner != null)
                   crmService.OwnerName = owner.UserName;

               var category = serviceCategories.FirstOrDefault(u => u.CategoryId == crmService.CategoryId);
               if (category != null)
                   crmService.CategoryName = category.Description;
           }

           return contacts;
       }

       /// <summary>
       /// Gets the service category.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_ServiceCategory> GetServiceCategory()
       {
           var contacts = _crmContext.Crm_ServiceCategory;

           return contacts;
       }
       /// <summary>
       /// Saves the contact.
       /// </summary>
       /// <param name="service">The service.</param>
       public void SaveService(Crm_Service service)
       {
           try
           {
               _crmContext.Crm_Service.AddOrUpdate(service);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Updates the contact.
       /// </summary>
       /// <param name="service">The service.</param>
       public void UpdateService(Crm_Service service)
       {
           try
           {
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_Service.AddOrUpdate(service);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }

       }

       /// <summary>
       /// Deletes the contact.
       /// </summary>
       /// <param name="serviceId">The service identifier.</param>
       public void DeleteService(string serviceId)
       {
           var service = _crmContext.Crm_Service.Find(int.Parse(serviceId));

           service.IsActive = false;
           _crmContext.Crm_Service.AddOrUpdate(service);
           _crmContext.SaveChanges();
       }
        #endregion

       /// <summary>
       /// Gets the contract.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_PriceBook> GetPriceBook()
       {
           var priceBook = _crmContext.Crm_PriceBook;
           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = priceBook.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
           }


           var  currencies = this.GetAllCurrencies();
           foreach (var crmPriceBook in priceBook)
           {
               var thisCurrency = currencies.FirstOrDefault(c => c.CurrencyId == crmPriceBook.CurrencyId);
               if (thisCurrency != null)
                   crmPriceBook.CurrencyName = thisCurrency.Name;

               crmPriceBook.StatusName = crmPriceBook.Status == true ? "Active" : "Passive";
           }

           return priceBook;
       }

       /// <summary>
       /// Saves the contact.
       /// </summary>
       /// <param name="priceBook">The price book.</param>
       public void SavePriceBook(Crm_PriceBook priceBook)
       {
           try
           {
               _crmContext.Crm_PriceBook.AddOrUpdate(priceBook);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Updates the contact.
       /// </summary>
       /// <param name="service">The service.</param>
       public void UpdatePriceBook(Crm_PriceBook priceBook)
       {
           try
           {
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_PriceBook.AddOrUpdate(priceBook);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }

       }

       /// <summary>
       /// Deletes the contact.
       /// </summary>
       /// <param name="serviceId">The service identifier.</param>
       public void DeletePriceBook(string priceBookId)
       {
           var service = _crmContext.Crm_PriceBook.Find(int.Parse(priceBookId));

           service.Status = false;
           _crmContext.Crm_PriceBook.AddOrUpdate(service);
           _crmContext.SaveChanges();
       }

       /// <summary>
       /// Saves the vendor.
       /// </summary>
       /// <param name="vendor">The vendor.</param>
       public void SaveVendor(Crm_Vendor vendor)
       {
           try
           {
               _crmContext.Crm_Vendor.AddOrUpdate(vendor);
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }


       }

       /// <summary>
       /// Updates the vendor.
       /// </summary>
       /// <param name="vendor">The vendor.</param>
       public void UpdateVendor(Crm_Vendor vendor)
       {
           try
           {
               _crmContext.Configuration.ValidateOnSaveEnabled = false;
               _crmContext.Crm_Vendor.AddOrUpdate(vendor);
               _crmContext.SaveChanges();
           }
           catch (Exception ex)
           {

               this.SaveErrorLog(new ErrorLog
               {
                   CreateDate = DateTime.Now,
                   ExceptionMessage = ex.Message,
                   ExceptionType = ex.GetType().ToString(),
                   ReferenceNumber = "Test",
                   TargetSiteModule = ex.StackTrace.Trim()
               });
           }

       }

       /// <summary>
       /// Deletes the vendor.
       /// </summary>
       /// <param name="vendorId">The vendor identifier.</param>
       public void DeleteVendor(string vendorId)
       {
           var vendor = _crmContext.Crm_Vendor.Find(int.Parse(vendorId));

           vendor.Status = false;
           _crmContext.Crm_Vendor.AddOrUpdate(vendor);
           _crmContext.SaveChanges();
       }

       /// <summary>
       /// Gets the contract.
       /// </summary>
       /// <returns></returns>
       public IQueryable<Crm_Vendor> GetVendors()
       {
           var contacts = _crmContext.Crm_Vendor.Where(q => q.Status == true);
           var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
           var claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
           var firstOrDefault = contacts.FirstOrDefault();
           if (firstOrDefault != null)
           {
               if (claim != null) firstOrDefault.CurrentRole = claim.Value;
           }

           var users = this.GetAllUsers();
           foreach (var crmVendor in contacts)
           {
               var crmUserDto = users.FirstOrDefault(c => c.UserId == crmVendor.AssignedTo);
               if (crmUserDto != null)
                   crmVendor.AssingMemberName =
                       crmUserDto.UserName;
           }

           return contacts;
       }
    }
}
