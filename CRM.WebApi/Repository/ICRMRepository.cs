﻿using System.Linq;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Entities.Product;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Core.Entities.SaleOrder;
using CRM.Core.Entities.Security;
using CRM.Core.Entities.Opportunity;
using CRM.Core.Entities.Sale;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;
using CRM.Core.Entities.Vendor;

namespace CRM.WebApi.Repository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface ICRMRepository
    {


        /// <summary>
        /// Gers the campaign by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Crm_Campaign GetCampaignById(int id);

        /// <summary>
        /// Gets the CRM campaign.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Campaign> GetCrmCampaigns();

        /// <summary>
        /// Gets the CRM campaign dto.
        /// </summary>
        /// <returns></returns>
        IQueryable<CrmCampaignDto> GetCrmCampaignDto();

        /// <summary>
        /// Gets the CRM campaign dto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        CrmCampaignDto GetCrmCampaignDto(int? id);

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns></returns>
        CrmCampaignDto GetAllProducts();

        /// <summary>
        /// Gets all members.
        /// </summary>
        /// <returns></returns>
        CrmCampaignDto GetAllMembers();

        /// <summary>
        /// Saves the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        void SaveCampaign(CrmCampaignDto campaign);

        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        void UpdateCampaign(CrmCampaignDto campaign);

        /// <summary>
        /// Deletes the campaign.
        /// </summary>
        /// <param name="id">Campaign id.</param>
        void DeleteCampaign(int id);

        /// <summary>
        /// Gets all campaign status.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_CampaignStatu> GetAllCampaignStatus();


        /// <summary>
        /// Gets the member role b member identifier.
        /// </summary>
        /// <param name="memberId">The member identifier.</param>
        /// <returns></returns>
        Members_Role GetMemberRoleBMemberId(int memberId);

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        IQueryable<CrmUserDto> GetAllUsers();

        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        void UpdateUser(CrmUserDto user);

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="user">The user.</param>
        void SaveUser(CrmUserDto user);

        /// <summary>
        /// Gets all roles.
        /// </summary>
        /// <returns></returns>
        IQueryable<CrmRoleDto> GetAllRoles();

        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        void DeleteUser(string userId);

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <returns></returns>
        IQueryable<Members_Customer> GetAllCustomers();

        /// <summary>
        /// Updates the CRM customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        void UpdateCrmCustomer(Members_Customer customer);

        /// <summary>
        /// Saves the CRM customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        void SaveCrmCustomer(Members_Customer customer);

        /// <summary>
        /// Deletes the CRM customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        void DeleteCrmCustomer(string customerId);

        /// <summary>
        /// Gets all CRM products.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Product> GetAllCrmProducts();

        /// <summary>
        /// Updates the CRM products.
        /// </summary>
        /// <param name="product">The product.</param>
        void UpdateCrmProducts(Crm_Product product);

        /// <summary>
        /// Saves the CRM product.
        /// </summary>
        /// <param name="product">The product.</param>
        void SaveCrmProduct(Crm_Product product);

        /// <summary>
        /// Deletes the CRM product.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        void DeleteCrmProduct(string productId);

        /// <summary>
        /// Gets all CRM leads.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Lead> GetAllCrmLeads();

        /// <summary>
        /// Updates the CRM leads.
        /// </summary>
        /// <param name="lead">The lead.</param>
        void UpdateCrmLeads(Crm_Lead lead);

        /// <summary>
        /// Saves the CRM lead.
        /// </summary>
        /// <param name="lead">The lead.</param>
        void SaveCrmLead(Crm_Lead lead);

        /// <summary>
        /// Deletes the CRM lead.
        /// </summary>
        /// <param name="leadId">The lead identifier.</param>
        void DeleteCrmLead(string leadId);

        /// <summary>
        /// Gets all districts.
        /// </summary>
        /// <returns></returns>
        IQueryable<This_District> GetAllDistricts();

        /// <summary>
        /// Gets all cities.
        /// </summary>
        /// <returns></returns>
        IQueryable<This_City> GetAllCities();

        /// <summary>
        /// Gets the quotes.
        /// </summary>
        /// <returns></returns>
        IQueryable<Projects_Quote> GetQuotes();

        /// <summary>
        /// Updates the quote.
        /// </summary>
        /// <param name="quote">The quote.</param>
        void UpdateQuote(Projects_Quote quote);

        /// <summary>
        /// Saves the quote.
        /// </summary>
        /// <param name="quote">The quote.</param>
        void SaveQuote(Projects_Quote quote);

        /// <summary>
        /// Deletes the quote.
        /// </summary>
        /// <param name="quoteId">The quote identifier.</param>
        void DeleteQuote(string quoteId);

        /// <summary>
        /// Gets the contract.
        /// </summary>
        /// <returns></returns>
        IQueryable<Members_Contact> GetContact();

        /// <summary>
        /// Gets the project.
        /// </summary>
        /// <returns></returns>
        IQueryable<Projects_Project> GetProject();

        /// <summary>
        /// Gets the quote stage.
        /// </summary>
        /// <returns></returns>
        IQueryable<Projects_QuoteStage> GetQuoteStage();

        /// <summary>
        /// Gets all lead source.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_LeadSource> GetAllLeadSource();

        /// <summary>
        /// Saves the contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        void SaveContact(Members_Contact contact);

        /// <summary>
        /// Updates the contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        void UpdateContact(Members_Contact contact);

        /// <summary>
        /// Deletes the contact.
        /// </summary>
        /// <param name="contactId">The contact identifier.</param>
        void DeleteContact(string contactId);

        #region Opportunuty Operations
        /// <summary>
        /// Gets the quotes.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Opportunity> GetOpportunities();

        /// <summary>
        /// Updates the opportunity.
        /// </summary>
        /// <param name="opportunity">The opportunity.</param>
        void UpdateOpportunity(Crm_Opportunity opportunity);

        /// <summary>
        /// Savees the opportunity.
        /// </summary>
        /// <param name="opportunity">The opportunity.</param>
        void SaveOpportunity(Crm_Opportunity opportunity);

        /// <summary>
        /// Deletes the opportunity.
        /// </summary>
        /// <param name="opportunityId">The opportunity identifier.</param>
        void DeleteOpportunity(string opportunityId);

        /// <summary>
        /// Gets the sale stage.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_SaleStage> GetSaleStage();
        #endregion

        #region Organization Operations

        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Organization> GetOrganization();

        /// <summary>
        /// Saves the organization.
        /// </summary>
        /// <param name="organization">The organization.</param>
        void SaveOrganization(Crm_Organization organization);

        /// <summary>
        /// Updates the organization.
        /// </summary>
        /// <param name="organization">The organization.</param>
        void UpdateOrganization(Crm_Organization organization);

        /// <summary>
        /// Deletes the organization.
        /// </summary>
        /// <param name="organizationId">The organization identifier.</param>
        void DeleteOrganization(string organizationId);

        IQueryable<This_Industry> GetIndustry();

        #endregion

        /// <summary>
        /// Gets the sale order.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_SaleOrder> GetSaleOrder();

        /// <summary>
        /// Saves the sale order.
        /// </summary>
        /// <param name="order">The order.</param>
        void SaveSaleOrder(Crm_SaleOrder order);

        /// <summary>
        /// Updates the sale order.
        /// </summary>
        /// <param name="order">The order.</param>
        void UpdateSaleOrder(Crm_SaleOrder order);

        /// <summary>
        /// Deletes the sale order.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        void DeleteSaleOrder(string orderId);

        /// <summary>
        /// Gets all currencies.
        /// </summary>
        /// <returns></returns>
        IQueryable<This_Currency> GetAllCurrencies();

        IQueryable<Crm_Service> GetService();

        void SaveService(Crm_Service service);

        void UpdateService(Crm_Service service);

        void DeleteService(string serviceId);

        IQueryable<Crm_ServiceCategory> GetServiceCategory();

        IQueryable<This_Unit> GetAllUnits();

        IQueryable<Crm_PriceBook> GetPriceBook();

        void SavePriceBook(Crm_PriceBook priceBook);

        void UpdatePriceBook(Crm_PriceBook priceBook);

        void DeletePriceBook(string priceBookId);

         IQueryable<Crm_Vendor> GetAllVendors();

        void SaveVendor(Crm_Vendor vendor);

        void UpdateVendor(Crm_Vendor vendor);

        void DeleteVendor(string vendorId);

        IQueryable<Crm_Vendor> GetVendors();
    }
}