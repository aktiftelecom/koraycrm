using System.Web.Mvc;
using CRM.Data;
using CRM.Data.Repository;
using CRM.Data.Repository.ContactRepository;
using CRM.Data.Repository.CrmRepository;
using CRM.Data.Repository.CustomerRepository;
using CRM.Data.Repository.LeadRepository;
using CRM.Data.Repository.OrganizationRepository;
using CRM.Data.Repository.PriceBookRepository;
using CRM.Data.Repository.ProductRepository;
using CRM.Data.Repository.QuoteRepository;
using CRM.Data.Repository.SaleOrderRepository;
using CRM.Data.Repository.ServiceRepository;
using CRM.Data.Repository.UserRepository;
using CRM.Data.Repository.VendorRepository;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CRM.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CRM.App_Start.NinjectWebCommon), "Stop")]

namespace CRM.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICRMRepository>().To<CRMRepository>().InRequestScope();
            kernel.Bind<IUserRepository>().To<UserRepository>().InRequestScope();
            kernel.Bind<ICustomerRepository>().To<CustomerRepository>().InRequestScope();
            kernel.Bind<IProductRepository>().To<ProductRepository>().InRequestScope();
            kernel.Bind<ILeadRepository>().To<LeadRepository>().InRequestScope();
            kernel.Bind<IQuoteRepository>().To<QuoteRepository>().InRequestScope();
            kernel.Bind<IOpportunityRepository>().To<OpportunityRepository>().InRequestScope();
            kernel.Bind<IContactRepository>().To<ContactRepository>().InRequestScope();
            kernel.Bind<IOrganizationRepository>().To<OrganizationRepository>().InRequestScope();
            kernel.Bind<ISaleOrderRepository>().To<SaleOrderRepository>().InRequestScope();
            kernel.Bind<IServiceRepository>().To<ServiceRepository>().InRequestScope();
            kernel.Bind<IPriceBookRepository>().To<PriceBookRepository>().InRequestScope();
            kernel.Bind<IVendorRepository>().To<VendorRepository>().InRequestScope();
            kernel.Unbind<ModelValidatorProvider>();
        }        
    }
}
