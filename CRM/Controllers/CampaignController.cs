﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Data.Repository.CrmRepository;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class CampaignController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private ICRMRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<CrmCampaignDto> model;
        #endregion

        public CampaignController(ICRMRepository repository)
        {
            this._repository = repository;
        }

        [Authorize(Order = 1)]
        public ActionResult Test()
        {
            return View();
        }

        public ActionResult CampaignsNewForm()
        {
            model = _repository.GetCrmCampaignDto();

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var memberList = model.ToList().Select(item => item.AssignMember).ToList();
            ViewData["AssignMember"] = memberList;

            var typeList = model.ToList().Select(item => item.AllCampaignTypes).ToList();
            ViewData["Types"] = typeList.FirstOrDefault();

            var responses = model.ToList().Select(item => item.AllExpectedResponses).ToList();
            ViewData["Responses"] = responses.FirstOrDefault();

            var statusList = model.ToList().Select(item => item.AllCampaignStatus).ToList();
            ViewData["Status"] = statusList.FirstOrDefault();
            Session["Status"] = statusList.FirstOrDefault();

            var productList = model.ToList().Select(item => item.Prodcut).ToList();
            ViewData["Products"] = productList;

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            foreach (var crmCampaignDto in model)
            {
                var crmCampaignExpectedResponseTypeDto = crmCampaignDto.AllExpectedResponses.FirstOrDefault(
                    x => x.ExpectedResponseTypeId == crmCampaignDto.ExpectedResponse);
                if (crmCampaignExpectedResponseTypeDto != null)
                    crmCampaignDto.ExpectedResponseTypeName =
                        crmCampaignExpectedResponseTypeDto.ExpectedResponseTypeName;

                var status = crmCampaignDto.AllCampaignStatus.FirstOrDefault(
                    x => x.CampaignStatusId == crmCampaignDto.CampaignStatusId);
                if (status != null)
                    crmCampaignDto.StatusNameId =
                        status.Description;

                var type = crmCampaignDto.AllCampaignTypes.FirstOrDefault(
                    x => x.TypeNameId == crmCampaignDto.TypeNameId);
                if (type != null)
                    crmCampaignDto.TypeName =
                        type.TypeName;
            }

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            
            return View(new CrmCampaignDto());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetCrmCampaignDto();

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var memberList = model.ToList().Select(item => item.AssignMember).ToList();
            ViewData["AssignMember"] = memberList;

            var typeList = model.ToList().Select(item => item.AllCampaignTypes).ToList();
            ViewData["Types"] = typeList.FirstOrDefault();

            var responses = model.ToList().Select(item => item.AllExpectedResponses).ToList();
            ViewData["Responses"] = responses.FirstOrDefault();

            var statusList = model.ToList().Select(item => item.AllCampaignStatus).ToList();
            ViewData["Status"] = statusList.FirstOrDefault();
            Session["Status"] = statusList.FirstOrDefault();

            var productList = model.ToList().Select(item => item.Prodcut).ToList();
            ViewData["Products"] = productList;

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            foreach (var crmCampaignDto in model)
            {
                var crmCampaignExpectedResponseTypeDto = crmCampaignDto.AllExpectedResponses.FirstOrDefault(
                    x => x.ExpectedResponseTypeId == crmCampaignDto.ExpectedResponse);
                if (crmCampaignExpectedResponseTypeDto != null)
                    crmCampaignDto.ExpectedResponseTypeName =
                        crmCampaignExpectedResponseTypeDto.ExpectedResponseTypeName;

                var status = crmCampaignDto.AllCampaignStatus.FirstOrDefault(
                    x => x.CampaignStatusId == crmCampaignDto.CampaignStatusId);
                if (status != null)
                    crmCampaignDto.StatusNameId =
                        status.Description;

                var type = crmCampaignDto.AllCampaignTypes.FirstOrDefault(
                    x => x.TypeNameId == crmCampaignDto.TypeNameId);
                if (type != null)
                    crmCampaignDto.TypeName =
                        type.TypeName;
            }

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new CrmCampaignDto());
        }

        public ActionResult TabPage()
        {
            model = _repository.GetCrmCampaignDto();

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var memberList = model.ToList().Select(item => item.AssignMember).ToList();
            ViewData["AssignMember"] = memberList;

            var typeList = model.ToList().Select(item => item.AllCampaignTypes).ToList();
            ViewData["Types"] = typeList.FirstOrDefault();

            var responses = model.ToList().Select(item => item.AllExpectedResponses).ToList();
            ViewData["Responses"] = responses.FirstOrDefault();

            var statusList = model.ToList().Select(item => item.AllCampaignStatus).ToList();
            ViewData["Status"] = statusList.FirstOrDefault();
            Session["Status"] = statusList.FirstOrDefault();

            var productList = model.ToList().Select(item => item.Prodcut).ToList();
            ViewData["Products"] = productList;

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            foreach (var crmCampaignDto in model)
            {
                var crmCampaignExpectedResponseTypeDto = crmCampaignDto.AllExpectedResponses.FirstOrDefault(
                    x => x.ExpectedResponseTypeId == crmCampaignDto.ExpectedResponse);
                if (crmCampaignExpectedResponseTypeDto != null)
                    crmCampaignDto.ExpectedResponseTypeName =
                        crmCampaignExpectedResponseTypeDto.ExpectedResponseTypeName;

                var status = crmCampaignDto.AllCampaignStatus.FirstOrDefault(
                    x => x.CampaignStatusId == crmCampaignDto.CampaignStatusId);
                if (status != null)
                    crmCampaignDto.StatusNameId =
                        status.Description;

                var type = crmCampaignDto.AllCampaignTypes.FirstOrDefault(
                    x => x.TypeNameId == crmCampaignDto.TypeNameId);
                if (type != null)
                    crmCampaignDto.TypeName =
                        type.TypeName;
            }

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new CrmCampaignDto());
        }

        /// <summary>
        /// Grids the view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetCrmCampaignDto();

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            
            var memberList = model.ToList().Select(item => item.AssignMember).ToList();
            ViewData["AssignMember"] = memberList;
            
            var typeList = model.ToList().Select(item => item.AllCampaignTypes).ToList();
            ViewData["Types"] = typeList.FirstOrDefault();

            var responses = model.ToList().Select(item => item.AllExpectedResponses).ToList();
            ViewData["Responses"] = responses.FirstOrDefault();

            var statusList = model.ToList().Select(item => item.AllCampaignStatus).ToList();
            ViewData["Status"] = statusList.FirstOrDefault();
            Session["Status"] = statusList.FirstOrDefault();

            var productList = model.ToList().Select(item => item.Prodcut).ToList();
            ViewData["Products"] = productList;

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            foreach (var crmCampaignDto in model)
            {
                var crmCampaignExpectedResponseTypeDto = crmCampaignDto.AllExpectedResponses.FirstOrDefault(
                    x => x.ExpectedResponseTypeId == crmCampaignDto.ExpectedResponse);
                if (crmCampaignExpectedResponseTypeDto != null)
                    crmCampaignDto.ExpectedResponseTypeName =
                        crmCampaignExpectedResponseTypeDto.ExpectedResponseTypeName;

                var status = crmCampaignDto.AllCampaignStatus.FirstOrDefault(
                    x => x.CampaignStatusId== crmCampaignDto.CampaignStatusId);
                if (status != null)
                    crmCampaignDto.StatusNameId =
                        status.Description;

                var type = crmCampaignDto.AllCampaignTypes.FirstOrDefault(
                    x => x.TypeNameId == crmCampaignDto.TypeNameId);
                if (type != null)
                    crmCampaignDto.TypeName =
                        type.TypeName;
            }

            return PartialView("_GridViewPartial", model.OrderByDescending(x => x.CampaignId).ToList());
        }

        /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] CrmCampaignDto param)
        {
            int expectedResponseTypeNameId;
            if (!int.TryParse(param.ExpectedResponseTypeName, out expectedResponseTypeNameId))
            {
                var crmCampaignExpectedResponseTypeDto = param.AllExpectedResponses.FirstOrDefault(
               p => p.ExpectedResponseTypeName == param.ExpectedResponseTypeName);
                if (crmCampaignExpectedResponseTypeDto != null)
                    param.ExpectedResponseTypeName =
                        crmCampaignExpectedResponseTypeDto.ExpectedResponseTypeId.ToString(CultureInfo.InvariantCulture);
            }

            int statusNAmeID;
            if (!int.TryParse(param.StatusNameId, out statusNAmeID))
            {
                if (Session["Status"] != null)
                {
                    var status = ((List<Crm_CampaignStatu>)Session["Status"]).FirstOrDefault(
               p => p.Description == param.StatusNameId);
                    if (status != null)
                        param.CampaignStatusId =
                            status.CampaignStatusId;
                }

            }
            else
            {
                param.CampaignStatusId = int.Parse(param.StatusNameId);
            }

            int typeNAmeID;
            if (!int.TryParse(param.TypeName, out typeNAmeID))
            {

                var type = (param.AllCampaignTypes.FirstOrDefault(
           p => p.TypeName == param.TypeName));
                if (type != null)
                    param.TypeNameId =
                        type.TypeNameId;

            }
            else
            {
                param.TypeNameId = int.Parse(param.TypeName);
            }
            
            _repository.SaveCampaign(param);

            var model = _repository.GetCrmCampaignDto();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("Test");
           
        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] CrmCampaignDto param)
        {
            if (param.CampaignId > 0)
            {
                try
                {
                    int expectedResponseTypeNameId;
                    if (!int.TryParse(param.ExpectedResponseTypeName, out expectedResponseTypeNameId))
                    {
                        var crmCampaignExpectedResponseTypeDto = param.AllExpectedResponses.FirstOrDefault(
                       p => p.ExpectedResponseTypeName == param.ExpectedResponseTypeName);
                        if (crmCampaignExpectedResponseTypeDto != null)
                            param.ExpectedResponseTypeName =
                                crmCampaignExpectedResponseTypeDto.ExpectedResponseTypeId.ToString(CultureInfo.InvariantCulture);
                    }

                    int statusNAmeID;
                    if (!int.TryParse(param.StatusNameId, out statusNAmeID))
                    {
                        if (Session["Status"] != null)
                        {
                            var status = ((List<Crm_CampaignStatu>)Session["Status"]).FirstOrDefault(
                       p => p.Description == param.StatusNameId);
                            if (status != null)
                                param.CampaignStatusId =
                                    status.CampaignStatusId;
                        }
                        
                    }
                    else
                    {
                        param.CampaignStatusId = int.Parse(param.StatusNameId);
                    }

                    int typeNAmeID;
                    if (!int.TryParse(param.TypeName, out typeNAmeID))
                    {
                        
                            var type = (param.AllCampaignTypes.FirstOrDefault(
                       p => p.TypeName == param.TypeName));
                            if (type != null)
                                param.TypeNameId =
                                    type.TypeNameId;

                    }
                    else
                    {
                        param.TypeNameId = int.Parse(param.TypeName);
                    }
                   
                    _repository.UpdateCampaign(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetCrmCampaignDto();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_GridViewPartial", model.OrderByDescending(x => x.CampaignId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="CampaignId">The campaign identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GridViewPartialDelete(string CampaignId)
        {
            var campaingId = int.Parse(CampaignId);
            if (campaingId >= 0)
            {
                try
                {
                    _repository.DeleteCampaign(campaingId);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }

            return View("Test");
        }


        /// <summary>
        /// Indexes the specified selected i ds hf.
        /// </summary>
        /// <param name="selectedIDsHF">The selected i ds hf.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            string _selectedIDs = selectedIDsHF;
            var result =_repository.GetCrmCampaignDto();
            if (result == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(result.ToList());
        }

        /// <summary>
        /// Grids the view editing partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult GridViewEditingPartial()
        {
            //Get all selected keys from e.customArgs on GridView callback
            string _selectedIDs = Request.Params["selectedIDs"];
            ViewData["_selectedIDs"] = _selectedIDs;

            var result = _repository.GetCrmCampaignDto();
            if (result == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_GridViewPartial", result.ToList());
        }

        public ActionResult LoadOnDemandPartial()
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            Thread.Sleep(1000);
            return PartialView("LoadOnDemandPartial");
        }

        [HttpPost]
        public void GetSelectedValuesFromGrid(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            ViewData["_selectedIDs"] = selectedIDsHF;
        }

        [HttpPost]
        public ActionResult BulkDelete(string selectedIDsHF)
        {
            var result = selectedIDsHF.Split(',');
            if (result[0] == "Delete")
            {

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    _repository.DeleteCampaign(int.Parse(result[i]));
                }
            }

            return View("Test");
        }

        [HttpGet]
        public ActionResult ExternalEditFormEdit(int CampaignId)
        {
            model = _repository.GetCrmCampaignDto();

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var memberList = model.ToList().Select(item => item.AssignMember).ToList();
            ViewData["AssignMember"] = memberList;

            var typeList = model.ToList().Select(item => item.AllCampaignTypes).ToList();
            ViewData["Types"] = typeList.FirstOrDefault();

            var responses = model.ToList().Select(item => item.AllExpectedResponses).ToList();
            ViewData["Responses"] = responses.FirstOrDefault();

            var statusList = model.ToList().Select(item => item.AllCampaignStatus).ToList();
            ViewData["Status"] = statusList.FirstOrDefault();
            Session["Status"] = statusList.FirstOrDefault();

            var productList = model.ToList().Select(item => item.Prodcut).ToList();
            ViewData["Products"] = productList;

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            foreach (var crmCampaignDto in model)
            {
                var crmCampaignExpectedResponseTypeDto = crmCampaignDto.AllExpectedResponses.FirstOrDefault(
                    x => x.ExpectedResponseTypeId == crmCampaignDto.ExpectedResponse);
                if (crmCampaignExpectedResponseTypeDto != null)
                    crmCampaignDto.ExpectedResponseTypeName =
                        crmCampaignExpectedResponseTypeDto.ExpectedResponseTypeName;

                var status = crmCampaignDto.AllCampaignStatus.FirstOrDefault(
                    x => x.CampaignStatusId == crmCampaignDto.CampaignStatusId);
                if (status != null)
                    crmCampaignDto.StatusNameId =
                        status.Description;

                var type = crmCampaignDto.AllCampaignTypes.FirstOrDefault(
                    x => x.TypeNameId == crmCampaignDto.TypeNameId);
                if (type != null)
                    crmCampaignDto.TypeName =
                        type.TypeName;
            }

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("CampaignsNewForm", model.FirstOrDefault(l => l.CampaignId == CampaignId));
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] CrmCampaignDto param)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                var allCampaigns = _repository.GetCrmCampaignDto();

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    var campaign = allCampaigns.FirstOrDefault(l => l.CampaignId == int.Parse(result[i]));
                    if (campaign != null)
                    {
                        int expectedResponseTypeNameId;
                        if (!int.TryParse(param.ExpectedResponseTypeName, out expectedResponseTypeNameId))
                        {
                            var crmCampaignExpectedResponseTypeDto = param.AllExpectedResponses.FirstOrDefault(
                           p => p.ExpectedResponseTypeName == param.ExpectedResponseTypeName);
                            if (crmCampaignExpectedResponseTypeDto != null)
                                campaign.ExpectedResponseTypeName =
                                    crmCampaignExpectedResponseTypeDto.ExpectedResponseTypeId.ToString(CultureInfo.InvariantCulture);
                        }

                        int statusNAmeID;
                        if (!int.TryParse(param.StatusNameId, out statusNAmeID))
                        {
                            if (Session["Status"] != null)
                            {
                                var status = ((List<Crm_CampaignStatu>)Session["Status"]).FirstOrDefault(
                           p => p.Description == param.StatusNameId);
                                if (status != null)
                                    campaign.CampaignStatusId =
                                        status.CampaignStatusId;
                            }

                        }
                        else
                        {
                            campaign.CampaignStatusId = int.Parse(param.StatusNameId);
                        }

                        int typeNAmeID;
                        if (!int.TryParse(param.TypeName, out typeNAmeID))
                        {

                            var type = (param.AllCampaignTypes.FirstOrDefault(
                       p => p.TypeName == param.TypeName));
                            if (type != null)
                                campaign.TypeNameId =
                                    type.TypeNameId;

                        }
                        else
                        {
                            campaign.TypeNameId = int.Parse(param.TypeName);
                        }

                        campaign.CampaignName = param.CampaignName;
                        campaign.AssignTo = param.AssignTo;
                        campaign.ExpectedCloseDate = param.ExpectedCloseDate.Equals(DateTime.MinValue) ? campaign.ExpectedCloseDate : param.ExpectedCloseDate;
                        campaign.UpdatedTime = DateTime.Now;
                        campaign.TargetAudience = param.TargetAudience;
                        campaign.Sponsor = param.Sponsor;
                        campaign.TargetSize = param.TargetSize;
                        campaign.NumberSent = param.NumberSent;
                        campaign.BudgetCost = param.BudgetCost;
                        campaign.ExpectedRevenue = param.ExpectedRevenue;
                        campaign.ExpectedSalesCount = param.ExpectedSalesCount;
                        campaign.ExpectedResponseCount = param.ExpectedResponseCount;
                        campaign.ExpectedROI = param.ExpectedROI;
                        campaign.ActualCost = param.ActualCost;
                        campaign.ActualSalesCount = param.ActualSalesCount;
                        campaign.ActualResponseCount = param.ActualResponseCount;
                        campaign.ActualROI = param.ActualROI;
                        campaign.Description = param.Description;

                        _repository.UpdateCampaign(campaign);
                    }
                }
            }

            return View("Test");

        }

        public ActionResult AssignToPartial()
        {
            return PartialView(new CrmCampaignDto());
        }

        
    }
}
