﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using CRM.Core.Entities.Contact;
using DevExpress.Web.Mvc;
using CRM.Data.Repository.ContactRepository;

namespace CRM.Controllers
{
    public class ContactController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IContactRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Members_Contact> model;
        #endregion

        public ContactController(IContactRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Contacts()
        {
            return View();
        }

        public ActionResult ContactsNewForm()
        {
             model = _repository.GetContact();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Members_Contact());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetContact();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Members_Contact());
        }

        public ActionResult TabPage()
        {
            model = _repository.GetContact();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Members_Contact());
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetContact();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_ContactGridViewPartial", model.OrderByDescending(x => x.ContactId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Members_Contact param)
         {
             
             param.IsDeleted = false;

            _repository.InsertContact(param);

            var model = _repository.GetContact();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View("Contacts");

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Members_Contact param)
        {
            if (param.ContactId > 0)
            {
                try
                {
                    param.IsDeleted = false;
                    _repository.UpdateContact(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetContact();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_ContactGridViewPartial", model.OrderByDescending(x => x.ContactId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="QuoteId">The quote identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GridViewPartialDelete(string ContactId)
        {
            _repository.DeleteContat(ContactId);

           
            return View("Contacts");
        }

        /// <summary>
        /// Contacts the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CustomerPartial()
        {
            return PartialView(new Members_Contact());
        }

        /// <summary>
        /// Leads the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult LeadSourcePartial()
        {
            return PartialView(new Members_Contact());
        }

        public ActionResult LoadOnDemandPartial()
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            Thread.Sleep(1000);
            return PartialView("LoadOnDemandPartial");
        }

        [HttpPost]
        public void GetSelectedValuesFromGrid(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            ViewData["_selectedIDs"] = selectedIDsHF;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] Members_Contact param)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                var contacts = _repository.GetContact();

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    var contact = contacts.FirstOrDefault(l => l.ContactId == int.Parse(result[i]));
                    if (contact != null)
                    {
                        contact.AssignedTo = param.AssignedTo != 0 ? param.AssignedTo : contact.AssignedTo;
                        contact.Assistant = param.Assistant != string.Empty ? param.Assistant : contact.Assistant;
                        contact.AssistantPhone = param.AssistantPhone != string.Empty ? param.AssistantPhone : contact.AssistantPhone;
                        contact.ContactId = param.ContactId != 0 ? param.ContactId : contact.ContactId;
                        contact.CustomerId = param.CustomerId != 0 ? param.CustomerId : contact.CustomerId;
                        contact.DateOfBirth = param.DateOfBirth.Equals(DateTime.MinValue) ? contact.DateOfBirth : param.DateOfBirth;
                        contact.Department = param.Department != string.Empty ? param.Department : contact.Department;
                        contact.Description = param.Description != string.Empty ? param.Description : contact.Description;
                        contact.DialogReference = param.DialogReference ?? contact.DialogReference;
                        contact.DoNotCall = param.DoNotCall;
                        contact.EmailOptionOut = param.EmailOptionOut;
                        contact.Fax = param.Fax != string.Empty ? param.Fax : contact.Fax;
                        contact.FirstName = param.FirstName != string.Empty ? param.FirstName : contact.FirstName;
                        contact.HomePhone = param.HomePhone != string.Empty ? param.HomePhone : contact.HomePhone;
                        contact.LastName = param.LastName ?? contact.LastName;
                        contact.LeadSourceId = param.LeadSourceId != 0 ? param.LeadSourceId : contact.LeadSourceId;
                        contact.MailingCity = param.MailingCity ?? contact.MailingCity;
                        contact.MailingCountry = param.MailingCountry ?? contact.MailingCountry;
                        contact.MailingPOBox = param.MailingPOBox != string.Empty ? param.MailingPOBox : contact.MailingPOBox;
                        contact.MailingState = param.MailingState != string.Empty ? param.MailingState : contact.MailingState;
                        contact.MailingStreet = param.MailingStreet != string.Empty ? param.MailingStreet : contact.MailingStreet;
                        contact.MailingZip = contact.MailingStreet = param.MailingZip != string.Empty ? param.MailingZip : contact.MailingZip;
                        contact.MobilePhone = contact.MailingStreet = param.MobilePhone != string.Empty ? param.MobilePhone : contact.MobilePhone;
                        contact.NotifyOwner = param.NotifyOwner;
                        contact.OfficePhone = param.OfficePhone != string.Empty ? param.OfficePhone : contact.OfficePhone;
                        contact.OtherCity = param.OtherCity != string.Empty ? param.OtherCity : contact.OtherCity;
                        contact.OtherCountry = param.OtherCountry != string.Empty ? param.OtherCountry : contact.OtherCountry;
                        contact.OtherPOBox = param.OtherPOBox != string.Empty ? param.OtherPOBox : contact.OtherPOBox;
                        contact.OtherPhone = param.OtherPhone != string.Empty ? param.OtherPhone : contact.OtherPhone;
                        contact.OtherState = param.OtherState != string.Empty ? param.OtherState : contact.OtherState;
                        contact.OtherStreet = param.OtherStreet != string.Empty ? param.OtherStreet : contact.OtherStreet;
                        contact.OtherZip = param.OtherZip != string.Empty ? param.OtherZip : contact.OtherZip;
                        contact.PortalUser = param.PortalUser;
                        contact.PrimaryEmail = param.PrimaryEmail != string.Empty ? param.PrimaryEmail : contact.PrimaryEmail;
                        contact.Referance = param.Referance;
                        contact.ReportsTo = param.ReportsTo ?? contact.ReportsTo;
                        contact.SecondaryEmail = param.SecondaryEmail != string.Empty ? param.SecondaryEmail : contact.SecondaryEmail;
                        contact.SupportEndDate = param.SupportEndDate.Equals(DateTime.MinValue) ? contact.SupportEndDate : param.SupportEndDate;
                        contact.SupportStartDate = param.SupportStartDate.Equals(DateTime.MinValue) ? contact.SupportStartDate : param.SupportStartDate;
                        contact.UpdatedTime = DateTime.Now;

                        _repository.UpdateContact(contact);
                    }
                }
            }

            return View("Contacts");

        }

        [HttpPost]
        public ActionResult BulkDelete(string selectedIDsHF)
        {
            var result = selectedIDsHF.Split(',');
            if (result[0] == "Delete")
            {

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    _repository.DeleteContat(result[i]);
                }
            }

            return View("Contacts");
        }

        [HttpGet]
        public ActionResult ExternalEditFormEdit(int ContactId)
        {
            model = _repository.GetContact();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("ContactsNewForm", model.FirstOrDefault(l => l.ContactId == ContactId));
        }

        public ActionResult AssignToPartial()
        {
            return PartialView(new Members_Contact());
        }

    }
}
