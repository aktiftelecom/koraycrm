﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.City;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Member;
using CRM.Data.Repository.CrmRepository;
using CRM.Data.Repository.CustomerRepository;
using DevExpress.Office.Utils;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class CustomerController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private ICustomerRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Members_Customer> model;
        #endregion

        public CustomerController(ICustomerRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Customers()
        {
            return View();
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetAllCustomers();

            var cities = model.FirstOrDefault().Cities;
            ViewData["Cities"] = cities;
            Session["Cities"] = cities;

            var types = model.FirstOrDefault().Types;
            types.RemoveAt(2);
            types.RemoveAt(2);
            ViewData["Types"] = types;
            Session["Types"] = types;

            var registerTypes = model.FirstOrDefault().RegisterTypes;
            registerTypes.RemoveAt(4);
            registerTypes.RemoveAt(4);
            registerTypes.RemoveAt(4);
            registerTypes.RemoveAt(4);
            ViewData["RegisterTypes"] = registerTypes;
            Session["RegisterTypes"] = ViewData["RegisterTypes"];

            foreach (var item in model)
            {
                var type = types.FirstOrDefault(t => t.Item1 == item.Type);
                if (type != null)
                {
                    item.TypeName = type.Item2;
                }
                else
                {
                   
                }

                var registerType = registerTypes.FirstOrDefault(t => t.Item1 == item.RegisterType);
                if (registerType != null)
                {
                    item.RegisterTypeName = registerType.Item2;
                }

                var city = cities.FirstOrDefault(c => c.Code == (item.CityId == null ? 1 : item.CityId.Value));
                if (city != null)
                {
                    item.CityName = city.Name;
                }
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_CustomerGridViewPartial", model.OrderByDescending(x => x.CustomerId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Members_Customer param)
         {
             param.Type = param.TypeName == null ? 1:int.Parse(param.TypeName);

             param.RegisterType = param.RegisterTypeName == null ? 1 : int.Parse(param.RegisterTypeName);

             param.CityId = param.CityName == null ? 0 : int.Parse(param.CityName);

            _repository.SaveCustomer(param);

            var model = _repository.GetAllCustomers();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_CustomerGridViewPartial", model.OrderByDescending(x => x.CustomerId).ToList());

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Members_Customer param)
        {
            if (param.CustomerId > 0)
            {
                try
                {
                    var type =
                        ((List<Tuple<int, string>>) Session["Types"]).FirstOrDefault(t => t.Item2 == param.TypeName);
                    if (type != null)
                    {
                        param.Type = type.Item1;
                    }
                    else if (!string.IsNullOrEmpty(param.TypeName))
                    {
                        param.Type = int.Parse(param.TypeName);
                    }

                    var registerType =
                        ((List<Tuple<int, string>>) Session["RegisterTypes"]).FirstOrDefault(t => t.Item2 == param.RegisterTypeName);
                    if (registerType != null)
                    {
                        param.RegisterType = registerType.Item1;
                    }
                    else if (!string.IsNullOrEmpty(param.RegisterTypeName))
                    {
                        param.RegisterType = int.Parse(param.RegisterTypeName);
                    }

                    param.CityId = param.CityName == null ? 0: int.Parse(param.CityName);

                    _repository.UpdateCustomer(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetAllCustomers();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_CustomerGridViewPartial", model.OrderByDescending(x => x.CustomerId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="CampaignId">The campaign identifier.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(string CustomerId)
        {
            _repository.DeleteCustomer(CustomerId);

            var allCustomers = _repository.GetAllCustomers();
            if (allCustomers == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_CustomerGridViewPartial", allCustomers.OrderByDescending(x => x.CustomerId).ToList());
        }
    }
}
