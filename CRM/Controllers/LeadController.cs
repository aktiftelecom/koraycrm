﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.City;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Manufacturer;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Product;
using CRM.Core.Entities.Unit;
using CRM.Core.Entities.Vendor;
using CRM.Data.Repository.CrmRepository;
using CRM.Data.Repository.CustomerRepository;
using CRM.Data.Repository.LeadRepository;
using CRM.Data.Repository.ProductRepository;
using DevExpress.Office.Utils;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class LeadController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private ILeadRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Crm_Lead> model;
        #endregion

        public LeadController(ILeadRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Leads()
        {
            return View();
        }

        public ActionResult LeadsNewForm()
        {
            model = _repository.GetAllLead();


            var crmLead = model.FirstOrDefault();
            if (crmLead != null)
            {
                var leadSources = crmLead.Crm_LeadSource;
                ViewData["Crm_LeadSource"] = leadSources;
                Session["Crm_LeadSource"] = leadSources;

                var leadStatu = crmLead.Crm_LeadStatu;
                ViewData["Crm_LeadStatu"] = leadStatu;
                Session["Crm_LeadStatu"] = leadStatu;

                var industry = crmLead.This_Industry;
                ViewData["This_Industry"] = industry;
                Session["This_Industry"] = industry;
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Lead());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetAllLead();


            var crmLead = model.FirstOrDefault();
            if (crmLead != null)
            {
                var leadSources = crmLead.Crm_LeadSource;
                ViewData["Crm_LeadSource"] = leadSources;
                Session["Crm_LeadSource"] = leadSources;

                var leadStatu = crmLead.Crm_LeadStatu;
                ViewData["Crm_LeadStatu"] = leadStatu;
                Session["Crm_LeadStatu"] = leadStatu;

                var industry = crmLead.This_Industry;
                ViewData["This_Industry"] = industry;
                Session["This_Industry"] = industry;
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Lead());
        }

        public ActionResult TabPage()
        {
            model = _repository.GetAllLead();


            var crmLead = model.FirstOrDefault();
            if (crmLead != null)
            {
                var leadSources = crmLead.Crm_LeadSource;
                ViewData["Crm_LeadSource"] = leadSources;
                Session["Crm_LeadSource"] = leadSources;

                var leadStatu = crmLead.Crm_LeadStatu;
                ViewData["Crm_LeadStatu"] = leadStatu;
                Session["Crm_LeadStatu"] = leadStatu;

                var industry = crmLead.This_Industry;
                ViewData["This_Industry"] = industry;
                Session["This_Industry"] = industry;
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Lead());
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetAllLead();

            
            var crmLead = model.FirstOrDefault();
            if (crmLead != null)
            {
                var leadSources = crmLead.Crm_LeadSource;
                ViewData["Crm_LeadSource"] = leadSources;
                Session["Crm_LeadSource"] = leadSources;

                var leadStatu = crmLead.Crm_LeadStatu;
                ViewData["Crm_LeadStatu"] = leadStatu;
                Session["Crm_LeadStatu"] = leadStatu;

                var industry = crmLead.This_Industry;
                ViewData["This_Industry"] = industry;
                Session["This_Industry"] = industry;

                foreach (var item in model)
                {
                    var leadSource = leadSources.FirstOrDefault(t => t.LeadSourceId == item.LeadSourceId);
                    if (leadSource != null)
                    {
                        item.LeadSourceNameId = leadSource.Description;
                    }

                    var statu = leadStatu.FirstOrDefault(t => t.LeadStatusId == item.LeadStatusId);
                    if (statu != null)
                    {
                        item.LeadStatuNameId = statu.Description;
                    }

                    var thisIndustry = industry.FirstOrDefault(c => c.IndustryId == item.IndustryId);
                    if (thisIndustry != null)
                    {
                        item.IndustryNameId = thisIndustry.Description;
                    }
                }
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_LeadGridViewPartial", model.OrderByDescending(x => x.LeadId).ToList());
        }

        /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <param name="selectedIDsHF">The selected i ds hf.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Lead param)
         {
             string value = EditorExtension.GetValue<string>("ProductName");
             param.IsActive = true;
            _repository.SaveLead(param);

            return View("Leads");

        }

        /// <summary>
        /// Bulks the edit.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
         [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Lead param)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                 var allLeads = _repository.GetAllLead();

                for (int i = 1; i <= result.Length-1; i++)
                {
                    var lead = allLeads.FirstOrDefault(l => l.LeadId == int.Parse(result[i]));
                    if (lead != null)
                    {
                        lead.AnnualRevenue = param.AnnualRevenue != 0 ? param.AnnualRevenue : lead.AnnualRevenue;
                        lead.AssignedTo = param.AssignedTo != 0 ? param.AssignedTo : lead.AssignedTo;
                        lead.CityId = param.CityId;
                        lead.Company = param.Company ?? lead.Company;
                        lead.Description = param.Description ?? lead.Description;
                        lead.Designation = param.Designation ?? lead.Designation;
                        lead.DistrictId = param.DistrictId;
                        lead.EmailOptOut = param.EmailOptOut;
                        lead.Fax = param.Fax ?? lead.Fax;
                        lead.FirstName = param.FirstName ?? lead.FirstName;
                        lead.IndustryId = param.IndustryId;
                        lead.LastName = param.LastName ?? lead.LastName;
                        lead.LeadSourceId = param.LeadSourceId;
                        lead.LeadStatusId = param.LeadStatusId;
                        lead.MobilePhone = param.MobilePhone ?? lead.MobilePhone;
                        lead.NumberEmployees = param.NumberEmployees != 0 ? param.NumberEmployees : lead.NumberEmployees;
                        lead.POBox = param.POBox ?? lead.POBox;
                        lead.PostalCode = param.PostalCode ?? lead.PostalCode;
                        lead.PrimaryEmail = param.PrimaryEmail ?? lead.PrimaryEmail;
                        lead.PrimaryPhone = param.PrimaryPhone ?? lead.PrimaryPhone;
                        lead.Rating = param.Rating != 0 ? param.Rating : lead.Rating;
                        lead.SecondaryEmail = param.SecondaryEmail ?? lead.SecondaryEmail;
                        lead.Street = param.Street ?? lead.Street;
                        lead.UpdatedTime = DateTime.Now;

                        _repository.UpdateLead(lead);
                    }
                }
            }

            return View("Leads");

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Lead param)
        {
            if (param.LeadId > 0)
            {
                try
                {
                    var leadSource =
                       ((List<Crm_LeadSource>)Session["Crm_LeadSource"]).FirstOrDefault(t => t.Description == param.LeadSourceNameId);
                    if (leadSource != null)
                    {
                        param.LeadSourceId = leadSource.LeadSourceId;
                    }
                    else if (!string.IsNullOrEmpty(param.LeadSourceNameId))
                    {
                        param.LeadSourceId = int.Parse(param.LeadSourceNameId);
                    }

                    var leadStatu =
                        ((List<Crm_LeadStatu>)Session["Crm_LeadStatu"]).FirstOrDefault(t => t.Description == param.LeadStatuNameId);
                    if (leadStatu != null)
                    {
                        param.LeadStatusId = leadStatu.LeadStatusId;
                    }
                    else if (!string.IsNullOrEmpty(param.LeadSourceNameId))
                    {
                        param.LeadStatusId = int.Parse(param.LeadSourceNameId);
                    }

                    var industry =
                        ((List<This_Industry>)Session["This_Industry"]).FirstOrDefault(t => t.Description == param.IndustryNameId);
                    if (industry != null)
                    {
                        param.IndustryId = industry.IndustryId;
                    }
                    else if (!string.IsNullOrEmpty(param.IndustryNameId))
                    {
                        param.IndustryId = int.Parse(param.IndustryNameId);
                    }

                    param.IsActive = true;
                    _repository.UpdateLead(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetAllLead();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_LeadGridViewPartial", model.OrderByDescending(x => x.LeadId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="LeadId">The lead identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GridViewPartialDelete(string LeadId)
        {
            _repository.DeleteLead(LeadId);

            return View("Leads");
        }

        public ActionResult CountryPartial()
        {
            return PartialView(new Crm_Lead());
        }
        public ActionResult CityPartial()
        {
            int country = (Request.Params["CityId"] != null) ? int.Parse(Request.Params["CityId"]) : -1;
            return PartialView(new Crm_Lead { CityId = country });
        }

        /// <summary>
        /// Gets the districts.
        /// </summary>
        /// <param name="city">The city.</param>
        /// <returns></returns>
        public IEnumerable<This_District> GetDistricts(int city)
        {
            var distritcs = _repository.GetAllDistrict();
            var list = distritcs.Where(d => d.CityId == city).ToList();
            
            return list;
        }

        public ActionResult IndustryPartial()
        {
            return PartialView(new Crm_Lead());
        }

        public ActionResult RatingTypePartial()
        {
            return PartialView(new Crm_Lead());
        }

        public ActionResult AssignToPartial()
        {
            return PartialView(new Crm_Lead());
        }

        public ActionResult LoadOnDemandPartial()
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            Thread.Sleep(1000);
            return PartialView("LoadOnDemandPartial");
        }

        [HttpPost]
        public void GetSelectedValuesFromGrid(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            ViewData["_selectedIDs"] = selectedIDsHF;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="selectedIDsHF">The selected i ds hf.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BulkDelete(string selectedIDsHF)
        {
            var result = selectedIDsHF.Split(',');
            if (result[0] == "Delete")
            {

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    _repository.DeleteLead(result[i]);
                }
            }

            return View("Leads");
        }

        [HttpGet]
        public ActionResult ExternalEditFormEdit(int LeadId)
        {
            model = _repository.GetAllLead();


            var crmLead = model.FirstOrDefault();
            if (crmLead != null)
            {
                var leadSources = crmLead.Crm_LeadSource;
                ViewData["Crm_LeadSource"] = leadSources;
                Session["Crm_LeadSource"] = leadSources;

                var leadStatu = crmLead.Crm_LeadStatu;
                ViewData["Crm_LeadStatu"] = leadStatu;
                Session["Crm_LeadStatu"] = leadStatu;

                var industry = crmLead.This_Industry;
                ViewData["This_Industry"] = industry;
                Session["This_Industry"] = industry;
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("LeadsNewForm", model.FirstOrDefault(l => l.LeadId == LeadId));
        }
    }
}
