﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CRM.Business.Security;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Security;
using CRM.Data.Repository;
using CRM.Data.Repository.CrmRepository;
using CRM.Data.Repository.UserRepository;
using CRM.Models;
using Newtonsoft.Json;

namespace CRM.Controllers
{
    public class LoginController : Controller
    {
        private IUserRepository _repository;

        public LoginController(IUserRepository repository)
        {
            _repository = repository;
        }
        // GET: Login
        //[Route("Login/Index")]
        public ActionResult Index()
        
        
        {
            return View();
        }

        /// <summary>
        /// Registers this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// Logins the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<RedirectToRouteResult> Login(LoginModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Index", "Login");
                }

                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["host"]+"/token");

                request.Method = "POST";
                string postData = "grant_type=password&username=" + model.UserName + "&password=" + model.Password;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse response = request.GetResponse();

                dataStream = response.GetResponseStream();
                var result = DeserializeFromStream(dataStream);

                dataStream.Close();
                response.Close();

                if (!string.IsNullOrEmpty(result.AccessToken))
                {
                    FormsAuthentication.SetAuthCookie(result.AccessToken, false); //FormsAuthentication.SetAuthCookie(result.AccessToken, false);
                    return RedirectToAction("Test", "Campaign");
                }
                return RedirectToAction("Index", "Login");
            }
            catch (Exception ex)
            {

                return RedirectToAction("Index", "Login");
            }            
            
        }

        /// <summary>
        /// Deserializes from stream.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static TokenDto DeserializeFromStream(Stream stream)
        {
            var serializer = new JsonSerializer();

            using (var sr = new StreamReader(stream))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                return serializer.Deserialize<TokenDto>(jsonTextReader);
            }
        }

        /// <summary>
        /// Registers the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            using (var client = new HttpClient())
            {
                // New code:
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["host"]+"/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //var member = new Members_Member() { AllowedSections = 127, CreationTime = DateTime.Now, CustomerId = 0, Email = "admin@company.com", FirstName = "test", IsGroup = true, L = 1, LoginName = "new test", MailEvents = 0, ParentId = 2, PassRetries = 0, Permissions = -1, R = 134, Status = 1, Type = 0, UseParentPermission = false,UserGender = 0,VisitsCount = 0,VisualParams = 5};
                var member = new UserModel { UserName = model.FirstName, Password = model.Password, ConfirmPassword = model.ConfirmPassword,EMail = model.EMail,FirstName = model.FirstName,LastName = model.LastName};
                var response = await client.PostAsJsonAsync("api/Account/Register", member);
                if (response.IsSuccessStatusCode)
                {
                    // Get the URI of the created resource.
                    Uri gizmoUrl = response.Headers.Location;
                }
            }
            return RedirectToAction("Index", "Login");
        }
    }
}