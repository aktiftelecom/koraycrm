﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Caching;
using System.Web.Mvc;
using CRM.Data.Repository.QuoteRepository;
using DevExpress.Web.Mvc;
using CRM.Core.Entities.Opportunity;
using System.Threading;

namespace CRM.Controllers
{
    public class OpportunityController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IOpportunityRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Crm_Opportunity> model;

        private Cache cache;
        #endregion

        public OpportunityController(IOpportunityRepository repository)
        {
            this._repository = repository;
            cache = new Cache();
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Opportunities()
        {
            return View();
        }

        public ActionResult OpportunitiesNewForm()
        {
            model = _repository.GetOpportunity();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Opportunity());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetOpportunity();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Opportunity());
        }

        public ActionResult TabPage()
        {
            model = _repository.GetOpportunity();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Opportunity());
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetOpportunity();
            System.Web.HttpContext.Current.Cache["Opportunity"] = model;

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_OpportunityGridViewPartial", model.OrderByDescending(x => x.OpportunityId).ToList());
        }

        [ValidateInput(false)]
        public ActionResult GridViewPartialCallback()
        {
            string createPermissions;
            Crm_Opportunity firstOrDefault;

            if (System.Web.HttpContext.Current.Cache["Opportunity"] != null)
            {
                model = (IQueryable<Crm_Opportunity>)System.Web.HttpContext.Current.Cache["Opportunity"];

                createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
                firstOrDefault = model.FirstOrDefault();
                ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

                if (model == null)
                {
                    return RedirectToAction("Index", "Login");
                }

                return PartialView("_OpportunityGridViewPartial", model.OrderByDescending(x => x.OpportunityId).ToList());
            }
            model = _repository.GetOpportunity();

            createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_OpportunityGridViewPartial", model.OrderByDescending(x => x.OpportunityId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Opportunity param)
         {
             
             param.IsDeleted = false;

            _repository.InsertOpportunity(param);

            return View("Opportunities");

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Opportunity param)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                var opportunities = _repository.GetOpportunity();

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    var opportunity = opportunities.FirstOrDefault(l => l.OpportunityId == int.Parse(result[i]));
                    if (opportunity != null)
                    {
                        opportunity.AssignedTo = param.AssignedTo != 0 ? param.AssignedTo : opportunity.AssignedTo;
                        opportunity.Description = param.Description ?? opportunity.Description;
                        opportunity.LeadSourceId = param.LeadSourceId;
                        opportunity.PrimaryEmail = param.PrimaryEmail ?? opportunity.PrimaryEmail;
                        opportunity.Amount = param.Amount != 0 ? param.Amount : opportunity.Amount;
                        opportunity.CampaignId = param.CampaignId != 0 ? param.CampaignId : opportunity.CampaignId;
                        opportunity.ContactId = param.ContactId != 0 ? param.ContactId : opportunity.ContactId;
                        opportunity.CustomerId = param.CustomerId != 0 ? param.CustomerId : opportunity.CustomerId;
                        opportunity.ExpectedCloseDate = param.ExpectedCloseDate.Equals(DateTime.MinValue) ? opportunity.ExpectedCloseDate : param.ExpectedCloseDate;
                        opportunity.ForecastAmount = param.ForecastAmount != 0
                            ? param.ForecastAmount
                            : opportunity.ForecastAmount;
                        opportunity.LeadSourceId = param.LeadSourceId != 0
                            ? param.LeadSourceId
                            : opportunity.LeadSourceId;
                        opportunity.NextStep = param.NextStep != string.Empty ? param.NextStep : opportunity.NextStep;
                        opportunity.Probability = param.Probability != 0 ? param.Probability : opportunity.Probability;
                        opportunity.SaleStageId = param.SaleStageId != 0 ? param.SaleStageId : opportunity.SaleStageId;
                        opportunity.Type = param.Type != 0 ? param.Type : opportunity.Type;
                        opportunity.UpdatedTime = DateTime.Now;
                        opportunity.Name = param.Name != string.Empty ? param.Name : opportunity.Name;

                        _repository.UpdateOpportunity(opportunity);
                    }
                }
            }

            return View("Opportunities");

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Opportunity param)
        {
            if (param.OpportunityId > 0)
            {
                try
                {
                   

                    param.IsDeleted = false;
                    _repository.UpdateOpportunity(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetOpportunity();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_OpportunityGridViewPartial", model.OrderByDescending(x => x.OpportunityId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="QuoteId">The quote identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GridViewPartialDelete(string OpportunityId)
        {
            _repository.DeleteOpportunity(OpportunityId);

            return View("Opportunities");
        }

        /// <summary>
        /// Contacts the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CustomerPartial()
        {
            return PartialView(new Crm_Opportunity());
        }

        /// <summary>
        /// Contacts the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult ContactPartial()
        {
            return PartialView(new Crm_Opportunity());
        }

        /// <summary>
        /// Stages the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult SaleStagePartial()
        {
            return PartialView(new Crm_Opportunity());
        }

        /// <summary>
        /// Leads the source partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult LeadSourcePartial()
        {
            return PartialView(new Crm_Opportunity());
        }

        /// <summary>
        /// Opportinities the type partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult OpportinityTypePartial()
        {
            return PartialView(new Crm_Opportunity());
        }

        /// <summary>
        /// Campaigns the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CampaignPartial()
        {
            return PartialView(new Crm_Opportunity());
        }

        public ActionResult AssignToPartial()
        {
            return PartialView(new Crm_Opportunity());

        }

        /// <summary>
        /// Loads the on demand partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult LoadOnDemandPartial()
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            Thread.Sleep(1000);
            return PartialView("LoadOnDemandPartial");
        }

        [HttpPost]
        public void GetSelectedValuesFromGrid(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            ViewData["_selectedIDs"] = selectedIDsHF;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="selectedIDsHF">The selected i ds hf.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BulkDelete(string selectedIDsHF)
        {
            System.Web.HttpContext.Current.Cache.Remove("Opportunity");
            var result = selectedIDsHF.Split(',');
            if (result[0] == "Delete")
            {

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    _repository.DeleteOpportunity(result[i]);
                }
            }

            return View("Opportunities");
        }

        [HttpGet]
        public ActionResult ExternalEditFormEdit(int OpportunityId)
        {
            model = _repository.GetOpportunity();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("OpportunitiesNewForm", model.FirstOrDefault(l => l.OpportunityId == OpportunityId));
        }
    }
}
