﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.Data.Repository.OrganizationRepository;
using DevExpress.Web.Mvc;
using CRM.Data.Repository.ContactRepository;

namespace CRM.Controllers
{
    public class OrganizationController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IOrganizationRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Crm_Organization> model;
        #endregion

        public OrganizationController(IOrganizationRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Organizations()
        {
            return View();
        }

        public ActionResult OrganizationsNewForm()
        {
            model = _repository.GetOrganization();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Organization());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetOrganization();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Organization());
        }

        public ActionResult TabPage()
        {
            model = _repository.GetOrganization();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Organization());
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetOrganization();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_OrganizationGridViewPartial", model.OrderByDescending(x => x.Id).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Organization param)
         {
             
             param.IsDeleted = false;

            _repository.InsertOrganization(param);

            return View("Organizations");

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Organization param)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                var organizations = _repository.GetOrganization();

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    var organization = organizations.FirstOrDefault(l => l.Id == int.Parse(result[i]));
                    if (organization != null)
                    {
                        organization.AnnualRevenue = param.AnnualRevenue ?? organization.AnnualRevenue;
                        organization.AssignedTo = param.AssignedTo != 0
                            ? param.AssignedTo
                            : organization.AssignedTo;
                        organization.BillingAddress = param.BillingAddress != string.Empty
                            ? param.BillingAddress
                            : organization.BillingAddress;
                        organization.BillingCity = param.BillingCity != 0
                            ? param.BillingCity
                            : organization.BillingCity;
                        organization.BillingCountry = param.BillingCountry != string.Empty
                            ? param.BillingCountry
                            : organization.BillingCountry;
                        organization.BillingPOBox = param.BillingPOBox != string.Empty
                            ? param.BillingPOBox
                            : organization.BillingPOBox;
                        organization.BillingPostalCode = param.BillingPostalCode != string.Empty
                            ? param.BillingPostalCode
                            : organization.BillingPostalCode;
                        organization.BillingState = param.BillingState != 0
                            ? param.BillingState
                            : organization.BillingState;
                        organization.Description = param.Description != string.Empty
                            ? param.Description
                            : organization.Description;
                        organization.EmailOptOut = param.EmailOptOut;
                        organization.Employees = param.Employees ?? organization.Employees;
                        organization.Fax = param.Fax != string.Empty
                            ? param.Fax
                            : organization.Fax;
                        organization.EmailOptOut = param.EmailOptOut;
                        organization.Industry = param.Industry ?? organization.Industry;
                        organization.MainOrganizationId = param.MainOrganizationId != 0
                            ? param.MainOrganizationId
                            : organization.MainOrganizationId;
                        organization.MemberOf = param.MemberOf ?? organization.MemberOf;
                        organization.NotifyOwner = param.NotifyOwner;
                        organization.OrganizationName = param.OrganizationName != string.Empty
                            ? param.OrganizationName
                            : organization.OrganizationName;
                        organization.Ownership = param.Ownership != string.Empty
                            ? param.Ownership
                            : organization.Ownership;
                        organization.PrimaryEmail = param.PrimaryEmail != string.Empty
                            ? param.PrimaryEmail
                            : organization.PrimaryEmail;
                        organization.PrimaryPhone = param.PrimaryPhone != string.Empty
                            ? param.PrimaryPhone
                            : organization.PrimaryPhone;
                        organization.Rating = param.Rating ?? organization.Rating;
                        organization.SICCode = param.SICCode != string.Empty
                            ? param.SICCode
                            : organization.SICCode;
                        organization.SecondaryEmail = param.SecondaryEmail != string.Empty
                            ? param.SecondaryEmail
                            : organization.SecondaryEmail;
                        organization.SecondaryPhone = param.SecondaryPhone != string.Empty
                            ? param.SecondaryPhone
                            : organization.SecondaryPhone;
                        organization.ShippingAddress = param.ShippingAddress != string.Empty
                            ? param.ShippingAddress
                            : organization.ShippingAddress;
                        organization.ShippingCity = param.ShippingCity != 0
                           ? param.ShippingCity
                           : organization.ShippingCity;
                        organization.ShippingCountry = param.ShippingCountry != string.Empty
                           ? param.ShippingCountry
                           : organization.ShippingCountry;
                        organization.ShippingPOBox = param.ShippingPOBox != string.Empty
                           ? param.ShippingPOBox
                           : organization.ShippingPOBox;
                        organization.ShippingPostalCode = param.ShippingPostalCode != string.Empty
                           ? param.ShippingPostalCode
                           : organization.ShippingPostalCode;
                        organization.ShippingState = param.ShippingState != 0
                           ? param.ShippingState
                           : organization.ShippingState;
                        organization.TickerSymbol = param.TickerSymbol != string.Empty
                           ? param.TickerSymbol
                           : organization.TickerSymbol;
                        organization.Type = param.Type ?? organization.Type;
                        organization.UpdatedTime = DateTime.Now;
                        organization.Website = param.Website != string.Empty
                           ? param.Website
                           : organization.Website;
                        organization.EmailOptOut = param.EmailOptOut;

                        _repository.UpdateOrganization(organization);
                    }
                }
            }

            return View("Organizations");

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Organization param)
        {
            if (param.Id > 0)
            {
                try
                {
                    param.IsDeleted = false;
                    _repository.UpdateOrganization(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetOrganization();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_OrganizationGridViewPartial", model.OrderByDescending(x => x.Id).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
         [HttpGet]
        public ActionResult GridViewPartialDelete(string Id)
        {
            _repository.DeleteOrganization(Id);

            return View("Organizations");
        }

        /// <summary>
        /// Contacts the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult IndustryPartial()
        {
            return PartialView(new Crm_Organization());
        }

        /// <summary>
        /// Organizations the type partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult TypePartial()
        {
            return PartialView(new Crm_Organization());
        }

        /// <summary>
        /// Countries the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CountryPartial()
        {
            return PartialView(new Crm_Organization());
        }
        /// <summary>
        /// Cities the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CityPartial()
        {
            int country = (Request.Params["BillingCity"] != null) ? int.Parse(Request.Params["BillingCity"]) : -1;
            return PartialView(new Crm_Organization { BillingCity = country });
        }

        /// <summary>
        /// Shippings the country partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult ShippingCountryPartial()
        {
            return PartialView(new Crm_Organization());
        }
        /// <summary>
        /// Shippings the city partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult ShippingCityPartial()
        {
            int country = (Request.Params["ShippingCity"] != null) ? int.Parse(Request.Params["ShippingCity"]) : -1;
            return PartialView(new Crm_Organization { ShippingCity = country });
        }

        public ActionResult AssignToPartial()
        {
            return PartialView(new Crm_Organization());

        }

        /// <summary>
        /// Loads the on demand partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult LoadOnDemandPartial()
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            Thread.Sleep(1000);
            return PartialView("LoadOnDemandPartial");
        }

        [HttpPost]
        public void GetSelectedValuesFromGrid(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            ViewData["_selectedIDs"] = selectedIDsHF;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="selectedIDsHF">The selected i ds hf.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BulkDelete(string selectedIDsHF)
        {
            System.Web.HttpContext.Current.Cache.Remove("Opportunity");
            var result = selectedIDsHF.Split(',');
            if (result[0] == "Delete")
            {

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    _repository.DeleteOrganization(result[i]);
                }
            }

            return View("Organizations");
        }

        [HttpGet]
        public ActionResult ExternalEditFormEdit(int Id)
        {
            model = _repository.GetOrganization();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("OrganizationsNewForm", model.FirstOrDefault(l => l.Id == Id));
        }

    }
}
