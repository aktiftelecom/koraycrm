﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Entities.Service;
using CRM.Data.Repository.PriceBookRepository;
using CRM.Data.Repository.ServiceRepository;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class PriceBookController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IPriceBookRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Crm_PriceBook> model;
        #endregion

        public PriceBookController(IPriceBookRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult PriceBooks()
        {
            return View();
        }

        public ActionResult PriceBooksNewForm()
        {
            model = _repository.GetPriceBook();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_PriceBook());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetPriceBook();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_PriceBook());
        }

        public ActionResult TabPage()
        {
            model = _repository.GetPriceBook();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_PriceBook());
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetPriceBook();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_PriceBookGridViewPartial", model.OrderByDescending(x => x.PriceBookId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_PriceBook param)
         {
             
             param.Status = true;

            _repository.SavePriceBook(param);

            return View("PriceBooks");

        }

        /// <summary>
        /// Bulks the edit.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_PriceBook param)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                var priceBooks = _repository.GetPriceBook();

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    var priceBook = priceBooks.FirstOrDefault(l => l.PriceBookId == int.Parse(result[i]));
                    if (priceBook != null)
                    {
                        priceBook.PriceBookName = param.PriceBookName != string.Empty
                            ? param.PriceBookName
                            : priceBook.PriceBookName;
                        priceBook.Status = param.Status;
                        priceBook.UpdatedTime = DateTime.Now;
                        priceBook.CurrencyId = param.CurrencyId;
                        priceBook.Description = param.Description != string.Empty
                            ? param.Description
                            : priceBook.Description;

                        this._repository.UpdatePriceBook(priceBook);
                    }
                }
            }

            return View("PriceBooks");

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_PriceBook param)
        {
            if (param.PriceBookId > 0)
            {
                try
                {
                    param.Status = true;
                    _repository.UpdatePriceBook(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetPriceBook();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_PriceBookGridViewPartial", model.OrderByDescending(x => x.PriceBookId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GridViewPartialDelete(string PriceBookId)
        {
            _repository.DeletePriceBook(PriceBookId);

            return View("PriceBooks");
        }

        /// <summary>
        /// Cities the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CurrencyPartial()
        {
            return PartialView(new Crm_PriceBook());
        }

        public ActionResult LoadOnDemandPartial()
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            Thread.Sleep(1000);
            return PartialView("LoadOnDemandPartial");
        }

        [HttpPost]
        public void GetSelectedValuesFromGrid(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            ViewData["_selectedIDs"] = selectedIDsHF;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="selectedIDsHF">The selected i ds hf.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BulkDelete(string selectedIDsHF)
        {
            var result = selectedIDsHF.Split(',');
            if (result[0] == "Delete")
            {

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    _repository.DeletePriceBook(result[i]);
                }
            }

            return View("PriceBooks");
        }

        [HttpGet]
        public ActionResult ExternalEditFormEdit(int PriceBookId)
        {
            model = _repository.GetPriceBook();


            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("PriceBooksNewForm", model.FirstOrDefault(l => l.PriceBookId == PriceBookId));
        }
    }
}
