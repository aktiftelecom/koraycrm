﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.City;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.Manufacturer;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Product;
using CRM.Core.Entities.Unit;
using CRM.Core.Entities.Vendor;
using CRM.Data.Repository.CrmRepository;
using CRM.Data.Repository.CustomerRepository;
using CRM.Data.Repository.ProductRepository;
using DevExpress.Office.Utils;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class ProductController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IProductRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Crm_Product> model;
        #endregion

        public ProductController(IProductRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Products()
        {
            return View();
        }

        public ActionResult ProductsNewForm()
        {
            model = _repository.GetAllProduct();


            var crmProduct = model.FirstOrDefault();
            if (crmProduct != null)
            {
                var categories = crmProduct.Categories;
                ViewData["Categories"] = categories;

                var manufacturers = crmProduct.Manufacturers;
                ViewData["Manufacturers"] = manufacturers;
                Session["Manufacturers"] = manufacturers;

                var vendors = crmProduct.Vendors;
                ViewData["Vendors"] = vendors;
                Session["Vendors"] = vendors;

                var currencies = crmProduct.Currencies;
                ViewData["Currencies"] = currencies;
                Session["Currencies"] = currencies;

                var units = crmProduct.Units;
                ViewData["Units"] = units;
                Session["Units"] = units;

                foreach (var item in model)
                {
                    var category = categories.FirstOrDefault(t => t.Item1 == item.Category);
                    if (category != null)
                    {
                        item.CategoryNameId = category.Item2;
                    }

                    var manufacturer = manufacturers.FirstOrDefault(t => t.ManufacturerId == item.ManufacturerId);
                    if (manufacturer != null)
                    {
                        item.ManufacturerNameId = manufacturer.Description;
                    }

                    var vendor = vendors.FirstOrDefault(c => c.VendorId == item.VendorId);
                    if (vendor != null)
                    {
                        item.VendorNameId = vendor.Name;
                    }

                    var currency = currencies.FirstOrDefault(c => c.CurrencyId == item.CurrencyId);
                    if (currency != null)
                    {
                        item.CurrencyNameId = currency.Name;
                    }

                    var unit = units.FirstOrDefault(c => c.UnitId == item.UsageUnitId);
                    if (unit != null)
                    {
                        item.UnitNameId = unit.Abbreviation;
                    }
                }

            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Product());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetAllProduct();


            var crmProduct = model.FirstOrDefault();
            if (crmProduct != null)
            {
                var categories = crmProduct.Categories;
                ViewData["Categories"] = categories;

                var manufacturers = crmProduct.Manufacturers;
                ViewData["Manufacturers"] = manufacturers;
                Session["Manufacturers"] = manufacturers;

                var vendors = crmProduct.Vendors;
                ViewData["Vendors"] = vendors;
                Session["Vendors"] = vendors;

                var currencies = crmProduct.Currencies;
                ViewData["Currencies"] = currencies;
                Session["Currencies"] = currencies;

                var units = crmProduct.Units;
                ViewData["Units"] = units;
                Session["Units"] = units;

                foreach (var item in model)
                {
                    var category = categories.FirstOrDefault(t => t.Item1 == item.Category);
                    if (category != null)
                    {
                        item.CategoryNameId = category.Item2;
                    }

                    var manufacturer = manufacturers.FirstOrDefault(t => t.ManufacturerId == item.ManufacturerId);
                    if (manufacturer != null)
                    {
                        item.ManufacturerNameId = manufacturer.Description;
                    }

                    var vendor = vendors.FirstOrDefault(c => c.VendorId == item.VendorId);
                    if (vendor != null)
                    {
                        item.VendorNameId = vendor.Name;
                    }

                    var currency = currencies.FirstOrDefault(c => c.CurrencyId == item.CurrencyId);
                    if (currency != null)
                    {
                        item.CurrencyNameId = currency.Name;
                    }

                    var unit = units.FirstOrDefault(c => c.UnitId == item.UsageUnitId);
                    if (unit != null)
                    {
                        item.UnitNameId = unit.Abbreviation;
                    }
                }
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Product());
        }

        public ActionResult TabPage()
        {
            model = _repository.GetAllProduct();


            var crmProduct = model.FirstOrDefault();
            if (crmProduct != null)
            {
                var categories = crmProduct.Categories;
                ViewData["Categories"] = categories;

                var manufacturers = crmProduct.Manufacturers;
                ViewData["Manufacturers"] = manufacturers;
                Session["Manufacturers"] = manufacturers;

                var vendors = crmProduct.Vendors;
                ViewData["Vendors"] = vendors;
                Session["Vendors"] = vendors;

                var currencies = crmProduct.Currencies;
                ViewData["Currencies"] = currencies;
                Session["Currencies"] = currencies;

                var units = crmProduct.Units;
                ViewData["Units"] = units;
                Session["Units"] = units;

                foreach (var item in model)
                {
                    var category = categories.FirstOrDefault(t => t.Item1 == item.Category);
                    if (category != null)
                    {
                        item.CategoryNameId = category.Item2;
                    }

                    var manufacturer = manufacturers.FirstOrDefault(t => t.ManufacturerId == item.ManufacturerId);
                    if (manufacturer != null)
                    {
                        item.ManufacturerNameId = manufacturer.Description;
                    }

                    var vendor = vendors.FirstOrDefault(c => c.VendorId == item.VendorId);
                    if (vendor != null)
                    {
                        item.VendorNameId = vendor.Name;
                    }

                    var currency = currencies.FirstOrDefault(c => c.CurrencyId == item.CurrencyId);
                    if (currency != null)
                    {
                        item.CurrencyNameId = currency.Name;
                    }

                    var unit = units.FirstOrDefault(c => c.UnitId == item.UsageUnitId);
                    if (unit != null)
                    {
                        item.UnitNameId = unit.Abbreviation;
                    }
                }
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Product());
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetAllProduct();

            
            var crmProduct = model.FirstOrDefault();
            if (crmProduct != null)
            {
                var categories = crmProduct.Categories;
                ViewData["Categories"] = categories;

                var manufacturers = crmProduct.Manufacturers;
                ViewData["Manufacturers"] = manufacturers;
                Session["Manufacturers"] = manufacturers;

                var vendors = crmProduct.Vendors;
                ViewData["Vendors"] = vendors;
                Session["Vendors"] = vendors;

                var currencies = crmProduct.Currencies;
                ViewData["Currencies"] = currencies;
                Session["Currencies"] = currencies;

                var units = crmProduct.Units;
                ViewData["Units"] = units;
                Session["Units"] = units;

                foreach (var item in model)
                {
                    var category = categories.FirstOrDefault(t => t.Item1 == item.Category);
                    if (category != null)
                    {
                        item.CategoryNameId = category.Item2;
                    }

                    var manufacturer = manufacturers.FirstOrDefault(t => t.ManufacturerId == item.ManufacturerId);
                    if (manufacturer != null)
                    {
                        item.ManufacturerNameId = manufacturer.Description;
                    }

                    var vendor = vendors.FirstOrDefault(c => c.VendorId == item.VendorId);
                    if (vendor != null)
                    {
                        item.VendorNameId = vendor.Name;
                    }

                    var currency = currencies.FirstOrDefault(c => c.CurrencyId == item.CurrencyId);
                    if (currency != null)
                    {
                        item.CurrencyNameId = currency.Name;
                    }

                    var unit = units.FirstOrDefault(c => c.UnitId == item.UsageUnitId);
                    if (unit != null)
                    {
                        item.UnitNameId = unit.Abbreviation;
                    }
                }
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_ProductGridViewPartial", model.OrderByDescending(x => x.ProductId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Product param, IEnumerable<UploadedFile> ucMultiSelection)
         {
             var product = _repository.GetAllProduct().FirstOrDefault();
             var category = product.Categories.FirstOrDefault(
                     x => x.Item2 == param.CategoryNameId);
             if (category != null)
                 param.Category =
                     category.Item1;
             else
             {
                 param.Category = param.CategoryNameId == null ? 1 : int.Parse(param.CategoryNameId);
             }

             var manufacturer = product.Manufacturers.FirstOrDefault(
                      x => x.Description == param.ManufacturerNameId);
             if (manufacturer != null)
                 param.ManufacturerId =
                     manufacturer.ManufacturerId;
             else
             {
                 param.ManufacturerId = param.ManufacturerNameId == null ? 1 : int.Parse(param.ManufacturerNameId);
             }

             var vendor = product.Vendors.FirstOrDefault(
                       x => x.Name == param.VendorNameId);
             if (vendor != null)
                 param.VendorId =
                     vendor.VendorId;
             else
             {
                 param.VendorId = param.VendorNameId == null ? 1 : int.Parse(param.VendorNameId);
             }

             var unit = product.Units.FirstOrDefault(
                        x => x.Text == param.UnitNameId);
             if (unit != null)
                 param.UsageUnitId =
                     unit.UnitId;
             else
             {
                 param.UsageUnitId = param.UnitNameId == null ? 1 : int.Parse(param.UnitNameId);
             }
            
             param.IsActive = true;
             var firstOrDefault = ucMultiSelection.FirstOrDefault();
             if (firstOrDefault != null)
                 param.ProductImage = firstOrDefault.FileBytes;

             _repository.SaveProduct(param);

            return View("Products");

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Product param, IEnumerable<UploadedFile> ucMultiSelection)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                var products = _repository.GetAllProduct();

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    var product = products.FirstOrDefault(l => l.ProductId == int.Parse(result[i]));
                    if (product != null)
                    {
                        var firstProduct = products.FirstOrDefault();
                        var category = firstProduct.Categories.FirstOrDefault(
                                x => x.Item2 == param.CategoryNameId);
                        if (category != null)
                            product.Category =
                                category.Item1;
                        else
                        {
                            product.Category = param.CategoryNameId == null ? 1 : int.Parse(param.CategoryNameId);
                        }

                        var manufacturer = firstProduct.Manufacturers.FirstOrDefault(
                                 x => x.Description == param.ManufacturerNameId);
                        if (manufacturer != null)
                            product.ManufacturerId =
                                manufacturer.ManufacturerId;
                        else
                        {
                            product.ManufacturerId = param.ManufacturerNameId == null ? 1 : int.Parse(param.ManufacturerNameId);
                        }

                        var vendor = firstProduct.Vendors.FirstOrDefault(
                                  x => x.Name == param.VendorNameId);
                        if (vendor != null)
                            product.VendorId =
                                vendor.VendorId;
                        else
                        {
                            product.VendorId = param.VendorNameId == null ? 1 : int.Parse(param.VendorNameId);
                        }

                        var unit = firstProduct.Units.FirstOrDefault(
                                   x => x.Text == param.UnitNameId);
                        if (unit != null)
                            product.UsageUnitId =
                                unit.UnitId;
                        else
                        {
                            product.UsageUnitId = param.UnitNameId == null ? 1 : int.Parse(param.UnitNameId);
                        }

                        product.CommissionRate = param.CommissionRate != 0
                            ? param.CommissionRate
                            : product.CommissionRate;
                        product.CurrencyId = param.CurrencyId;
                        product.Description = param.Description != string.Empty
                            ? param.Description
                            : product.Description;
                        product.GLAccount = param.GLAccount;
                        product.Handler = param.Handler;
                        product.IsActive = param.IsActive;
                        product.ManufacturPartNo = param.ManufacturPartNo != string.Empty
                            ? param.ManufacturPartNo
                            : product.ManufacturPartNo;
                        product.Name = param.Name != string.Empty
                            ? param.Name
                            : product.Name;
                        product.PartNo = param.PartNo != string.Empty
                            ? param.PartNo
                            : product.PartNo;
                        product.PercentSales = param.PercentSales != 0
                            ? param.PercentSales
                            : product.PercentSales;
                        product.PercentService = param.PercentService != 0
                            ? param.PercentService
                            : product.PercentService;
                        product.PercentVAT = param.PercentVAT != 0
                            ? param.PercentVAT
                            : product.PercentVAT;
                        product.ProductId = param.ProductId;
                        product.ProductImage = param.ProductImage;
                        product.ProductPage = param.ProductPage != string.Empty
                            ? param.ProductPage
                            : product.ProductPage;
                        product.PurchaseCost = param.PurchaseCost != 0
                            ? param.PurchaseCost
                            : product.PurchaseCost;
                        product.QuantityInDemand = param.QuantityInDemand != 0
                            ? param.QuantityInDemand
                            : product.QuantityInDemand;
                        product.QuantityInStock = param.QuantityInStock != 0
                            ? param.QuantityInStock
                            : product.QuantityInStock;
                        product.QuantityPerUnit = param.QuantityPerUnit != 0
                            ? param.QuantityPerUnit
                            : product.QuantityPerUnit;
                        product.ReorderLevel = param.ReorderLevel != 0
                            ? param.ReorderLevel
                            : product.ReorderLevel;
                        product.SalesEndDate = param.SalesEndDate;
                        product.SalesStartDate = param.SalesStartDate;
                        product.SerialNo = param.SerialNo != string.Empty
                            ? param.SerialNo
                            : product.SerialNo;
                        product.SupportExpiryDate = param.SupportExpiryDate;
                        product.SupportStartDate = param.SupportStartDate;
                        product.UnitPrice = param.UnitPrice != 0
                            ? param.UnitPrice
                            : product.UnitPrice;
                        product.VendorPartNo = param.VendorPartNo != string.Empty
                            ? param.VendorPartNo
                            : product.VendorPartNo;
                        product.Website = param.Website != string.Empty
                            ? param.Website
                            : product.Website;
                        product.ProductImage = ucMultiSelection.FirstOrDefault().FileBytes;

                        this._repository.UpdateProduct(product);
                    }
                }
            }

            return View("Products");

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Product param)
        {
            if (param.ProductId > 0)
            {
                try
                {
                    var category =
                        param.Categories.FirstOrDefault(t => t.Item2 == param.CategoryNameId);
                    if (category != null)
                    {
                        param.Category = category.Item1;
                    }
                    else if (!string.IsNullOrEmpty(param.CategoryNameId))
                    {
                        param.Category = int.Parse(param.CategoryNameId);
                    }

                    var manufacturer =
                        ((List<Crm_Manufacturer>)Session["Manufacturers"]).FirstOrDefault(t => t.Description == param.ManufacturerNameId);
                    if (manufacturer != null)
                    {
                        param.ManufacturerId = manufacturer.ManufacturerId;
                    }
                    else if (!string.IsNullOrEmpty(param.ManufacturerNameId))
                    {
                        param.ManufacturerId = int.Parse(param.ManufacturerNameId);
                    }

                    var vendor =
                        ((List<Crm_Vendor>)Session["Vendors"]).FirstOrDefault(t => t.Name == param.VendorNameId);
                    if (vendor != null)
                    {
                        param.VendorId = vendor.VendorId;
                    }
                    else if (!string.IsNullOrEmpty(param.VendorNameId))
                    {
                        param.VendorId = int.Parse(param.VendorNameId);
                    }

                    var currency =
                        ((List<This_Currency>)Session["Currencies"]).FirstOrDefault(t => t.Name == param.CurrencyNameId);
                    if (currency != null)
                    {
                        param.CurrencyId = currency.CurrencyId;
                    }
                    else if (!string.IsNullOrEmpty(param.CurrencyNameId))
                    {
                        param.CurrencyId = int.Parse(param.CurrencyNameId);
                    }

                    var unit =
                        ((List<This_Unit>)Session["Units"]).FirstOrDefault(t => t.Abbreviation == param.UnitNameId);
                    if (unit != null)
                    {
                        param.UsageUnitId = unit.UnitId;
                    }
                    else if (!string.IsNullOrEmpty(param.UnitNameId))
                    {
                        param.UsageUnitId = int.Parse(param.UnitNameId);
                    }

                    param.IsActive = true;
                    _repository.UpdateProduct(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetAllProduct();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_ProductGridViewPartial", model.OrderByDescending(x => x.ProductId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="ProductId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GridViewPartialDelete(string ProductId)
        {
            _repository.DeleteProduct(ProductId);

            return View("Products");
        }

        public ActionResult LoadOnDemandPartial()
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            Thread.Sleep(1000);
            return PartialView("LoadOnDemandPartial");
        }

        [HttpPost]
        public void GetSelectedValuesFromGrid(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            ViewData["_selectedIDs"] = selectedIDsHF;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="selectedIDsHF">The selected i ds hf.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BulkDelete(string selectedIDsHF)
        {
            var result = selectedIDsHF.Split(',');
            if (result[0] == "Delete")
            {

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    _repository.DeleteProduct(result[i]);
                }
            }

            return View("Products");
        }

        [HttpGet]
        public ActionResult ExternalEditFormEdit(int ProductId)
        {
            model = _repository.GetAllProduct();


            var crmProduct = model.FirstOrDefault();
            if (crmProduct != null)
            {
                var categories = crmProduct.Categories;
                ViewData["Categories"] = categories;

                var manufacturers = crmProduct.Manufacturers;
                ViewData["Manufacturers"] = manufacturers;
                Session["Manufacturers"] = manufacturers;

                var vendors = crmProduct.Vendors;
                ViewData["Vendors"] = vendors;
                Session["Vendors"] = vendors;

                var currencies = crmProduct.Currencies;
                ViewData["Currencies"] = currencies;
                Session["Currencies"] = currencies;

                var units = crmProduct.Units;
                ViewData["Units"] = units;
                Session["Units"] = units;

                foreach (var item in model)
                {
                    var category = categories.FirstOrDefault(t => t.Item1 == item.Category);
                    if (category != null)
                    {
                        item.CategoryNameId = category.Item2;
                    }

                    var manufacturer = manufacturers.FirstOrDefault(t => t.ManufacturerId == item.ManufacturerId);
                    if (manufacturer != null)
                    {
                        item.ManufacturerNameId = manufacturer.Description;
                    }

                    var vendor = vendors.FirstOrDefault(c => c.VendorId == item.VendorId);
                    if (vendor != null)
                    {
                        item.VendorNameId = vendor.Name;
                    }

                    var currency = currencies.FirstOrDefault(c => c.CurrencyId == item.CurrencyId);
                    if (currency != null)
                    {
                        item.CurrencyNameId = currency.Name;
                    }

                    var unit = units.FirstOrDefault(c => c.UnitId == item.UsageUnitId);
                    if (unit != null)
                    {
                        item.UnitNameId = unit.Abbreviation;
                    }
                }
            }

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            
            return View("ProductsNewForm", model.FirstOrDefault(l => l.ProductId == ProductId));
        }
    }
}
