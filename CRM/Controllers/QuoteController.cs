﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.City;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Manufacturer;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Product;
using CRM.Core.Entities.Quote;
using CRM.Core.Entities.Unit;
using CRM.Core.Entities.Vendor;
using CRM.Data.Repository.CrmRepository;
using CRM.Data.Repository.CustomerRepository;
using CRM.Data.Repository.LeadRepository;
using CRM.Data.Repository.ProductRepository;
using CRM.Data.Repository.QuoteRepository;
using DevExpress.Office.Utils;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class QuoteController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IQuoteRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Projects_Quote> model;
        #endregion

        public QuoteController(IQuoteRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Quotes()
        {
            return View();
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetQuote();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_QuoteGridViewPartial", model.OrderByDescending(x => x.QuoteId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Projects_Quote param)
         {
             
             param.IsDeleted = false;

            _repository.InsertQuote(param);

            var model = _repository.GetQuote();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_QuoteGridViewPartial", model.OrderByDescending(x => x.QuoteId).ToList());

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Projects_Quote param)
        {
            if (param.QuoteId > 0)
            {
                try
                {
                   

                    param.IsDeleted = false;
                    _repository.UpdateQuote(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetQuote();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_QuoteGridViewPartial", model.OrderByDescending(x => x.QuoteId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="QuoteId">The quote identifier.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(string QuoteId)
        {
            _repository.DeleteQuote(QuoteId);

            var allQuote = _repository.GetQuote();
            if (allQuote == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_QuoteGridViewPartial", allQuote.OrderByDescending(x => x.QuoteId).ToList());
        }

        /// <summary>
        /// Contacts the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult ContactPartial()
        {
            return PartialView(new Projects_Quote());
        }

        /// <summary>
        /// Leads the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult LeadPartial()
        {
            return PartialView(new Projects_Quote());
        }

        /// <summary>
        /// Projects the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult ProjectPartial()
        {
            return PartialView(new Projects_Quote());
        }

        /// <summary>
        /// Shippings the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult ShippingPartial()
        {
            return PartialView(new Projects_Quote());
        }

        /// <summary>
        /// Stages the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult StagePartial()
        {
            return PartialView(new Projects_Quote());
        }
    }
}
