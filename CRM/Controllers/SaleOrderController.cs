﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.SaleOrder;
using CRM.Data.Repository.OrganizationRepository;
using CRM.Data.Repository.SaleOrderRepository;
using DevExpress.Web.Mvc;
using CRM.Data.Repository.ContactRepository;

namespace CRM.Controllers
{
    public class SaleOrderController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private ISaleOrderRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Crm_SaleOrder> model;
        #endregion

        public SaleOrderController(ISaleOrderRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult SaleOrders()
        {
            return View();
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetSaleOrder();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_SaleOrderGridViewPartial", model.OrderByDescending(x => x.SaleOrderId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_SaleOrder param)
         {
             
             param.IsActive = true;

            _repository.InsertSaleOrder(param);

            var model = _repository.GetSaleOrder();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_SaleOrderGridViewPartial", model.OrderByDescending(x => x.SaleOrderId).ToList());

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_SaleOrder param)
        {
            if (param.SaleOrderId > 0)
            {
                try
                {
                    param.IsActive = true;
                    _repository.UpdateSaleOrder(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetSaleOrder();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_SaleOrderGridViewPartial", model.OrderByDescending(x => x.SaleOrderId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(string SaleOrderId)
        {
            _repository.DeleteSaleOrder(SaleOrderId);

            var allQuote = _repository.GetSaleOrder();
            if (allQuote == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_SaleOrderGridViewPartial", allQuote.OrderByDescending(x => x.SaleOrderId).ToList());
        }

        

        /// <summary>
        /// Countries the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CountryPartial()
        {
            return PartialView(new Crm_SaleOrder());
        }
        /// <summary>
        /// Cities the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CityPartial()
        {
            int country = (Request.Params["BillingCity"] != null) ? int.Parse(Request.Params["BillingCity"]) : -1;
            return PartialView(new Crm_SaleOrder { BillingCity = country });
        }

        /// <summary>
        /// Shippings the country partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult ShippingCountryPartial()
        {
            return PartialView(new Crm_SaleOrder());
        }
        /// <summary>
        /// Shippings the city partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult ShippingCityPartial()
        {
            int country = (Request.Params["ShippingCity"] != null) ? int.Parse(Request.Params["ShippingCity"]) : -1;
            return PartialView(new Crm_SaleOrder { ShippingCity = country });
        }

        public ActionResult ContactPartial()
        {
            return PartialView(new Crm_SaleOrder());
        }

        public ActionResult CurrencyPartial()
        {
            return PartialView(new Crm_SaleOrder());
        }

        public ActionResult CustomerPartial()
        {
            return PartialView(new Crm_SaleOrder());
        }

        public ActionResult OpportunityPartial()
        {
            return PartialView(new Crm_SaleOrder());
        }

        public ActionResult OrganizationPartial()
        {
            return PartialView(new Crm_SaleOrder());
        }

        public ActionResult TaxModeTypePartial()
        {
            return PartialView(new Crm_SaleOrder());
        }
    }
}
