﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using CRM.Core.Entities.Service;
using CRM.Data.Repository.ServiceRepository;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class ServiceController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IServiceRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Crm_Service> model;
        #endregion

        public ServiceController(IServiceRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Services()
        {
            return View();
        }

        public ActionResult ServicesNewForm()
        {
            model = _repository.GetService();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Service());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetService();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Service());
        }

        public ActionResult TabPage()
        {
             model = _repository.GetService();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Service());
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetService();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_ServiceGridViewPartial", model.OrderByDescending(x => x.ServiceId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Service param)
         {
             
             param.IsActive = true;

            _repository.InsertService(param);

            return View("Services");

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Service param)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                var services = _repository.GetService();

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    var service = services.FirstOrDefault(l => l.ServiceId == int.Parse(result[i]));
                    if (service != null)
                    {
                        service.CategoryId = param.CategoryId;
                        service.CommissionRate = param.CommissionRate != 0
                            ? param.CommissionRate
                            : service.CommissionRate;
                        service.CurrencyId = param.CurrencyId;
                        service.Description = param.Description != string.Empty
                            ? param.Description
                            : service.Description;
                        service.IsActive = param.IsActive;
                        service.Name = param.Name != string.Empty
                            ? param.Name
                            : service.Name;
                        service.NumberOfUnits = param.NumberOfUnits != 0
                            ? param.NumberOfUnits
                            : service.NumberOfUnits;
                        service.OwnerId = param.OwnerId;
                        service.PercentSales = param.PercentSales != 0
                            ? param.PercentSales
                            : service.PercentSales;
                        service.PercentService = param.PercentService != 0
                           ? param.PercentService
                           : service.PercentService;
                        service.PercentVAT = param.PercentVAT != 0
                           ? param.PercentVAT
                           : service.PercentVAT;
                        service.Price = param.Price != 0
                          ? param.Price
                          : service.Price;
                        service.PurchaseCost = param.PurchaseCost != 0
                          ? param.PurchaseCost
                          : service.PurchaseCost;
                        service.SalesEndDate = param.SalesEndDate;
                        service.SalesStartDate  = param.SalesStartDate;
                        service.SupportExpiryDate = param.SupportExpiryDate;
                        service.SupportStartDate = param.SupportStartDate;
                        service.UpdatedTime = DateTime.Now;
                        service.UsageUnitId = param.UsageUnitId;
                        service.Website = param.Website;

                        this._repository.UpdateService(service);
                    }
                }
            }

            return View("Services");

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Service param)
        {
            if (param.ServiceId > 0)
            {
                try
                {
                    param.IsActive = true;
                    _repository.UpdateService(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetService();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_ServiceGridViewPartial", model.OrderByDescending(x => x.ServiceId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GridViewPartialDelete(string ServiceId)
        {
            _repository.DeleteService(ServiceId);

            return View("Services");
        }

        

        /// <summary>
        /// Countries the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CategoryPartial()
        {
            return PartialView(new Crm_Service());
        }
        /// <summary>
        /// Cities the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CurrencyPartial()
        {
            return PartialView(new Crm_Service());
        }

        /// <summary>
        /// Shippings the country partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult UnitPartial()
        {
            return PartialView(new Crm_Service());
        }

        public ActionResult LoadOnDemandPartial()
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            Thread.Sleep(1000);
            return PartialView("LoadOnDemandPartial");
        }

        [HttpPost]
        public void GetSelectedValuesFromGrid(string selectedIDsHF)
        {
            //Get all selected keys from hidden input
            ViewData["_selectedIDs"] = selectedIDsHF;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="selectedIDsHF">The selected i ds hf.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BulkDelete(string selectedIDsHF)
        {
            var result = selectedIDsHF.Split(',');
            if (result[0] == "Delete")
            {

                for (int i = 1; i <= result.Length - 1; i++)
                {
                    _repository.DeleteService(result[i]);
                }
            }

            return View("Services");
        }

        [HttpGet]
        public ActionResult ExternalEditFormEdit(int ServiceId)
        {
            model = _repository.GetService();


            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("ServicesNewForm", model.FirstOrDefault(l => l.ServiceId == ServiceId));
        }
    }
}
