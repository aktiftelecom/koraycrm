﻿using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Security;
using CRM.Data.Repository.CrmRepository;
using CRM.Data.Repository.UserRepository;
using CRM.Models;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class UserController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IUserRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<CrmUserDto> model;
        #endregion

        public UserController(IUserRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Users()
        {
            return View();
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult UserGridViewPartial()
        {
            model = _repository.GetAllUsers();

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var roleList = model.ToList().Select(item => item.Roles).ToList();
            ViewData["Roles"] = _repository.GetAllRoles().ToList();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            return PartialView("_UserGridViewPartial", model.OrderByDescending(x => x.UserId).ToList());
        }

        /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        //[HttpPost, ValidateInput(false)]
        //public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] CrmUserDto param)
        //{

        //    _repository.SaveUser(param);

        //    var model = _repository.GetAllUsers();
        //    if (model == null)
        //    {
        //        return RedirectToAction("Index", "Login");
        //    }
        //    return PartialView("_UserGridViewPartial", model.OrderByDescending(x => x.UserId).ToList());

        //}

        public async Task<ActionResult> GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] CrmUserDto param)
        {
            using (var client = new HttpClient())
            {
                // New code:
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["host"] + "/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //var member = new Members_Member() { AllowedSections = 127, CreationTime = DateTime.Now, CustomerId = 0, Email = "admin@company.com", FirstName = "test", IsGroup = true, L = 1, LoginName = "new test", MailEvents = 0, ParentId = 2, PassRetries = 0, Permissions = -1, R = 134, Status = 1, Type = 0, UseParentPermission = false,UserGender = 0,VisitsCount = 0,VisualParams = 5};
                var member = new UserModel { UserName = param.UserName, Password = param.Password, ConfirmPassword = param.Password, EMail = param.Email, FirstName = param.FirstName.Trim(), LastName = param.LastName, PhoneNumber = param.PhoneNumber};
                var response = await client.PostAsJsonAsync("api/Account/Register", member);
                if (response.IsSuccessStatusCode)
                {
                    // Get the URI of the created resource.
                    Uri gizmoUrl = response.Headers.Location;
                }
            }
            var model = _repository.GetAllUsers();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_UserGridViewPartial", model.OrderByDescending(x => x.UserId).ToList());
        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] CrmUserDto param)
        {
            if (!string.IsNullOrEmpty(param.Id))
            {
                try
                {
                    _repository.UpdateUser(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetAllUsers();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_UserGridViewPartial", model.OrderByDescending(x => x.UserId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="CampaignId">The campaign identifier.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(string Id)
        {
            if (!string.IsNullOrEmpty(Id))
            {
                try
                {
                    _repository.DeleteUser(Id);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }

            var model = _repository.GetAllUsers();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_UserGridViewPartial", model.OrderByDescending(x => x.UserId).ToList());
        }
        
    }
}
