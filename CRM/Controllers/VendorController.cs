﻿using System;
using System.Configuration;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Vendor;
using CRM.Data.Repository.PriceBookRepository;
using CRM.Data.Repository.ServiceRepository;
using CRM.Data.Repository.VendorRepository;
using DevExpress.Web.Mvc;

namespace CRM.Controllers
{
    public class VendorController : Controller
    {
        #region prop
        /// <summary>
        /// The _repository
        /// </summary>
        private IVendorRepository _repository;

        /// <summary>
        /// The model
        /// </summary>
        private IQueryable<Crm_Vendor> model;
        #endregion

        public VendorController(IVendorRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Users this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Order = 1)]
        public ActionResult Vendors()
        {
            return View();
        }

        public ActionResult VendorsNewForm()
        {
            model = _repository.GetVendors();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Vendor());
        }

        public ActionResult TabPageTest()
        {
            model = _repository.GetVendors();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Vendor());
        }

        public ActionResult TabPage()
        {
            model = _repository.GetVendors();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Crm_Vendor());
        }

        /// <summary>
        /// Users the grid view partial.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            model = _repository.GetVendors();

            var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
            var firstOrDefault = model.FirstOrDefault();
            ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return PartialView("_VendorGridViewPartial", model.OrderByDescending(x => x.VendorId).ToList());
        }

         /// <summary>
        /// Grids the view partial add new.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Vendor param)
         {
             
             param.Status = true;

            _repository.InsertVendors(param);

            return View("Vendors");

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult BulkEdit([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Vendor param)
        {
            var result = param.CurrentOperation.Split(',');
            if (result[0] == "Edit")
            {
                var vendors = _repository.GetVendors();
                 using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    for (int i = 1; i <= result.Length - 1; i++)
                    {
                        var vendor = vendors.FirstOrDefault(l => l.VendorId == int.Parse(result[i]));
                        if (vendor != null)
                        {
                            vendor.AssignedTo = param.AssignedTo;
                            vendor.Category = param.Category;
                            vendor.CityId = param.CityId;
                            vendor.Country = param.Country != string.Empty ? param.Country : vendor.Country;
                            vendor.Description = param.Description != string.Empty ? param.Description : vendor.Description;
                            vendor.Email = param.Email != string.Empty ? param.Email : vendor.Email;
                            vendor.GLAccount = param.GLAccount;
                            vendor.Name = param.Name != string.Empty ? param.Name : vendor.Name;
                            vendor.POBox = param.POBox != string.Empty ? param.POBox : vendor.POBox;
                            vendor.Phone = param.Phone != string.Empty ? param.Phone : vendor.Phone;
                            vendor.PostalCode = param.PostalCode ?? vendor.PostalCode;
                            vendor.State = param.State;
                            vendor.Street = param.Street != string.Empty ? param.Street : vendor.Street;
                            vendor.UpdatedTime = DateTime.Now;
                            vendor.WebSite = param.WebSite != string.Empty ? param.WebSite : vendor.WebSite;

                            this._repository.UpdateVendor(vendor);
                        }
                    }

                scope.Complete();

                }
            }

            return View("Vendors");

        }

        /// <summary>
        /// Grids the view partial update.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Crm_Vendor param)
        {
            if (param.VendorId > 0)
            {
                try
                {
                    param.Status = true;
                    _repository.UpdateVendor(param);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            var model = _repository.GetVendors();
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return PartialView("_VendorGridViewPartial", model.OrderByDescending(x => x.VendorId).ToList());
        }

        /// <summary>
        /// Grids the view partial delete.
        /// </summary>
        /// <param name="VendorId">The vendor identifier.</param>
        /// <returns></returns>
         [HttpGet]
        public ActionResult GridViewPartialDelete(string VendorId)
        {
            _repository.DeleteVendor(VendorId);

            return View("Vendors");
        }

         public ActionResult LoadOnDemandPartial()
         {
             // Intentionally pauses server-side processing, 
             // to demonstrate the Loading Panel functionality.
             return PartialView("LoadOnDemandPartial");
         }

         [HttpPost]
         public void GetSelectedValuesFromGrid(string selectedIDsHF)
         {
             //Get all selected keys from hidden input
             ViewData["_selectedIDs"] = selectedIDsHF;
         }

         /// <summary>
         /// Bulks the delete.
         /// </summary>
         /// <param name="selectedIDsHF">The selected i ds hf.</param>
         /// <returns></returns>
         [HttpPost]
         public ActionResult BulkDelete(string selectedIDsHF)
         {
             var result = selectedIDsHF.Split(',');
             if (result[0] == "Delete")
             {

                 for (int i = 1; i <= result.Length - 1; i++)
                 {
                     _repository.DeleteVendor(result[i]);
                 }
             }

             return View("Vendors");
         }

         [HttpGet]
         public ActionResult ExternalEditFormEdit(int VendorId)
         {
             model = _repository.GetVendors();

             var createPermissions = ConfigurationManager.AppSettings["CreatePermissions"];
             var firstOrDefault = model.FirstOrDefault();
             ViewData["IsAuthorized"] = firstOrDefault != null && createPermissions.Contains(firstOrDefault.CurrentRole);

             if (model == null)
             {
                 return RedirectToAction("Index", "Login");
             }


             return View("VendorsNewForm", model.FirstOrDefault(l => l.VendorId == VendorId));
         }

        /// <summary>
        /// Cities the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult GLAccountPartial()
        {
            return PartialView(new Crm_Vendor());
        }

        /// <summary>
        /// Countries the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CountryPartial()
        {
            return PartialView(new Crm_Vendor());
        }

        /// <summary>
        /// Cities the partial.
        /// </summary>
        /// <returns></returns>
        public ActionResult CityPartial()
        {
            int country = (Request.Params["CityId"] != null) ? int.Parse(Request.Params["CityId"]) : -1;
            return PartialView(new Crm_Vendor { CityId = country });
        }

        public ActionResult AssignToPartial()
        {
            return PartialView(new Crm_Vendor());

        }
    }
}
