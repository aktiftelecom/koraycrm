﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Core.Utility;
using CRM.Data.Repository.QuoteRepository;

namespace CRM.Data.Repository.ContactRepository
{
    public class ContactRepository : IContactRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteRepository" /> class.
        /// </summary>
        public ContactRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Contact/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Members_Contact> GetContact()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetContact", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Members_Contact>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="contact">The contact.</param>
        public void UpdateContact(Members_Contact contact)
        {
            WebApiUtility.CallWebApiForPost(contact, UserWebApiUrl + "UpdateContact");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="contact">The contact.</param>
        public void InsertContact(Members_Contact contact)
        {
            WebApiUtility.CallWebApiForPost(contact, UserWebApiUrl + "InsertContact");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="contactId">The contact identifier.</param>
        public void DeleteContat(string contactId)
        {
            WebApiUtility.CallWebApiForPost(contactId, UserWebApiUrl + "DeleteContact");
        }
    }
}
