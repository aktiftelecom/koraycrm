﻿using System.Linq;
using CRM.Core.Entities.Contact;

namespace CRM.Data.Repository.ContactRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IContactRepository
    {
        /// <summary>
        /// Gets the contact.
        /// </summary>
        /// <returns></returns>
        IQueryable<Members_Contact> GetContact();

        /// <summary>
        /// Updates the contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        void UpdateContact(Members_Contact contact);

        /// <summary>
        /// Inserts the contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        void InsertContact(Members_Contact contact);

        /// <summary>
        /// Deletes the contat.
        /// </summary>
        /// <param name="contactId">The contact identifier.</param>
        void DeleteContat(string contactId);

    }
}