﻿using System;
using System.Configuration;
using System.Linq;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Business.Dtoes.ProductDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.Logs;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Product;
using CRM.Core.Enums;
using CRM.Core.Utility;

namespace CRM.Data.Repository.CrmRepository
{
    public class CRMRepository : ICRMRepository
    {
        private string WebApiUrl;
        private string CampaignWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="CRMRepository"/> class.
        /// </summary>
        public CRMRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            CampaignWebApiUrl = WebApiUrl + "Campaign/";
        }

        /// <summary>
        /// Gets the CRM campaign dto.
        /// </summary>
        /// <returns></returns>
        public IQueryable<CrmCampaignDto> GetCrmCampaignDto()
        {
            var content = WebApiUtility.WebApiGet(CampaignWebApiUrl + "GetCrmCampaign",true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<CrmCampaignDto>(content) : null;
        }

        /// <summary>
        /// Saves the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        public void SaveCampaign(CrmCampaignDto campaign)
        {
            WebApiUtility.CallWebApiForPost(campaign, CampaignWebApiUrl + "InsertCrmCampaign");
        }

        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        public void UpdateCampaign(CrmCampaignDto campaign)
        {
            WebApiUtility.CallWebApiForPost(campaign, CampaignWebApiUrl + "UpdateCrmCampaign");
        }

        /// <summary>
        /// Delete the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <param name="id">Campaign Id</param>
        public void DeleteCampaign(int id)
        {
            WebApiUtility.CallWebApiForPost(id, CampaignWebApiUrl + "DeleteCrmCampaign");
        }
    }
}
