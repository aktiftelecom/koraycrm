﻿using System.Linq;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.Logs;

namespace CRM.Data.Repository.CrmRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface ICRMRepository
    {

        /// <summary>
        /// Gets the CRM campaign dto.
        /// </summary>
        /// <returns></returns>
        IQueryable<CrmCampaignDto> GetCrmCampaignDto();

        /// <summary>
        /// Saves the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        void SaveCampaign(CrmCampaignDto campaign);

        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        void UpdateCampaign(CrmCampaignDto campaign);

        /// <summary>
        /// Deletes the campaign.
        /// </summary>
        /// <param name="id">Campaign id.</param>
        void DeleteCampaign(int id);
    }
}