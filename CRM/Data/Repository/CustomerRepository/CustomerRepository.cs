﻿using System.Configuration;
using System.Linq;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Member;
using CRM.Core.Utility;
using CRM.Data.Repository.CrmRepository;
using CRM.Data.Repository.UserRepository;

namespace CRM.Data.Repository.CustomerRepository
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="CRMRepository"/> class.
        /// </summary>
        public CustomerRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Member/";
        }

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Members_Customer> GetAllCustomers()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetCrmCustomer", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Members_Customer>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="user">The user.</param>
        public void UpdateCustomer(Members_Customer customer)
        {
            WebApiUtility.CallWebApiForPost(customer, UserWebApiUrl + "UpdateCrmCustomer");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="customer">The customer.</param>
        public void SaveCustomer(Members_Customer customer)
        {
            WebApiUtility.CallWebApiForPost(customer, UserWebApiUrl + "InsertCrmCustomer");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        public void DeleteCustomer(string customerId)
        {
            WebApiUtility.CallWebApiForPost(customerId, UserWebApiUrl + "DeleteCrmCustomer");
        }
    }
}
