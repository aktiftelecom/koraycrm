﻿using System.Linq;
using CRM.Business.Dtoes.MemberDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Member;

namespace CRM.Data.Repository.CustomerRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface ICustomerRepository
    {
        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <returns></returns>
        IQueryable<Members_Customer> GetAllCustomers();

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="user">The user.</param>
        void UpdateCustomer(Members_Customer customer);

        /// <summary>
        /// Saves the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        void SaveCustomer(Members_Customer customer);

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        void DeleteCustomer(string customerId);
    }
}