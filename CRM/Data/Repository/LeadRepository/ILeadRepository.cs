﻿using System.Linq;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;

namespace CRM.Data.Repository.LeadRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface ILeadRepository
    {
        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Lead> GetAllLead();

        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="lead">The lead.</param>
        void UpdateLead(Crm_Lead lead);

        /// <summary>
        /// Saves the product.
        /// </summary>
        /// <param name="lead">The lead.</param>
        void SaveLead(Crm_Lead lead);

        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="leadId">The lead identifier.</param>
        void DeleteLead(string leadId);

        /// <summary>
        /// Gets all district.
        /// </summary>
        /// <returns></returns>
        IQueryable<This_District> GetAllDistrict();

        /// <summary>
        /// Gets the CRM lead source.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_LeadSource> GetCrmLeadSource();
    }
}