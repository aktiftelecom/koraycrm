﻿using System.Configuration;
using System.Linq;
using System.Text;
using CRM.Core.Entities.City;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Product;
using CRM.Core.Utility;
using CRM.Data.Repository.CrmRepository;

namespace CRM.Data.Repository.LeadRepository
{
    public class LeadRepository : ILeadRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="CRMRepository" /> class.
        /// </summary>
        public LeadRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Lead/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_Lead> GetAllLead()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetCrmLead", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_Lead>(content) : null;
        }

        /// <summary>
        /// Gets all district.
        /// </summary>
        /// <returns></returns>
        public IQueryable<This_District> GetAllDistrict()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetDistrict", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<This_District>(content) : null;
        }

        /// <summary>
        /// Gets all cities.
        /// </summary>
        /// <returns></returns>
        public IQueryable<This_City> GetAllCities()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetCity", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<This_City>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="lead">The lead.</param>
        public void UpdateLead(Crm_Lead lead)
        {
            WebApiUtility.CallWebApiForPost(lead, UserWebApiUrl + "UpdateCrmLead");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="lead">The lead.</param>
        public void SaveLead(Crm_Lead lead)
        {
            WebApiUtility.CallWebApiForPost(lead, UserWebApiUrl + "InsertCrmLead");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="leadId">The product identifier.</param>
        public void DeleteLead(string leadId)
        {
            WebApiUtility.CallWebApiForPost(leadId, UserWebApiUrl + "DeleteCrmLead");
        }

        /// <summary>
        /// Gets the CRM lead source.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_LeadSource> GetCrmLeadSource()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetCrmLeadSource", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_LeadSource>(content) : null;
        }
    }
}
