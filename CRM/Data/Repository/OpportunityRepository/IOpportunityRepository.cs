﻿using System.Linq;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Core.Entities.Opportunity;
using CRM.Core.Entities.Sale;

namespace CRM.Data.Repository.QuoteRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IOpportunityRepository
    {
        /// <summary>
        /// Gets the opportunity.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Opportunity> GetOpportunity();

        /// <summary>
        /// Updates the opportunity.
        /// </summary>
        /// <param name="quote">The quote.</param>
        void UpdateOpportunity(Crm_Opportunity quote);

        /// <summary>
        /// Inserts the opportunity.
        /// </summary>
        /// <param name="opportunity">The opportunity.</param>
        void InsertOpportunity(Crm_Opportunity opportunity);

        /// <summary>
        /// Deletes the opportunity.
        /// </summary>
        /// <param name="opportunityId">The opportunity identifier.</param>
        void DeleteOpportunity(string opportunityId);

        /// <summary>
        /// Gets the sale stage.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_SaleStage> GetSaleStage();
    }
}