﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Core.Utility;
using CRM.Data.Repository.CrmRepository;
using CRM.Core.Entities.Opportunity;
using CRM.Core.Entities.Sale;

namespace CRM.Data.Repository.QuoteRepository
{
    public class OpportunityRepository : IOpportunityRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteRepository" /> class.
        /// </summary>
        public OpportunityRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Opportunity/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_Opportunity> GetOpportunity()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetOpportunity", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_Opportunity>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="quote">The quote.</param>
        public void UpdateOpportunity(Crm_Opportunity quote)
        {
            WebApiUtility.CallWebApiForPost(quote, UserWebApiUrl + "UpdateOpportunity");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="opportunity">The opportunity.</param>
        public void InsertOpportunity(Crm_Opportunity opportunity)
        {
            WebApiUtility.CallWebApiForPost(opportunity, UserWebApiUrl + "InsertOpportunity");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="opportunityId">The opportunity identifier.</param>
        public void DeleteOpportunity(string opportunityId)
        {
            WebApiUtility.CallWebApiForPost(opportunityId, UserWebApiUrl + "DeleteOpportunity");
        }

        /// <summary>
        /// Gets the sale stage.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_SaleStage> GetSaleStage()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetSaleStage", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_SaleStage>(content) : null;
        }
    }
}
