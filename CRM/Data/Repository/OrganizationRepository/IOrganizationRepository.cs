﻿using System.Linq;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;

namespace CRM.Data.Repository.OrganizationRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IOrganizationRepository
    {
        /// <summary>
        /// Gets the contact.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Organization> GetOrganization();

        /// <summary>
        /// Updates the contact.
        /// </summary>
        /// <param name="organization">The organization.</param>
        void UpdateOrganization(Crm_Organization organization);

        /// <summary>
        /// Inserts the contact.
        /// </summary>
        /// <param name="organization">The organization.</param>
        void InsertOrganization(Crm_Organization organization);

        /// <summary>
        /// Deletes the contat.
        /// </summary>
        /// <param name="organizationId">The organization identifier.</param>
        void DeleteOrganization(string organizationId);

        /// <summary>
        /// Gets the industry.
        /// </summary>
        /// <returns></returns>
        IQueryable<This_Industry> GetIndustry();

    }
}