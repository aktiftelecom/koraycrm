﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.Core.Utility;
using CRM.Data.Repository.QuoteRepository;

namespace CRM.Data.Repository.OrganizationRepository
{
    public class OrganizationRepository : IOrganizationRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteRepository" /> class.
        /// </summary>
        public OrganizationRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Organization/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_Organization> GetOrganization()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetOrganization", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_Organization>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="organization">The organization.</param>
        public void UpdateOrganization(Crm_Organization organization)
        {
            WebApiUtility.CallWebApiForPost(organization, UserWebApiUrl + "UpdateOrganization");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="organization">The organization.</param>
        public void InsertOrganization(Crm_Organization organization)
        {
            WebApiUtility.CallWebApiForPost(organization, UserWebApiUrl + "InsertOrganization");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="organizationId">The organization identifier.</param>
        public void DeleteOrganization(string organizationId)
        {
            WebApiUtility.CallWebApiForPost(organizationId, UserWebApiUrl + "DeleteOrganization");
        }

        /// <summary>
        /// Gets the industry.
        /// </summary>
        /// <returns></returns>
        public IQueryable<This_Industry> GetIndustry()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetIndustry", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<This_Industry>(content) : null;
        }
    }
}
