﻿using System.Linq;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;

namespace CRM.Data.Repository.PriceBookRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IPriceBookRepository
    {
        /// <summary>
        /// Gets the price book.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_PriceBook> GetPriceBook();

        /// <summary>
        /// Updates the price book.
        /// </summary>
        /// <param name="priceBook">The price book.</param>
        void UpdatePriceBook(Crm_PriceBook priceBook);

        /// <summary>
        /// Saves the price book.
        /// </summary>
        /// <param name="priceBook">The price book.</param>
        void SavePriceBook(Crm_PriceBook priceBook);

        /// <summary>
        /// Deletes the price book.
        /// </summary>
        /// <param name="priceBookId">The price book identifier.</param>
        void DeletePriceBook(string priceBookId);

    }
}