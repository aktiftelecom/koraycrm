﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Utility;
using CRM.Data.Repository.QuoteRepository;

namespace CRM.Data.Repository.PriceBookRepository
{
    public class PriceBookRepository : IPriceBookRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteRepository" /> class.
        /// </summary>
        public PriceBookRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "PriceBook/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_PriceBook> GetPriceBook()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetPriceBook", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_PriceBook>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="service">The service.</param>
        public void UpdatePriceBook(Crm_PriceBook priceBook)
        {
            WebApiUtility.CallWebApiForPost(priceBook, UserWebApiUrl + "UpdatePriceBook");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="service">The service.</param>
        public void SavePriceBook(Crm_PriceBook priceBook)
        {
            WebApiUtility.CallWebApiForPost(priceBook, UserWebApiUrl + "SavePriceBook");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        public void DeletePriceBook(string priceBookId)
        {
            WebApiUtility.CallWebApiForPost(priceBookId, UserWebApiUrl + "DeletePriceBook");
        }
    }
}
