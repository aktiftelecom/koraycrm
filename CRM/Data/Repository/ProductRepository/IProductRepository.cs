﻿using System.Linq;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.Product;

namespace CRM.Data.Repository.ProductRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IProductRepository
    {
        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Product> GetAllProduct();

        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="product">The product.</param>
        void UpdateProduct(Crm_Product product);

        /// <summary>
        /// Saves the product.
        /// </summary>
        /// <param name="product">The product.</param>
        void SaveProduct(Crm_Product product);

        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        void DeleteProduct(string productId);

        /// <summary>
        /// Gets all currencies.
        /// </summary>
        /// <returns></returns>
        IQueryable<This_Currency> GetAllCurrencies();
    }
}