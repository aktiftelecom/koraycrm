﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Product;
using CRM.Core.Utility;
using CRM.Data.Repository.CrmRepository;

namespace CRM.Data.Repository.ProductRepository
{
    public class ProductRepository : IProductRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="CRMRepository"/> class.
        /// </summary>
        public ProductRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Product/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_Product> GetAllProduct()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetCrmProduct", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_Product>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="product">The product.</param>
        public void UpdateProduct(Crm_Product product)
        {
            WebApiUtility.CallWebApiForPost(product, UserWebApiUrl + "UpdateCrmProduct");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="product">The product.</param>
        public void SaveProduct(Crm_Product product)
        {
            WebApiUtility.CallWebApiForPost(product, UserWebApiUrl + "InsertCrmProduct");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        public void DeleteProduct(string productId)
        {
            WebApiUtility.CallWebApiForPost(productId, UserWebApiUrl + "DeleteCrmProduct");
        }

        /// <summary>
        /// Gets all currencies.
        /// </summary>
        /// <returns></returns>
        public IQueryable<This_Currency> GetAllCurrencies()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetAllCurrencies", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<This_Currency>(content) : null;
        }
    }
}
