﻿using System.Linq;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;

namespace CRM.Data.Repository.QuoteRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IQuoteRepository
    {
        /// <summary>
        /// Gets the quote.
        /// </summary>
        /// <returns></returns>
        IQueryable<Projects_Quote> GetQuote();

        /// <summary>
        /// Updates the quote.
        /// </summary>
        /// <param name="quote">The quote.</param>
        void UpdateQuote(Projects_Quote quote);

        /// <summary>
        /// Inserts the quote.
        /// </summary>
        /// <param name="quote">The quote.</param>
        void InsertQuote(Projects_Quote quote);

        /// <summary>
        /// Deletes the quote.
        /// </summary>
        /// <param name="quoteId">The quote identifier.</param>
        void DeleteQuote(string quoteId);

        /// <summary>
        /// Gets the contact.
        /// </summary>
        /// <returns></returns>
        IQueryable<Members_Contact> GetContact();

        /// <summary>
        /// Gets the project.
        /// </summary>
        /// <returns></returns>
        IQueryable<Projects_Project> GetProject();

        /// <summary>
        /// Gets the quote stage.
        /// </summary>
        /// <returns></returns>
        IQueryable<Projects_QuoteStage> GetQuoteStage();
    }
}