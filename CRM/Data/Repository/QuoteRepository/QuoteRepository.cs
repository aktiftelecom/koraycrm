﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.City;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.District;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Core.Utility;
using CRM.Data.Repository.CrmRepository;

namespace CRM.Data.Repository.QuoteRepository
{
    public class QuoteRepository : IQuoteRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteRepository" /> class.
        /// </summary>
        public QuoteRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Quote/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Projects_Quote> GetQuote()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetQuote", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Projects_Quote>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="quote">The quote.</param>
        public void UpdateQuote(Projects_Quote quote)
        {
            WebApiUtility.CallWebApiForPost(quote, UserWebApiUrl + "UpdateQuote");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="quote">The quote.</param>
        public void InsertQuote(Projects_Quote quote)
        {
            WebApiUtility.CallWebApiForPost(quote, UserWebApiUrl + "InsertQuote");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="quoteId">The quote identifier.</param>
        public void DeleteQuote(string quoteId)
        {
            WebApiUtility.CallWebApiForPost(quoteId, UserWebApiUrl + "DeleteQuote");
        }

        /// <summary>
        /// Gets the contact.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Members_Contact> GetContact()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetContact", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Members_Contact>(content) : null;
        }

        /// <summary>
        /// Gets the project.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Projects_Project> GetProject()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetProject", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Projects_Project>(content) : null;
        }

        /// <summary>
        /// Gets the quote stage.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Projects_QuoteStage> GetQuoteStage()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetQuoteStage", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Projects_QuoteStage>(content) : null;
        }
    }
}
