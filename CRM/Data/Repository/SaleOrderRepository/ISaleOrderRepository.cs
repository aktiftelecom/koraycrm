﻿using System.Linq;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.SaleOrder;

namespace CRM.Data.Repository.SaleOrderRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface ISaleOrderRepository
    {
        /// <summary>
        /// Gets the sale order.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_SaleOrder> GetSaleOrder();

        /// <summary>
        /// Updates the sale order.
        /// </summary>
        /// <param name="order">The order.</param>
        void UpdateSaleOrder(Crm_SaleOrder order);

        /// <summary>
        /// Inserts the sale order.
        /// </summary>
        /// <param name="order">The order.</param>
        void InsertSaleOrder(Crm_SaleOrder order);

        /// <summary>
        /// Deletes the sale order.
        /// </summary>
        /// <param name="saleOrderId">The sale order identifier.</param>
        void DeleteSaleOrder(string saleOrderId);

    }
}