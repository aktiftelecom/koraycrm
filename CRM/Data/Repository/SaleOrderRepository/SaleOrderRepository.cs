﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.Industry;
using CRM.Core.Entities.Organization;
using CRM.Core.Entities.SaleOrder;
using CRM.Core.Utility;
using CRM.Data.Repository.QuoteRepository;

namespace CRM.Data.Repository.SaleOrderRepository
{
    public class SaleOrderRepository : ISaleOrderRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteRepository" /> class.
        /// </summary>
        public SaleOrderRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "SaleOrder/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_SaleOrder> GetSaleOrder()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetSaleOrder", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_SaleOrder>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="order">The order.</param>
        public void UpdateSaleOrder(Crm_SaleOrder order)
        {
            WebApiUtility.CallWebApiForPost(order, UserWebApiUrl + "UpdateSaleOrder");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="order">The order.</param>
        public void InsertSaleOrder(Crm_SaleOrder order)
        {
            WebApiUtility.CallWebApiForPost(order, UserWebApiUrl + "InsertSaleOrder");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="saleOrderId">The sale order identifier.</param>
        public void DeleteSaleOrder(string saleOrderId)
        {
            WebApiUtility.CallWebApiForPost(saleOrderId, UserWebApiUrl + "DeleteSaleOrder");
        }

    }
}
