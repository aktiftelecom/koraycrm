﻿using System.Linq;
using CRM.Core.Entities.SaleOrder;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;

namespace CRM.Data.Repository.ServiceRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IServiceRepository
    {
        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Service> GetService();

        /// <summary>
        /// Updates the service.
        /// </summary>
        /// <param name="service">The service.</param>
        void UpdateService(Crm_Service service);

        /// <summary>
        /// Inserts the service.
        /// </summary>
        /// <param name="service">The service.</param>
        void InsertService(Crm_Service service);

        /// <summary>
        /// Deletes the service.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        void DeleteService(string serviceId);

        /// <summary>
        /// Gets the service category.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_ServiceCategory> GetServiceCategory();

        /// <summary>
        /// Gets the units.
        /// </summary>
        /// <returns></returns>
        IQueryable<This_Unit> GetUnits();

    }
}