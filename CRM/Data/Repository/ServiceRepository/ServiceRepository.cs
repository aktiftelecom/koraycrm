﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.SaleOrder;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;
using CRM.Core.Utility;
using CRM.Data.Repository.QuoteRepository;

namespace CRM.Data.Repository.ServiceRepository
{
    public class ServiceRepository : IServiceRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteRepository" /> class.
        /// </summary>
        public ServiceRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Service/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_Service> GetService()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetService", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_Service>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="service">The service.</param>
        public void UpdateService(Crm_Service service)
        {
            WebApiUtility.CallWebApiForPost(service, UserWebApiUrl + "UpdateService");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="service">The service.</param>
        public void InsertService(Crm_Service service)
        {
            WebApiUtility.CallWebApiForPost(service, UserWebApiUrl + "InsertService");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        public void DeleteService(string serviceId)
        {
            WebApiUtility.CallWebApiForPost(serviceId, UserWebApiUrl + "DeleteService");
        }

        /// <summary>
        /// Gets the service category.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_ServiceCategory> GetServiceCategory()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetServiceCategory", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_ServiceCategory>(content) : null;
        }

        /// <summary>
        /// Gets the units.
        /// </summary>
        /// <returns></returns>
        public IQueryable<This_Unit> GetUnits()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetUnit", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<This_Unit>(content) : null;
        }

    }
}
