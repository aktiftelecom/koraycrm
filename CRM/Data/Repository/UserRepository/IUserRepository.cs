﻿using System.Linq;
using CRM.Business.Dtoes.CampaignDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Entities.Campaign;
using CRM.Core.Entities.Logs;

namespace CRM.Data.Repository.UserRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Gets all roles.
        /// </summary>
        /// <returns></returns>
        IQueryable<CrmRoleDto> GetAllRoles();

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        IQueryable<CrmUserDto> GetAllUsers();

        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        void UpdateUser(CrmUserDto user);

        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="user">The user.</param>
        void DeleteUser(string userId);
    }
}