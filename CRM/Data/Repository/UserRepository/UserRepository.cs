﻿using System.Configuration;
using System.Linq;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Core.Utility;
using CRM.Data.Repository.CrmRepository;

namespace CRM.Data.Repository.UserRepository
{
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="CRMRepository"/> class.
        /// </summary>
        public UserRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Account/";
        }

        /// <summary>
        /// Gets all roles.
        /// </summary>
        /// <returns></returns>
        public IQueryable<CrmRoleDto> GetAllRoles()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetRoles",false);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<CrmRoleDto>(content) : null;
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        public IQueryable<CrmUserDto> GetAllUsers()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetCrmUsers",true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<CrmUserDto>(content) : null;
        }

        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="user">The user.</param>
        public void UpdateUser(CrmUserDto user)
        {
            WebApiUtility.CallWebApiForPost(user, UserWebApiUrl + "UpdateCrmUsers");


        }

        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        public void DeleteUser(string userId)
        {
            WebApiUtility.CallWebApiForPost(userId, UserWebApiUrl + "DeleteCrmUser");
        }
    }
}
