﻿using System.Linq;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Entities.Vendor;

namespace CRM.Data.Repository.VendorRepository
{
    /// <summary>
    /// ICampaignRepository
    /// </summary>
    public interface IVendorRepository
    {
        /// <summary>
        /// Gets the vendors.
        /// </summary>
        /// <returns></returns>
        IQueryable<Crm_Vendor> GetVendors();

        /// <summary>
        /// Updates the vendor.
        /// </summary>
        /// <param name="vendor">The vendor.</param>
        void UpdateVendor(Crm_Vendor vendor);

        /// <summary>
        /// Inserts the vendors.
        /// </summary>
        /// <param name="vendor">The vendor.</param>
        void InsertVendors(Crm_Vendor vendor);

        /// <summary>
        /// Deletes the vendor.
        /// </summary>
        /// <param name="vendorId">The vendor identifier.</param>
        void DeleteVendor(string vendorId);

    }
}