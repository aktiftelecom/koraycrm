﻿using System.Configuration;
using System.Linq;
using CRM.Core.Entities.PriceBook;
using CRM.Core.Entities.Vendor;
using CRM.Core.Utility;
using CRM.Data.Repository.QuoteRepository;

namespace CRM.Data.Repository.VendorRepository
{
    public class VendorRepository : IVendorRepository
    {
        /// <summary>
        /// The web API URL
        /// </summary>
        private string WebApiUrl;

        /// <summary>
        /// The campaign web API URL
        /// </summary>
        private string UserWebApiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteRepository" /> class.
        /// </summary>
        public VendorRepository()
        {
            WebApiUrl = ConfigurationManager.AppSettings["host"]+"/api/";
            UserWebApiUrl = WebApiUrl + "Vendor/";
        }

        /// <summary>
        /// Gets all product.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Crm_Vendor> GetVendors()
        {
            var content = WebApiUtility.WebApiGet(UserWebApiUrl + "GetVendors", true);
            return !string.IsNullOrEmpty(content) ? WebApiUtility.GetQueryableObjectFromJson<Crm_Vendor>(content) : null;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="vendor">The service.</param>
        public void UpdateVendor(Crm_Vendor vendor)
        {
            WebApiUtility.CallWebApiForPost(vendor, UserWebApiUrl + "UpdateVendor");


        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="vendor">The service.</param>
        public void InsertVendors(Crm_Vendor vendor)
        {
            WebApiUtility.CallWebApiForPost(vendor, UserWebApiUrl + "InsertVendors");
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="vendorId">The service identifier.</param>
        public void DeleteVendor(string vendorId)
        {
            WebApiUtility.CallWebApiForPost(vendorId, UserWebApiUrl + "DeleteVendor");
        }
    }
}
