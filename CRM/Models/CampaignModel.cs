﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CRM.Models
{
    /// <summary>
    /// Campaign Model
    /// </summary>
    public class CampaignModel
    {
        /// <summary>
        /// Gets or sets the campaign identifier.
        /// </summary>
        /// <value>
        /// The campaign identifier.
        /// </value>
        public int CampaignId { get; set; }

        /// <summary>
        /// Gets or sets the name of the campaign.
        /// </summary>
        /// <value>
        /// The name of the campaign.
        /// </value>
        [Required]
        [StringLength(20)]
        [DataType(DataType.Text)]
        public string CampaignName { get; set; }

        /// <summary>
        /// Gets or sets the assign to.
        /// </summary>
        /// <value>
        /// The assign to.
        /// </value>
        public int AssignTo { get; set; }

        /// <summary>
        /// Gets or sets the type identifier.
        /// </summary>
        /// <value>
        /// The type identifier.
        /// </value>
        public int TypeId { get; set; }

        /// <summary>
        /// Gets or sets the status identifier.
        /// </summary>
        /// <value>
        /// The status identifier.
        /// </value>
        public int StatusId { get; set; }

        /// <summary>
        /// Gets or sets the expected close date.
        /// </summary>
        /// <value>
        /// The expected close date.
        /// </value>
        public DateTime ExpectedCloseDate { get; set; }

        /// <summary>
        /// Gets or sets the target audience.
        /// </summary>
        /// <value>
        /// The target audience.
        /// </value>
        public string TargetAudience { get; set; }

        /// <summary>
        /// Gets or sets the sponsor.
        /// </summary>
        /// <value>
        /// The sponsor.
        /// </value>
        public string Sponsor { get; set; }

        /// <summary>
        /// Gets or sets the size of the target.
        /// </summary>
        /// <value>
        /// The size of the target.
        /// </value>
        public int TargetSize { get; set; }

        /// <summary>
        /// Gets or sets the number sent.
        /// </summary>
        /// <value>
        /// The number sent.
        /// </value>
        public int NumberSent { get; set; }

        /// <summary>
        /// Gets or sets the budget cost.
        /// </summary>
        /// <value>
        /// The budget cost.
        /// </value>
        public double BudgetCost { get; set; }

        /// <summary>
        /// Gets or sets the expected revenue.
        /// </summary>
        /// <value>
        /// The expected revenue.
        /// </value>
        public double ExpectedRevenue { get; set; }

        /// <summary>
        /// Gets or sets the expected sales count.
        /// </summary>
        /// <value>
        /// The expected sales count.
        /// </value>
        public int ExpectedSalesCount { get; set; }

        /// <summary>
        /// Gets or sets the expected response count.
        /// </summary>
        /// <value>
        /// The expected response count.
        /// </value>
        public int ExpectedResponseCount { get; set; }

        /// <summary>
        /// Gets or sets the expected roi.
        /// </summary>
        /// <value>
        /// The expected roi.
        /// </value>
        public double ExpectedROI { get; set; }

        /// <summary>
        /// Gets or sets the actual cost.
        /// </summary>
        /// <value>
        /// The actual cost.
        /// </value>
        public double ActualCost { get; set; }

        /// <summary>
        /// Gets or sets the expected response.
        /// </summary>
        /// <value>
        /// The expected response.
        /// </value>
        public int ExpectedResponse { get; set; }

        /// <summary>
        /// Gets or sets the actual sales count.
        /// </summary>
        /// <value>
        /// The actual sales count.
        /// </value>
        public int ActualSalesCount { get; set; }

        /// <summary>
        /// Gets or sets the actual response count.
        /// </summary>
        /// <value>
        /// The actual response count.
        /// </value>
        public int ActualResponseCount { get; set; }

        /// <summary>
        /// Gets or sets the actual roi.
        /// </summary>
        /// <value>
        /// The actual roi.
        /// </value>
        public double ActualROI { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the updated time.
        /// </summary>
        /// <value>
        /// The updated time.
        /// </value>
        public DateTime UpdatedTime { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created time.
        /// </summary>
        /// <value>
        /// The created time.
        /// </value>
        public System.DateTime CreatedTime { get; set; }

        /// <summary>
        /// Gets or sets the updated by.
        /// </summary>
        /// <value>
        /// The updated by.
        /// </value>
        public int UpdatedBy { get; set; }
    }
}
