﻿using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.ShippingDtoes;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Data.Repository.LeadRepository;
using CRM.Data.Repository.OrganizationRepository;
using CRM.Data.Repository.QuoteRepository;
using CRM.Core.Entities.Industry;

namespace CRM.Models
{
    public class Industry
    {

        public static IEnumerable<This_Industry> GetIndustry()
        {
            var repository = new OrganizationRepository();
            return repository.GetIndustry();
        }
    }
}
