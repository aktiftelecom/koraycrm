﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Core.Entities.City;
using CRM.Core.Entities.District;
using CRM.Data.Repository.LeadRepository;

namespace CRM.Models
{
    public class Location
    {
        public Location()
        {
            ID = -1;
            Name = string.Empty;
            Country = -1;
            City = -1;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int Country { get; set; }
        public int City { get; set; }
    }


    public class Country
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public static IEnumerable<This_City> GetCountries()
        {
            var _repository = new LeadRepository();

            return _repository.GetAllCities().ToList();
        }
    }
    public class City
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CountryID { get; set; }

        public static IEnumerable<This_District> GetCities(int country)
        {
            var _repository = new LeadRepository();
            return _repository.GetAllDistrict().Where(d => d.CityId == country);
        }
    }
}
