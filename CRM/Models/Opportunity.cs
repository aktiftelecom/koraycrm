﻿using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.ShippingDtoes;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Opportunity;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Data.Repository.LeadRepository;
using CRM.Data.Repository.QuoteRepository;
using CRM.Data.Repository.CustomerRepository;
using CRM.Core.Entities.Member;
using CRM.Core.Entities.Sale;
using CRM.Business.Dtoes.OpportunityDtoes;
using CRM.Data.Repository.CrmRepository;
using CRM.Business.Dtoes.CampaignDtoes;

namespace CRM.Models
{
    public class Opportunity
    {

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Members_Customer> GetCustomer()
        {
            var repository = new CustomerRepository();
            return repository.GetAllCustomers();
        }

        /// <summary>
        /// Gets the sale stage.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Crm_SaleStage> GetSaleStage()
        {
            var repository = new OpportunityRepository();
            return repository.GetSaleStage();
        }

        public static IEnumerable<Crm_LeadSource> GetCrmLeadSource()
        {
            var repository = new LeadRepository();
            return repository.GetCrmLeadSource();
        }

        /// <summary>
        /// Gets the type of the opportunity.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<OpportunityDto> GetOpportunityType()
        {
            return OpportunityDto.GetSOpportunityDtoes();
        }

        /// <summary>
        /// Gets the campaign.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<CrmCampaignDto> GetCampaign()
        {
            var repository = new CRMRepository();
            return repository.GetCrmCampaignDto();
        }

        /// <summary>
        /// Gets the opportunity.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Crm_Opportunity> GetOpportunity()
        {
            var repository = new OpportunityRepository();
            return repository.GetOpportunity();
        }
    }
}
