﻿using System.Collections.Generic;
using CRM.Business.Dtoes.OrganizationDtoes;
using CRM.Core.Entities.Currency;
using CRM.Core.Entities.Organization;
using CRM.Data.Repository.OrganizationRepository;
using CRM.Data.Repository.ProductRepository;

namespace CRM.Models
{
    public class Product
    {
        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<This_Currency> GetAllCurrencies()
        {
            var repository = new ProductRepository();
            return repository.GetAllCurrencies();
        }
    }
}
