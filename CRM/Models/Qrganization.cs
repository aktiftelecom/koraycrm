﻿using System.Collections.Generic;
using CRM.Business.Dtoes.OrganizationDtoes;
using CRM.Core.Entities.Organization;
using CRM.Data.Repository.OrganizationRepository;

namespace CRM.Models
{
    public class Qrganization
    {

        /// <summary>
        /// Gets the type of the organization.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<OrganizationDto> GetOrganizationType()
        {
            return OrganizationDto.GetOrganizationDtos();
        }

        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Crm_Organization> GetOrganization()
        {
            var repository = new OrganizationRepository();
            return repository.GetOrganization();
        }
    }
}
