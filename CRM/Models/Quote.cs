﻿using System.Collections.Generic;
using System.Linq;
using CRM.Business.Dtoes.ShippingDtoes;
using CRM.Core.Entities.Contact;
using CRM.Core.Entities.Lead;
using CRM.Core.Entities.Project;
using CRM.Core.Entities.Quote;
using CRM.Data.Repository.LeadRepository;
using CRM.Data.Repository.QuoteRepository;

namespace CRM.Models
{
    public class Quote
    {

        public static IEnumerable<Members_Contact> GetContact()
        {
            var repository = new QuoteRepository();
            return repository.GetContact();
        }

        public static IEnumerable<Crm_Lead> GetLead()
        {
            var repository = new LeadRepository();
            return repository.GetAllLead();
        }

        public static IEnumerable<Projects_Project> GetProject()
        {
            var repository = new QuoteRepository();
            return repository.GetProject();
        }

        public static IEnumerable<ShippingDto> GetShipping()
        {
            return ShippingDto.GetShippingDtos();
        }

        public static IEnumerable<Projects_QuoteStage> GetQuoteStage()
        {
            var repository = new QuoteRepository();
            return repository.GetQuoteStage();
        }
    }
}
