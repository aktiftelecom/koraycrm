﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Business.Dtoes.RatingDtoes;

namespace CRM.Models
{
    public class Rating
    {
        /// <summary>
        /// Gets the rating dtos.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<RatingDto> GetRatingDtos()
        {
            return RatingDto.GetRatingDtos();
        }
    }
}
