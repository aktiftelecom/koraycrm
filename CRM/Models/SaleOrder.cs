﻿using System.Collections.Generic;
using CRM.Business.Dtoes.OrganizationDtoes;
using CRM.Business.Dtoes.SaleOrderDto;
using CRM.Core.Entities.Organization;
using CRM.Data.Repository.OrganizationRepository;

namespace CRM.Models
{
    public class SaleOrder
    {

        /// <summary>
        /// Gets the type of the organization.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<TaxModeDto> GetTaxModeTypes()
        {
            return TaxModeDto.GetTaxModeDtos();
        }

        public static IEnumerable<PaymenDurationtDto> GetPaymenDurationtDtos()
        {
            return PaymenDurationtDto.GetPaymenDurationtDtos();
        }
    }
}
