﻿using System.Collections.Generic;
using CRM.Core.Entities.Service;
using CRM.Core.Entities.Unit;
using CRM.Data.Repository.ServiceRepository;

namespace CRM.Models
{
    public class Service
    {

        public static IEnumerable<Crm_ServiceCategory> GetServiceCategory()
        {
            var repository = new ServiceRepository();
            return repository.GetServiceCategory();
        }

        public static IEnumerable<This_Unit> GetUnits()
        {
            var repository = new ServiceRepository();
            return repository.GetUnits();
        }
    }
}
