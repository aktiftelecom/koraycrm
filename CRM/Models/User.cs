﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Business.Dtoes.RatingDtoes;
using CRM.Business.Dtoes.UserDtoes;
using CRM.Data.Repository.UserRepository;

namespace CRM.Models
{
    public class User
    {
        /// <summary>
        /// Gets the rating dtos.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<CrmUserDto> GetCrmUserDto()
        {
            var repository = new UserRepository();
            return repository.GetAllUsers();
        }
    }
}
