﻿using System.Collections.Generic;
using CRM.Business.Dtoes.GLAccountDtoes;

namespace CRM.Models
{
    public class VendorModel
    {

        /// <summary>
        /// Gets the type of the organization.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<GLAccountDto> GetGlAccountDtos()
        {
            return GLAccountDto.GetGLAccountDtos();
        }
    }
}
