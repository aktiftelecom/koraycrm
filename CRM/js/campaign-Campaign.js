﻿var app = angular.module("myApp", ["ngGrid", "ngTouch", "ui.grid", "ui.grid.exporter", "ui.grid.selection"]);
app.controller("MyCtrl", function ($scope, $http) {

    $http({ method: "GET", url: getBaseURL() + "api/v1/campaigns/Get" }).
    success(function (data, status, headers, config) {
        $scope.myData = data;
    }).
    error(function (data, status, headers, config) {
        alert("error");
    });

    $scope.columnDefs1 = [
        { field: "campaignId", visible: false },
        { field: "campaignName", displayName: "İsim" },
        { field: "targetAudience", displayName: "Hedef Kitle" },
        { field: "sponsor", displayName: "Sponsor" },
        { field: "budgetCost", displayName: "Bütçe" },
        { field: "expectedRevenue", displayName: "Kazanç" },
        { field: "expectedCloseDate", displayName: "Kapanış Süresi" },
        {
            field: "", displayName: "", cellTemplate:
                   "<div class=\"ngCellText\" ng-class=\"col.colIndex()\">" +
                   "<button class=\"glyphicon glyphicon-remove\" ng-click=\"EditCampaign(row)\">Edit</button></div>"
        },
        {
            field: "", displayName: "", cellTemplate:
                   "<div class=\"ngCellText\" ng-class=\"col.colIndex()\">" +
                   "<button class=\"glyphicon glyphicon-remove\" ng-click=\"DeleteCampaign(row)\">Delete</button></div>"
        }
    ];

    $scope.gridOptions = {
        data: "myData",
        columnDefs: "columnDefs1",
        enablePaging: true,
        showFooter: true,
        jqueryUITheme: true,
        exporterMenuCsv: false,
        enableGridMenu: true,
        multiSelect: true,
        showSelectionCheckbox: true,
        beforeSelectionChange: function (rowItem, event) {
            if (!event.ctrlKey && !event.shiftKey && event.type != "click") {
                var grid = $scope.gridOptions.ngGrid;
                grid.$viewport.scrollTop(rowItem.offsetTop - (grid.config.rowHeight * 2));
                angular.forEach($scope.myData, function (data, index) {
                    $scope.gridOptions.selectRow(index, false);
                });
            }
            return true;
        },
    };

    $scope.EditCampaign = function (row) {
        $.fancybox({
            'width': "40%",
            'height': "40%",
            'autoScale': true,
            'transitionIn': "fade",
            'transitionOut': "fade",
            'type': "iframe",
            'href': getBaseURL() + "Campaign/Edit/" + row.entity.campaignId
        });
    };

    $scope.DeleteCampaign = function (row) {
        var r = confirm("Are you sure to delete?");
        if (r === true) {
            $scope.DeleteCampaignConfirmed(row);
        }
    }

    $scope.DeleteCampaignConfirmed = function (row) {
        $.ajax({
            url: getBaseURL() + "Campaign/Delete/" + row.entity.campaignId,
            method: "POST",
            dataType: "json",
            traditional: true,
            async: false,
            success: function (response) {
                if (response.Success) {
                    window.location.reload();
                } else {
                    alert("error");
                }
            }
        });
    }

    $scope.title = "Kampanyalar";
    $scope.btntitle = "Yeni";
});
